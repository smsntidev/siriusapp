﻿using Xamarin.Forms;
using Xamarin.Forms.Platform.iOS;
using UIKit;
using CoreGraphics;
using System;
using Sirius.App;

[assembly: ExportRenderer(typeof(VEditor), typeof(Sirius.App.IOS.VEditorRenderer))]
namespace Sirius.App.IOS
{
    public class VEditorRenderer : EditorRenderer
    {
        protected override void OnElementChanged(ElementChangedEventArgs<Editor> e)
        {
            base.OnElementChanged(e);
            var view = Element as VEditor;

            if (Control != null)
            {
                Control.Layer.CornerRadius = 5;
                Control.Layer.BorderWidth = 0;
                Control.BackgroundColor = Color.FromHex("#868BAF").ToUIColor();
                ResizeHeight();
            }
        }

        private void ResizeHeight()
        {
            if (Element == null)
                return;

            if (Element.HeightRequest >= 0) return;

            var height = Math.Max(0, //Eu não sei se atribuir 0 é o correto, porém...
                new UITextField { Font = Control.Font }.IntrinsicContentSize.Height) * 2;

            Control.Frame = new CGRect(0.0f, 0.0f, (nfloat)Element.Width, (nfloat)height);

            Element.HeightRequest = height;
        }
    }
}