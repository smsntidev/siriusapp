﻿using Sirius.App;
using Xamarin.Forms;
using Xamarin.Forms.Platform.iOS;

[assembly: ExportRenderer(typeof(VLink), typeof(Sirius.App.IOS.VLinkRenderer))]
namespace Sirius.App.IOS
{
    public class VLinkRenderer : ButtonRenderer
    {
        protected override void OnElementChanged(ElementChangedEventArgs<Xamarin.Forms.Button> e)
        {
            base.OnElementChanged(e);

            if (Control != null)
            {
                Control.SetTitleColor(Color.White.ToUIColor(), UIKit.UIControlState.Normal);
                Control.BackgroundColor = Color.Transparent.ToUIColor();
                Control.Layer.BorderWidth = 0;
            }
        }
    }
}