﻿using Sirius.App;
using Xamarin.Forms;
using Xamarin.Forms.Platform.iOS;

[assembly: ExportRenderer(typeof(VButton), typeof(Sirius.App.IOS.VButtonRenderer))]
namespace Sirius.App.IOS
{
    public class VButtonRenderer : ButtonRenderer
    {
    }
}