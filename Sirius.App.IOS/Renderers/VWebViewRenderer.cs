﻿using Foundation;
using Sirius.App;
using System;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Net;
using UIKit;
using Xamarin.Forms;
using Xamarin.Forms.Platform.iOS;

[assembly: ExportRenderer(typeof(VWebView), typeof(Sirius.App.IOS.VWebViewRenderer))]
namespace Sirius.App.IOS
{
    public class VWebViewRenderer : ViewRenderer<VWebView, UIWebView>
    { 
        protected override void OnElementChanged(ElementChangedEventArgs<VWebView> e)
        {
            base.OnElementChanged(e);

            if (Control == null)
            {
                SetNativeControl(new UIWebView());
            }
            if (e.OldElement != null)
            {
                // Cleanup
            }
            if (e.NewElement != null)
            {
                var customWebView = Element as VWebView;
                SetUp(customWebView);
            }
        }

        private void SetUp(VWebView customWebView)
        {
            if (customWebView == null)
                return;
            
            if (customWebView.PdfBytes == null && customWebView.Uri != null)
            {
                string fileName = Path.Combine(NSBundle.MainBundle.BundlePath, string.Format("Content/{0}", WebUtility.UrlEncode(customWebView.Uri)));
                Control.LoadRequest(new NSUrlRequest(new NSUrl(fileName, false)));
                Control.ScalesPageToFit = true;
            }
            else if (customWebView.PdfBytes != null)
            {
                string fileName = GetPath(customWebView.PdfBytes.ToArray());
                Control.LoadRequest(new NSUrlRequest(new NSUrl(fileName, false)));
                Control.ScalesPageToFit = true;
            }
        }

        protected override void OnElementPropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            base.OnElementPropertyChanged(sender, e);

            if (e.PropertyName == "PdfBytes" || e.PropertyName == "Uri")
                SetUp(Element);
        }

        private string GetPath(byte[] bytes)
        {
            string[] paths = Foundation.NSSearchPath.GetDirectories(NSSearchPathDirectory.DocumentDirectory, NSSearchPathDomain.User, true);
            string documentsDirectory = paths[0];

            string filePath = $"{documentsDirectory}/{Guid.NewGuid().ToString("N")}.pdf";
            File.WriteAllBytes(filePath, bytes);
            return filePath;
        }
    }
}
