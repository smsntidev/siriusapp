﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;

using Foundation;
using UIKit;

[assembly: Xamarin.Forms.Dependency(typeof(Sirius.App.IOS.Implementations.iOSCookieManager))]

namespace Sirius.App.IOS.Implementations
{
    public class iOSCookieManager : ICookieManager
    {
        public void Set(Uri cookiesUrl, IEnumerable<Cookie> cookies)
        {
            var cookieUrl = cookiesUrl;
            var cookieJar = NSHttpCookieStorage.SharedStorage;
            cookieJar.AcceptPolicy = NSHttpCookieAcceptPolicy.Always;
            foreach (var aCookie in cookieJar.Cookies)
            {
                cookieJar.DeleteCookie(aCookie);
            }

            var jCookies = cookies.ToList();
            IList<NSHttpCookie> eCookies =
                (from object jCookie in jCookies
                 where jCookie != null
                 select (Cookie)jCookie
                 into netCookie
                 select new NSHttpCookie(netCookie)).ToList();
            cookieJar.SetCookies(eCookies.ToArray(), cookieUrl, cookieUrl);
        }
    }
}