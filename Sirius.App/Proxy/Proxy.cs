﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sirius.App
{
    public static class Proxy
    {
#if DEBUG
        //private const string Domain = "192.168.10.147";
        //private const string Domain = "10.0.2.2";
        //private const string Domain = "172.22.16.30";
        private const string Domain = "172.22.16.30/siriuswebapi";
        //private const string Domain = "siriusapi.conecti.com.br";
#else
        private const string Domain = "172.22.16.30";
        //private const string Domain = "siriusapi.conecti.com.br";
#endif
        private const string Port = "";
        private const string Schema = "http";

#if DEBUG
        private const string Subpath = "";
#else
        private const string Subpath = "/sirius/webapi";
        //private const string Subpath = "";
#endif

        private const string RelatorioSolucoesPath = "/Relatorio/RelatorioSolucoes";
        private const string RelatorioAtendimentosPath = "/Relatorio/RelatorioAtendimentos";
        private const string RelatorioMovimentacaoMaterialPath = "/Relatorio/RelatorioMovimentacaoMaterial";
        private const string RelatorioInventarioPorUnidadePath = "/Relatorio/RelatorioInventarioPorUnidade";
        private const string RelatorioInconsistenciasPath = "/Relatorio/RelatorioInconsistencias";

        public static string InconsistenciasUrl()
        {
            return $"{Schema}://{Domain}{Port}{Subpath}{RelatorioInconsistenciasPath}";
        }
        public static string InventarioPorUnidadeUrl(int pIdEstabelecimento, DateTime pDataInicial, DateTime pDataFinal)
        {
            return $"{Schema}://{Domain}{Port}{Subpath}{RelatorioInventarioPorUnidadePath}" + QueryString(new Dictionary<string, string>
            {
                ["pDataInicial"] = pDataInicial.ToString(@"yyyy\-MM\-dd", CultureInfo.InvariantCulture),
                ["pDataFinal"] = pDataFinal.ToString(@"yyyy\-MM\-dd", CultureInfo.InvariantCulture),
                ["pIdEstabelecimento"] = pIdEstabelecimento.ToString()
            });
        }
        public static string SolucoesUrl(int pIdProblema, DateTime pDataInicial, DateTime pDataFinal)
        {
            return $"{Schema}://{Domain}{Port}{Subpath}{RelatorioSolucoesPath}" + QueryString(new Dictionary<string, string>
            {
                ["pDataInicial"] = pDataInicial.ToString(@"yyyy\-MM\-dd", CultureInfo.InvariantCulture),
                ["pDataFinal"] = pDataFinal.ToString(@"yyyy\-MM\-dd", CultureInfo.InvariantCulture),
                ["pIdProblema"] = pIdProblema.ToString()
            });
        }
        public static string AtendimentosUrl(int pIdEstabelecimento, int pIdSetor, long? identificadorMaterial, DateTime pDataInicial, DateTime pDataFinal)
        {
            return $"{Schema}://{Domain}{Port}{Subpath}{RelatorioAtendimentosPath}" + QueryString(new Dictionary<string, string>
            {
                ["pDataInicial"] = pDataInicial.ToString(@"yyyy\-MM\-dd", CultureInfo.InvariantCulture),
                ["pDataFinal"] = pDataFinal.ToString(@"yyyy\-MM\-dd", CultureInfo.InvariantCulture),
                ["pIdEstabelecimento"] = pIdEstabelecimento.ToString(),
                ["pIdSetor"] = pIdSetor.ToString(),
                ["pIdentificadorMaterial"] = identificadorMaterial?.ToString(),
            });
        }
        public static string MovimentacaoMaterialUrl(int pIdTipoMovimentacao, int pIdNatureza, DateTime pDataInicial, DateTime pDataFinal)
        {
            return $"{Schema}://{Domain}{Port}{Subpath}{RelatorioMovimentacaoMaterialPath}" + QueryString(new Dictionary<string, string>
            {
                ["pDataInicial"] = pDataInicial.ToString(@"yyyy\-MM\-dd", CultureInfo.InvariantCulture),
                ["pDataFinal"] = pDataFinal.ToString(@"yyyy\-MM\-dd", CultureInfo.InvariantCulture),
                ["pIdTipoMovimentacao"] = pIdTipoMovimentacao.ToString(),
                ["pIdNatureza"] = pIdNatureza.ToString()
            });
        }
        private static string QueryString(Dictionary<string, string> dictionary)
        {
            return "?" + string.Join("&", dictionary.Keys.Where(x => !string.IsNullOrWhiteSpace(dictionary[x])).Select(x => x + "=" + Uri.EscapeDataString(dictionary[x])).ToArray());
        }
    }
}
