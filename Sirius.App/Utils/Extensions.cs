﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using Xamarin.Forms;
using ZXing.Mobile;
using ZXing.Net.Mobile.Forms;

namespace Sirius.App
{
    public static class Extensions
    {
        public static bool IsEmpty(this string str) => string.IsNullOrWhiteSpace(str);
        public static bool IsNotEmpty(this string str) => !string.IsNullOrWhiteSpace(str);

        public static byte[] ReadToEnd(this System.IO.Stream stream)
        {
            long originalPosition = 0;

            if (stream.CanSeek)
            {
                originalPosition = stream.Position;
                stream.Position = 0;
            }

            try
            {
                byte[] readBuffer = new byte[4096];

                int totalBytesRead = 0;
                int bytesRead;

                while ((bytesRead = stream.Read(readBuffer, totalBytesRead, readBuffer.Length - totalBytesRead)) > 0)
                {
                    totalBytesRead += bytesRead;

                    if (totalBytesRead == readBuffer.Length)
                    {
                        int nextByte = stream.ReadByte();
                        if (nextByte != -1)
                        {
                            byte[] temp = new byte[readBuffer.Length * 2];
                            Buffer.BlockCopy(readBuffer, 0, temp, 0, readBuffer.Length);
                            Buffer.SetByte(temp, totalBytesRead, (byte)nextByte);
                            readBuffer = temp;
                            totalBytesRead++;
                        }
                    }
                }

                byte[] buffer = readBuffer;
                if (readBuffer.Length != totalBytesRead)
                {
                    buffer = new byte[totalBytesRead];
                    Buffer.BlockCopy(readBuffer, 0, buffer, 0, totalBytesRead);
                }
                return buffer;
            }
            finally
            {
                if (stream.CanSeek)
                {
                    stream.Position = originalPosition;
                }
            }
        }

        public static string ReadBase64(this System.IO.Stream stream)
        {
            return Convert.ToBase64String(ReadToEnd(stream));
        }

        public static string ApenasNumeros(this string numeros)
        {
            return new string(numeros.Where(c => char.IsDigit(c)).ToArray());
        }

        public static long? TryToLong(this string numeros)
        {
            try
            {
                return long.Parse(numeros);
            }
            catch
            {
                return null;
            }
        }

        public static List<T> CloneAdd<T>(this List<T> list, T obj)
        {
            var newList = list.ToList();
            newList.Insert(0, obj);
            return newList;
        }

        private class CustomZXingScannerPage : ZXingScannerPage
        {
            public static readonly BindableProperty GoBackCommandProperty =
                BindableProperty.Create("GoBackCommand", typeof(ICommand), typeof(CustomZXingScannerPage), null);
            public CustomZXingScannerPage(MobileBarcodeScanningOptions options = null, View customOverlay = null) : base(options, customOverlay)
            {
            }

            public ICommand GoBackCommand
            {
                get { return (ICommand)GetValue(GoBackCommandProperty); }
                set { SetValue(GoBackCommandProperty, value); }
            }

            protected override bool OnBackButtonPressed()
            {
                if (GoBackCommand.CanExecute(null))
                    GoBackCommand.Execute(null);
                return true;
            }
        }
        private async static Task InternalScanBarcode(Controller controller, Action<ZXing.Result> onResult)
        {
            try
            {
                var options = new MobileBarcodeScanningOptions
                {
                    AutoRotate = false,
                    UseFrontCameraIfAvailable = false,
                    TryHarder = true,
                    PossibleFormats = new List<ZXing.BarcodeFormat>
                    {
                       ZXing.BarcodeFormat.ITF,
                       ZXing.BarcodeFormat.CODE_39
                    }
                };
                var scanPage = new CustomZXingScannerPage(options)
                {
                    DefaultOverlayTopText = string.Empty,
                    DefaultOverlayBottomText = string.Empty,
                    DefaultOverlayShowFlashButton = true,
                    Title = "Leitor de QR Code",
                    ControlTemplate = Application.Current.Resources["CameraPage"] as ControlTemplate,
                    GoBackCommand = new Command(async () =>
                    {
                        await controller.Owner.Navigation.PopAsync();
                        onResult(null);
                    })
                };
                NavigationPage.SetHasNavigationBar(scanPage, false);
                scanPage.OnScanResult += (result) =>
                {
                    // Stop scanning
                    scanPage.IsScanning = false;

                    // Pop the page and show the result
                    Xamarin.Forms.Device.BeginInvokeOnMainThread(async () =>
                    {
                        await controller.Owner.Navigation.PopAsync();
                        onResult(result);
                    });
                };
                await controller.Owner.Navigation.PushAsync(scanPage);
            }
            catch
            {
                onResult(null);
            }
        }
        private static async Task<Task<ZXing.Result>> InternalScanBarcode(Controller controller)
        {
            TaskCompletionSource<ZXing.Result> taskSource = new TaskCompletionSource<ZXing.Result>();
            await InternalScanBarcode(controller, (result) =>
            {
                taskSource.SetResult(result);
            });
            return taskSource.Task;
        }
        public static async Task<ZXing.Result> ScanBarcode(this Controller controller)
        {
            return await (await InternalScanBarcode(controller));
        }
    }
}
