﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace Sirius.App.Utils
{
    public class NumeroCodigoBarBehavior : Behavior<VEntry>    
    {
        private int _EntryLenght = 6;
        public int EntryLenght
        {
            get => _EntryLenght;
            set
            {
                _EntryLenght = value;
            }
        }

        protected override void OnAttachedTo(VEntry entry)
        {
            entry.TextChanged += OnEntryTextChanged;
            base.OnAttachedTo(entry);
        }

        protected override void OnDetachingFrom(VEntry entry)
        {
            entry.TextChanged -= OnEntryTextChanged;
            base.OnDetachingFrom(entry);
        }

        private void OnEntryTextChanged(object sender, TextChangedEventArgs args)
        {
            var entry = sender as VEntry;

            var text = entry.Text;

            if (string.IsNullOrWhiteSpace(text))
                return;
            if (text.Length > EntryLenght)
            {
                entry.Text = text.Remove(text.Length - 1);
                return;
            }

            if (text.Length >= _EntryLenght)
            {
                entry.TextColor = Color.White;
            }
            else
            {
                entry.TextColor = Color.Red;
            }

            if (entry.Text != text)
                entry.Text = text;
        }
    }
}
