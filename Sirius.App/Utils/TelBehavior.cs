﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace Sirius.App.Utils
{ 
    public class TelBehavior : Behavior<Entry>
    {

        private string _mask = "(XX) XXXXXXXXX";
        public string Mask
        {
            get => _mask;
            set
            {
                _mask = value;
                SetPositions();
            }
        }
        private int _EntryLenght = 13;
        public int EntryLenght
        {
            get => _EntryLenght;
            set
            {
                _EntryLenght = value;
                SetPositions();
            }
        }


        protected override void OnAttachedTo(Entry entry)
        {
            entry.TextChanged += OnEntryTextChanged;
            base.OnAttachedTo(entry);
        }

        protected override void OnDetachingFrom(Entry entry)
        {
            entry.TextChanged -= OnEntryTextChanged;
            base.OnDetachingFrom(entry);
        }

        IDictionary<int, char> _positions;

        private bool _FirstTime = false;
        void SetPositions()
        {
            if (string.IsNullOrEmpty(Mask))
            {
                _positions = null;
                return;
            }

            var list = new Dictionary<int, char>();
            for (var i = 0; i < Mask.Length; i++)
                if (Mask[i] != 'X')
                    list.Add(i, Mask[i]);

            _positions = list;
            _FirstTime = true;
        }

        private void OnEntryTextChanged(object sender, TextChangedEventArgs args)
        {
            if (!_FirstTime) { SetPositions(); }
            var entry = sender as Entry;

            var text = entry.Text;

            if (string.IsNullOrWhiteSpace(text) || _positions == null)
                return;

       
            if (text.Length >(EntryLenght+1))
            {
                entry.Text = text.Remove(text.Length - 1);
                return;
            }

            foreach (var position in _positions)
                if (text.Length >= position.Key + 1)
                {
                    var value = position.Value.ToString();
                    if (text.Substring(position.Key, 1) != value)
                        text = text.Insert(position.Key, value);
                }

           

            if (text.Length >= _EntryLenght)
            {
            entry.TextColor = Color.White;
            }
            else
            {
                entry.TextColor = Color.Red;
            }

            if (entry.Text != text)
                entry.Text = text;
            return;
        }
    }
}