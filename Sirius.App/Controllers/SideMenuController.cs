﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using Xamarin.Forms;

namespace Sirius.App
{
    public class SideMenuController : Controller
    {
        public SideMenuController(VContentPage owner) : base(owner)
        {
            this.InventarioCommand = CreateCommand(async () => await Inventario());
            this.MovimentoCommand = CreateCommand(async () => await Movimento());
            this.MaterialCommand = CreateCommand(async () => await Material());
            //this.OrdemServicoCommand = CreateCommand(async () => await OrdemServico());
            this.ConsultaCommand = CreateCommand(async () => await Consulta());
            this.RelatoriosCommand = CreateCommand(async () => await Relatorios());
            this.AlterarSenhaCommand = CreateCommand(async () => await AlterarSenha());
            //this.SincronizarCommand = CreateCommand(async () => await Sincronizar());
            this.SairCommand = CreateCommand(async () => await Sair());
        }

        //private async Task OrdemServico()
        //{
        //    var master = (Owner.Parent as Xamarin.Forms.MasterDetailPage);
        //    var nav = master.Detail as NavigationPage;
        //    master.IsPresented = false;
        //    await nav.PushAsync(new AdicionarMaterialOrdemServicoPage());
        //}

        private async Task Material()
        {
            var master = (Owner.Parent as Xamarin.Forms.MasterDetailPage);
            var nav = master.Detail as NavigationPage;
            master.IsPresented = false;
            await nav.Navigation.PushAsync(new MaterialPage());
        }

        private async Task Sair()
        {
            await Business.Current.LogOff();
            await Owner.Navigation.PushAsync(new LoginPage());
            List<Page> toBeRemoved = Owner.Navigation.NavigationStack.Where(p => !(p is LoginPage)).ToList();
            foreach (var page in toBeRemoved)
                Owner.Navigation.RemovePage(page);
        }

        //private async Task Sincronizar()
        //{
        //    await DisplayError("Erro ao tentar sincronizar");
        //}

        private async Task AlterarSenha()
        {
            var master = (Owner.Parent as Xamarin.Forms.MasterDetailPage);
            var nav = master.Detail as NavigationPage;
            master.IsPresented = false;
            await nav.Navigation.PushAsync(new AlterarSenhaPage());
        }

        private async Task Relatorios()
        {
            var master = (Owner.Parent as Xamarin.Forms.MasterDetailPage);
            var nav = master.Detail as NavigationPage;
            master.IsPresented = false;
            await nav.Navigation.PushAsync(new RelatorioListaPage());
        }

        private async Task Consulta()
        {
            var master = (Owner.Parent as Xamarin.Forms.MasterDetailPage);
            var nav = master.Detail as NavigationPage;
            master.IsPresented = false;
            await nav.Navigation.PushAsync(new PesquisarOrdemServicoPage());
        }

        private async Task Movimento()
        {
            var master = (Owner.Parent as Xamarin.Forms.MasterDetailPage);
            var nav = master.Detail as NavigationPage;
            master.IsPresented = false;
            await nav.Navigation.PushAsync(new MovimentacaoListaPage());
        }

        private async Task Inventario()
        {
            var master = (Owner.Parent as Xamarin.Forms.MasterDetailPage);
            var nav = master.Detail as NavigationPage;
            master.IsPresented = false;
            await nav.Navigation.PushAsync(new ListaInventariosPage());
        }

        public string NomeUsuario => Business.Current.UsuarioLogado.Nome;
        public ICommand InventarioCommand { get; set; }
        public ICommand MovimentoCommand { get; set; }
        public ICommand MaterialCommand { get; set; }
        //public ICommand OrdemServicoCommand { get; set; }
        public ICommand ConsultaCommand { get; set; }
        public ICommand RelatoriosCommand { get; set; }
        public ICommand AlterarSenhaCommand { get; set; }
        //public ICommand SincronizarCommand { get; set; }
        public ICommand SairCommand { get; set; }
    }
}
