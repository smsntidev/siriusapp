﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

namespace Sirius.App
{
    public class MovimentacaoListaController : Controller
    {
        public MovimentacaoListaController(VContentPage owner) : base(owner)
        {
            PesquisarCodigoBarrasCommand = CreateCommand(async () => await PesquisarCodigoBarras());
            BuscarMovimentacoesCommand = CreateCommand(async () => { await BuscarMovimentacoes(); });
            ExibirMovimentacaoCommand = CreateCommand(async (Movimentacao m) => { await ExibirMovimentacao(m); });
            CriarMovimentacaoCommand = CreateCommand(async () => { await CriarMovimentacao(); });
        }

        protected override Task OnAppearing()
        {
            return Protected(async () =>
            {
                await CarregarTiposBuscaMaterial();
            }, ProtectedFlags.SetLoading);
        }

        //protected override Task OnShowing()
        //{
        //    return Protected(async () =>
        //    {
        //        if (TipoBuscaMaterial != null && CodigoMaterialPesquisa != null && Movimentacoes != null && Movimentacoes.Count != 0)
        //            await BuscarMovimentacoes();
        //    }, ProtectedFlags.SetLoading);
        //}

        private TipoBuscaMaterial _TipoBuscaMaterial;
        public TipoBuscaMaterial TipoBuscaMaterial
        {
            get => _TipoBuscaMaterial;

            set
            {
                if (_TipoBuscaMaterial != value)
                {
                    _TipoBuscaMaterial = value;
                    FirePropertyChanged("TipoBuscaMaterial");
                }
            }
        }

        private List<TipoBuscaMaterial> _TiposBuscaMaterial = new List<TipoBuscaMaterial>();
        public List<TipoBuscaMaterial> TiposBuscaMaterial
        {
            get => _TiposBuscaMaterial;
            set => SetProperty(ref _TiposBuscaMaterial, value);
        }

        private string _CodigoMaterialPesquisa = string.Empty;
        public string CodigoMaterialPesquisa
        {
            get => _CodigoMaterialPesquisa;
            set
            {
                if (_CodigoMaterialPesquisa != value)
                {

                    _CodigoMaterialPesquisa = value;
                    Validar();
                    FirePropertyChanged("CodigoMaterialPesquisa");
                }
            }
        }

        private List<Movimentacao> _Movimentacoes;
        public List<Movimentacao> Movimentacoes
        {
            get => _Movimentacoes;
            set => SetProperty(ref _Movimentacoes, value);
        }

        private bool _PesquisandoMovimentacao = false;
        public bool PesquisandoMovimentacao
        {
            get => _PesquisandoMovimentacao;
            set => SetProperty(ref _PesquisandoMovimentacao, value, linkedPropertyName: "NotPesquisandoMovimentacao");
        }
        public bool NotPesquisandoMovimentacao => !PesquisandoMovimentacao;

        private async Task PesquisarCodigoBarras()
        {
            await Protected(async () =>
            {
                if (!HasAppeared)
                    return;
                try
                {
                    var result = await this.ScanBarcode();
                    if (result == null)
                        throw new Exception("Código de barras não pode ser lido.");

                    string materialId = result.Text;
                    
                    if (materialId.Length == 8)
                    {
                        //tipoMaterial = 0;
                        materialId = materialId.Substring(2, 6);
                    }
                    CodigoMaterialPesquisa = materialId;
                    //else
                    //{
                    //    if (materialId[0] == '4')
                    //        tipoMaterial = 2;
                    //    else
                    //        tipoMaterial = 1;
                    //}

                    //var movimentacoes = await Business.Current.GetMovimentacoesByFiltro(materialId, TipoBuscaMaterialCodigoBarras.Codigo);
                    //if (movimentacoes.Count == 0) throw new Exception("Nenhuma Movimentação foi encontrada.");

                    //Movimentacoes = movimentacoes;
                }
                finally
                {
                    PesquisandoMovimentacao = false;
                }
            }, ProtectedFlags.SetLoading);
        }
        private bool _TipoBuscaPesquisaCodigoBarras = false;
        public bool TipoBuscaPesquisaCodigoBarras
        {
            get => _TipoBuscaPesquisaCodigoBarras;

            set
            {
                if (_TipoBuscaPesquisaCodigoBarras != value)
                {
                    _TipoBuscaPesquisaCodigoBarras = value;
                    FirePropertyChanged("TipoBuscaPesquisaCodigoBarras");
                }
            }
        }
        private TipoBuscaMaterial _TipoBuscaMaterialCodigoBarras;
        public TipoBuscaMaterial TipoBuscaMaterialCodigoBarras
        {
            get => _TipoBuscaMaterialCodigoBarras;

            set
            {
                if (_TipoBuscaMaterialCodigoBarras != value)
                {
                    _TipoBuscaMaterialCodigoBarras = value;
                    TipoBuscaPesquisaCodigoBarras = true;
                    Validar();
                    FirePropertyChanged("TipoBuscaMaterialCodigoBarras");

                }
            }
        }
        private async Task BuscarMovimentacoes()
        {
            await Protected(async () =>
            {
                if (!HasAppeared) return;
                try
                {
                    Validate.Condition(TipoBuscaMaterialCodigoBarras != null, "O campo Tipo precisa ser preenchido.");
                    Validate.NotEmpty(CodigoMaterialPesquisa, "O campo Número precisa ser preenchido.");
                    Validate.StringLength(CodigoMaterialPesquisa, 6, "O campo Número precisa ter 6 digitos.");
                    //if (TipoBuscaMaterialCodigo == null) throw new Exception("Selecione o Tipo");
                    //if (string.IsNullOrEmpty(CodigoMaterialPesquisa)) throw new Exception("Digite o Código");

                    PesquisandoMovimentacao = true;
                    var result = await Business.Current.GetMovimentacoesByFiltro(CodigoMaterialPesquisa, TipoBuscaMaterialCodigoBarras.Codigo);
                    if (result.Count == 0) throw new Exception("Nenhuma Movimentação foi encontrada.");

                    Movimentacoes = result;
                }
                catch (Exception e)
                {
                    await DisplayError(e.Message);
                }
                finally
                {
                    PesquisandoMovimentacao = false;
                }
            }, ProtectedFlags.SetLoading);
        }
        private void Validar()
        {
            if (_CodigoMaterialPesquisa != string.Empty)
            {
                CameraIsVisible = false;
                LupaIsVisible = true;
            }
            else
            {
                CameraIsVisible = true;
                LupaIsVisible = false;
            }
        }
        private bool _CameraIsVisible = true;
        public bool CameraIsVisible
        {
            get => _CameraIsVisible;

            set
            {
                if (_CameraIsVisible != value)
                {
                    _CameraIsVisible = value;
                    FirePropertyChanged("CameraIsVisible");
                }
            }
        }
        private bool _LupaIsVisible = false;
        public bool LupaIsVisible
        {
            get => _LupaIsVisible;

            set
            {
                if (_LupaIsVisible != value)
                {
                    _LupaIsVisible = value;
                    FirePropertyChanged("LupaIsVisible");
                }
            }
        }
        private async Task ExibirMovimentacao(Movimentacao movimentacao)
        {
            await Owner.Navigation.PushAsync(new MovimentacaoPage(movimentacao));
        }
        private async Task CriarMovimentacao()
        {
            await Owner.Navigation.PushAsync(new MovimentacaoPage());
        }
        public Task CarregarTiposBuscaMaterial()
        {
            TiposBuscaMaterial = new List<TipoBuscaMaterial>()
            {
                new TipoBuscaMaterial(TipoBuscaMaterial.NumeroPatrimonialCodigo, TipoBuscaMaterial.NumeroPatrimonial),
                new TipoBuscaMaterial(TipoBuscaMaterial.IBTCodigo, TipoBuscaMaterial.IBT),
                new TipoBuscaMaterial(TipoBuscaMaterial.IPBMCodigo, TipoBuscaMaterial.IPBM),
            };
            return Task.FromResult(true);
        }

        public ICommand BuscarMovimentacoesCommand { get; set; }
        public ICommand ExibirMovimentacaoCommand { get; set; }
        public ICommand CriarMovimentacaoCommand { get; set; }
        public ICommand PesquisarCodigoBarrasCommand { get; set; }
    }
}
