﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

namespace Sirius.App
{
    public class MovimentacaoCriadaController : Controller
    {
        public MovimentacaoCriadaController(VContentPage owner, int numeroMovimentacao, bool newMovimentacao = false) : base(owner)
        {
            NumeroMovimentacao = "Número " + numeroMovimentacao.ToString();
            AdicionarMovimentacaoCommand = CreateCommand(async () => { await AdicionarMovimentacao(); });

            if (newMovimentacao)
                Message = "MOVIMENTAÇÃO CRIADA COM SUCESSO";
            else
                Message = "MOVIMENTAÇÃO ATUALIZADA COM SUCESSO";
        }

        private string _NumeroMovimentacao;
        public string NumeroMovimentacao
        {
            get => _NumeroMovimentacao;
            set => SetProperty(ref _NumeroMovimentacao, value);
        }

        private string _Message;
        private string Message
        {
            get => _Message;
            set => SetProperty(ref _Message, value);
        }

        private async Task AdicionarMovimentacao()
        {
            await Owner.Navigation.PushAsync(new MovimentacaoPage());
            Owner.Navigation.RemovePage(Owner);
        }

        public ICommand AdicionarMovimentacaoCommand { get; set; }
    }
}
