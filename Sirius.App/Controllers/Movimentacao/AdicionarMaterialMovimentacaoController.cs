﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

namespace Sirius.App
{
    public class AdicionarMaterialMovimentacaoController : Controller
    {
        public AdicionarMaterialMovimentacaoController(VContentPage owner, Estabelecimento estabelecimentoOrigem, Estabelecimento estabelecimentoDestino, Movimentacao movimentacao, Action<Item> onAdd) : base(owner)
        {
            this.estabelecimentoOrigem = estabelecimentoOrigem;

            if(estabelecimentoDestino!= null)
            {
                this.estabelecimentoDestino = estabelecimentoDestino;
                PesquisarSetores();

            }            
            this.movimentacao = movimentacao;
            this.PesquisarCodigoBarrasCommand = CreateCommand(async () => await PesquisarCodigoBarras());
            this.PesquisarMaterialCommand = CreateCommand(async () => await PesquisarMaterial());
            this.IncluirItemCommand = CreateCommand(async () => await IncluirItem());
            this.OnAdd = onAdd;


        }

        protected override Task OnAppearing()
        {
            return Protected(async () =>
            {
                CarregarTiposBuscaMaterial();
                SituacaoFisicaLista = await Business.Current.GetSituacoesFisicas();
            }, ProtectedFlags.SetLoading);
        }
        public Estabelecimento estabelecimentoOrigem { get; set; }
        public Estabelecimento estabelecimentoDestino { get; set; }
        public Movimentacao movimentacao { get; set; }
        public Action<Item> OnAdd { get; }

        

        private bool _MostrarMaterial = false;
        public bool MostrarMaterial
        {
            get => _MostrarMaterial;
            set
            {
                if (_MostrarMaterial != value)
                {
                    _MostrarMaterial = value;
                    FirePropertyChanged("MostrarMaterial");
                }
            }
        }
        private bool _TipoBuscaPesquisaCodigoBarras = false;
        public bool TipoBuscaPesquisaCodigoBarras
        {
            get => _TipoBuscaPesquisaCodigoBarras;

            set
            {
                if (_TipoBuscaPesquisaCodigoBarras != value)
                {
                    _TipoBuscaPesquisaCodigoBarras = value;
                    FirePropertyChanged("TipoBuscaPesquisaCodigoBarras");
                }
            }
        }
       

        private TipoBuscaMaterial _TipoBuscaMaterialCodigo;
        public TipoBuscaMaterial TipoBuscaMaterialCodigo
        {
            get => _TipoBuscaMaterialCodigo;
            set
            {
                if (_TipoBuscaMaterialCodigo != value)
                {
                    _TipoBuscaMaterialCodigo = value;
                    Validar();
                    FirePropertyChanged("TipoBuscaMaterialCodigoBarras");

                }
            }
        }

        private List<TipoBuscaMaterial> _TiposBuscaMaterial = new List<TipoBuscaMaterial>();
        public List<TipoBuscaMaterial> TiposBuscaMaterial
        {
            get => _TiposBuscaMaterial;
            set => SetProperty(ref _TiposBuscaMaterial, value);
        }

        private string _CodigoMaterialPesquisa = string.Empty;
        public string CodigoMaterialPesquisa
        {
            get => _CodigoMaterialPesquisa;
            set
            {
                if (_CodigoMaterialPesquisa != value)
                {

                    _CodigoMaterialPesquisa = value;
                    Validar();
                    FirePropertyChanged("CodigoMaterialPesquisa");
                }
            }
        }

        private bool _PesquisandoMaterial = false;
        public bool PesquisandoMaterial
        {
            get => _PesquisandoMaterial;
            set => SetProperty(ref _PesquisandoMaterial, value, linkedPropertyName: "NotPesquisandoMaterial");
        }

        public bool NotPesquisandoMaterial => !PesquisandoMaterial;

        private bool _SetorDestinoIsVisible = false;
        public bool SetorDestinoIsVisible
        {
            get => _SetorDestinoIsVisible;
            set => SetProperty(ref _SetorDestinoIsVisible, value);
        }
        
        private Item _Item = null;
        public Item Item
        {
            get => _Item;
            set
            {
                SetProperty(ref _Item, value, action: () =>
                {
                    //CodigoMaterial = value.CodigoMaterial;
                    CodigoMaterial = value.Material.Identificador;
                    NomeMaterial = value.Material.NomeMaterial;
                    ClasseMaterial = value.Material.NomeClasse;
                    SetorOrigem = value.NomeSetor;
                    SituacaoMaterial = SituacaoFisicaLista.FirstOrDefault(a => a.Codigo == value.CodigoSituacaoFisica);
                    MostrarMaterial = _Item != null;
                    EntrySetorIsVisible = (_Item != null && estabelecimentoDestino != null);
                    SetorDestinoIsVisible = estabelecimentoDestino != null;

                });
            }
        }

        //private int? _CodigoMaterial;
        //public int? CodigoMaterial
        //{
        //    get { return _CodigoMaterial; }
        //    set => SetProperty(ref _CodigoMaterial, value);
        //}
        private long? _CodigoMaterial;
        public long? CodigoMaterial
        {
            get { return _CodigoMaterial; }
            set => SetProperty(ref _CodigoMaterial, value);
        }

        private string _NomeMaterial;
        public string NomeMaterial
        {
            get { return _NomeMaterial; }
            set => SetProperty(ref _NomeMaterial, value);
        }

        private string _ClasseMaterial;
        public string ClasseMaterial
        {
            get { return _ClasseMaterial; }
            set => SetProperty(ref _ClasseMaterial, value);
        }

        private List<SituacaoFisica> _SituacaoFisicaLista = new List<SituacaoFisica>();
        public List<SituacaoFisica> SituacaoFisicaLista
        {
            get => _SituacaoFisicaLista;
            set => SetProperty(ref _SituacaoFisicaLista, value);
        }

        private SituacaoFisica _SituacaoMaterial = null;
        public SituacaoFisica SituacaoMaterial
        {
            get { return _SituacaoMaterial; }
            set => SetProperty(ref _SituacaoMaterial, value, action: () =>
            {
                if (Item != null)
                {
                    Item.SituacaoFisica = value.Nome;
                    Item.CodigoSituacaoFisica = value.Codigo;
                }
            });
        }

        private bool _CarregandoSituacoesFisicas = false;
        public bool CarregandoSituacoesFisicas
        {
            get => _PesquisandoMaterial;
            set => SetProperty(ref _CarregandoSituacoesFisicas, value, linkedPropertyName: "NotCarregandoSituacoesFisicas");
        }

        public bool NotCarregandoSituacoesFisicas => !CarregandoSituacoesFisicas;

        //private IList<Setor> _Setores = new List<Setor>();
        //public IList<Setor> Setores
        //{
        //    get { return _Setores = new List<Setor>(); }
        //    set => SetProperty(ref _Setores, value);
        //}


        private bool _CameraIsVisible = true;
        public bool CameraIsVisible
        {
            get => _CameraIsVisible;

            set
            {
                if (_CameraIsVisible != value)
                {
                    _CameraIsVisible = value;
                    FirePropertyChanged("CameraIsVisible");
                }
            }
        }
        private bool _LupaIsVisible = false;
        public bool LupaIsVisible
        {
            get => _LupaIsVisible;

            set
            {
                if (_LupaIsVisible != value)
                {
                    _LupaIsVisible = value;
                    FirePropertyChanged("LupaIsVisible");
                }
            }
        }
        public List<Setor> TodosSetores { set; get; }
        private List<Setor> _Setores = new List<Setor>();
        public List<Setor> Setores
        {
            get => _Setores.Where(x => x.Excluido == false).ToList();
            set
            {
                if (_Setores != value)
                {
                    _Setores = value;
                    FirePropertyChanged("Setores");
                }
            }
        }

        private Setor _SetorDestinoSelectedItem = null;
        public Setor SetorDestinoSelectedItem
        {
            get => _SetorDestinoSelectedItem;
            set
            {               
                if (value == null)
                    return;
                SetProperty(ref _SetorDestinoSelectedItem, value, action: () =>
                {                   
                    if (HasAppeared)
                    {
                        _SetorDestinoSelectedItem = value;
                        EntrySetor = _SetorDestinoSelectedItem.Nome;
                        ListViewSetoresIsVisible = false;
                    }
                });
            }
        }
        private bool _ListViewSetoresIsVisible = false;
        public bool ListViewSetoresIsVisible
        {
            get { return _ListViewSetoresIsVisible; }
            set { SetProperty(ref _ListViewSetoresIsVisible, value); }
        }
        private bool _EntrySetorIsVisible = false;
        public bool EntrySetorIsVisible
        {
            get { return _EntrySetorIsVisible; }
            set { SetProperty(ref _EntrySetorIsVisible, value); }
        }
        private string _EntrySetor = string.Empty;
        public string EntrySetor
        {

            get => _EntrySetor;
            set
            {
                if (_EntrySetor != value)
                {
                    _EntrySetor = value;
                    CarregarBuscaSetor();
                    FirePropertyChanged("EntrySetor");
                    if (value == string.Empty)
                    {
                        ListViewSetoresIsVisible = false;
                    }
                }
            }
        }
        private int _HeightRequestListViewSetor = 0;
        public int HeightRequestListViewSetor
        {
            get { return _HeightRequestListViewSetor; }
            set { SetProperty(ref _HeightRequestListViewSetor, value); }
        }
        private async void CarregarBuscaSetor()
        {
            await Protected(async () =>
            {
                    Setores = TodosSetores.Where(f => f.Nome.ToLower().Contains(_EntrySetor.ToLower())).ToList();
                    //mostrar aviso de lista vazia
                    HeightRequestListViewSetor = (Setores.Count < 10 ? Setores.Count * 25 : 150);
                    ListViewSetoresIsVisible = (Setores.Count > 0);
               
            });
        }

        private string _SetorOrigem;
        public string SetorOrigem
        {
            get { return _SetorOrigem; }
            set => SetProperty(ref _SetorOrigem, value);
        }

        private async void PesquisarSetores()
        {
            try
            {
                Setores = TodosSetores = await Business.Current.GetSetorPorEstabelecimento(estabelecimentoDestino.Codigo);                  
            }
            catch (Exception)
            {

                throw;
            }
        }

            private Task PesquisarCodigoBarras()
        {
            return Protected(async () =>
            {
                var result = await this.ScanBarcode();
                if (result == null)
                    throw new Exception("Código de barras não pode ser lido.");

                PesquisandoMaterial = true;
                try
                {
                    string materialId = result.Text;
                    
                    if (materialId.Length == 8)
                    {
                        materialId = materialId.Substring(2, 6);
                    }
                    CodigoMaterialPesquisa = materialId;
                    //var result2 = await Business.Current.GetItemPorCodigoBarras(result.Text);
                    //var result2 = await Business.Current.GetItemPorMaterial(materialId, TipoBuscaMaterialCodigoBarras.Codigo);
                    //if (result2 == null)
                    //    throw new Exception("Nenhum Material foi encontrado");

                    //Item = result2;
                }
                finally
                {
                    PesquisandoMaterial = false;
                }
            }, ProtectedFlags.SetLoading);
        }

        private void Validar()
        {
            if (_CodigoMaterialPesquisa != string.Empty)
            {
                CameraIsVisible = false;
                LupaIsVisible = true;
            }
            else
            {
                CameraIsVisible = true;
                LupaIsVisible = false;
            }
        }

        private async Task PesquisarMaterial()
        {
            await Protected(async () =>
            {
                if (!HasAppeared) return;
                try
                {
                    Validate.Condition(TipoBuscaMaterialCodigo != null, "O campo Tipo precisa ser preenchido.");
                    Validate.NotEmpty(CodigoMaterialPesquisa, "O campo Número precisa ser preenchido.");
                    Validate.StringLength(CodigoMaterialPesquisa, 6, "O campo Número precisa ter 6 digitos.");
                    //if (TipoBuscaMaterialCodigo == null) throw new Exception("Selecione o Tipo");
                    //if (string.IsNullOrEmpty(CodigoMaterialPesquisa)) throw new Exception("Digite o Código");

                    PesquisandoMaterial = true;
                    var result = await Business.Current.GetItemPorMaterial(CodigoMaterialPesquisa, TipoBuscaMaterialCodigo.Codigo);
                    if (result == null) throw new Exception("Nenhum Material foi encontrado");
                    if (estabelecimentoOrigem.Codigo == result.CodigoEstabelecimento)
                    {
                        Item = result;
                    }
                    else
                    {
                        await DisplayError("Este item não pertence ao estabelecimento Origem.");
                    }
                }
                catch (Exception e)
                {
                    await DisplayError(e.Message);    

                }
                finally
                {
                    PesquisandoMaterial = false;
                }
            });
        }

        private async void CarregarSituacoesFisicas()
        {
            await Protected(async () =>
            {
                if (!HasAppeared)
                    return;

                CarregandoSituacoesFisicas = true;
                try
                {
                    SituacaoFisicaLista = await Business.Current.GetSituacoesFisicas();
                }
                finally
                {
                    CarregandoSituacoesFisicas = false;
                }
            });
        }

        private async Task IncluirItem()
        {
            if (SetorDestinoSelectedItem == null && estabelecimentoDestino != null)
            {
                await DisplayError("O campo Setor de Destino precisa ser preenchido.");
                return;                
            }

            OnAdd(Item);
            await Owner.Navigation.PopAsync();       
            

        }

        public void CarregarTiposBuscaMaterial()
        {
            TiposBuscaMaterial = new List<TipoBuscaMaterial>()
            {
                new TipoBuscaMaterial(TipoBuscaMaterial.NumeroPatrimonialCodigo, TipoBuscaMaterial.NumeroPatrimonial),
                new TipoBuscaMaterial(TipoBuscaMaterial.IBTCodigo, TipoBuscaMaterial.IBT),
                new TipoBuscaMaterial(TipoBuscaMaterial.IPBMCodigo, TipoBuscaMaterial.IPBM),
           };
        }

        public ICommand PesquisarMaterialCommand { get; set; }
        public ICommand IncluirItemCommand { get; set; }
        public ICommand PesquisarCodigoBarrasCommand { get; set; }
    }
}
