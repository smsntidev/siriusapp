﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

namespace Sirius.App
{
    public class RelatorioListaController : Controller
    {
        public RelatorioListaController(VContentPage owner) : base(owner)
        {
            SolucoesCommand = CreateCommand(async () => await Solucoes());
            AtendimentoCommand = CreateCommand(async () => await Atendimento());
            MovimentacaoCommand = CreateCommand(async () => await Movimentacao());
            InconsistenciasCommand = CreateCommand(async () => await Inconsistencias());
            InventarioUnidadeCommand = CreateCommand(async () => await InventarioUnidade());
        }

        private async Task InventarioUnidade()
        {
            await Owner.Navigation.PushAsync(new RelatorioInventarioUnidadePage());
        }

        private async Task Inconsistencias()
        {
            var url = Proxy.InconsistenciasUrl();
            await Owner.Navigation.PushAsync(new RelatorioPage("INCONSISTÊNCIAS", url));
        }

        private async Task Atendimento()
        {
            await Owner.Navigation.PushAsync(new RelatorioAtendimentoPage());
        }

        private async Task Solucoes()
        {
            await Owner.Navigation.PushAsync(new RelatorioSolucoesPage());
        }
        private async Task Movimentacao()
        {
            await Owner.Navigation.PushAsync(new RelatorioMovimentacaoMaterialPage());
        }

        public ICommand MovimentacaoCommand { get; }
        public ICommand SolucoesCommand { get; }
        public ICommand AtendimentoCommand { get; }
        public ICommand InventarioUnidadeCommand { get; }
        public ICommand InconsistenciasCommand { get; }
    }
}
