﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

namespace Sirius.App
{
    public class ListaInventariosController : Controller
    {
        public ListaInventariosController(VContentPage owner) : base(owner)
        {
            IsLoading = true;
            CarregarInventarioSelecionadoCommand = CreateCommand<Inventario>(async (Inventario) => await CarregarInventarioSelecionado(Inventario));
            AddCommand = CreateCommand(async () => { await CriarInventario(); });
        }

        private List<Inventario> _Inventarios = new List<Inventario>();
        public List<Inventario> Inventarios
        {
            get => _Inventarios.Where(x => x.Setor.Excluido == false && x.Estabelecimento.Excluido == false && x.ResponsavelAtesto.Excluido == false &&x.Fechado == false ).ToList();
            set
            {
                if (_Inventarios != value)
                {
                    _Inventarios = value;
                    FirePropertyChanged("Inventarios");
                }
            }
        }

        //protected override Task OnAppearing()
        //{
        //    return Protected(async () =>
        //    {                
        //        try
        //        {
        //            await CarregarListaInventarios();
        //        }
        //        finally
        //        {
                    
        //        }
        //    }, ProtectedFlags.SetLoading);
        //}

        protected override Task OnShowing()
        {
            return Protected(async () =>
            {
                try
                {
                    await CarregarListaInventarios();
                }
                finally
                {

                }
            }, ProtectedFlags.SetLoading);
        }

        private async Task CarregarListaInventarios()
        {
            await Protected(async () =>
            {
                if (!HasAppeared)
                    return;

                //CarregandoInventarios = true;
                try
                {
                    Inventarios = await Business.Current.GetListaInventarios();
                }
                finally
                {
                    //CarregandoInventarios = false;
                }
            });
        }

        private async Task CriarInventario()
        {
            await Owner.Navigation.PushAsync(new InventarioPage());
        }

        private async Task CarregarInventarioSelecionado(Inventario inventario)
        {
            if (inventario != null)
                await Owner.Navigation.PushAsync(new InventarioPage(inventario));
        }

        public ICommand CarregarInventariosCommand { get; set; }
        public ICommand CarregarInventarioSelecionadoCommand { get; set; }
        public ICommand AddCommand { get; set; }
    }
}
