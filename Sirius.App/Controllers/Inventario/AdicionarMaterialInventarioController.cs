﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using Xamarin.Forms;
using ZXing.Mobile;
using ZXing.Net.Mobile.Forms;

namespace Sirius.App
{
    public class AdicionarMaterialInventarioController : Controller
    {
        public AdicionarMaterialInventarioController(VContentPage owner, Inventario inventario, Action atualizar) : base(owner)
        {
            this.Inventario = inventario;
            this.AtualizarListaMateriais = atualizar;
            PesquisarMaterialCommand = CreateCommand(async () => { await PesquisarMaterial(); });
            IncluirItemCommand = CreateCommand(async () => { await IncluirItem(); });
            PesquisarCodigoBarrasCommand = CreateCommand(async () => { await PesquisarCodigoBarras(); });
            //CleanCommand = CreateCommand(async () => { await Clean(); });
        }
        
        private async Task PesquisarCodigoBarras()
        {
            await Protected(async () =>
            {

                if (!HasAppeared)
                    return;

                var result = await this.ScanBarcode();
                
                if (result == null)
                    throw new Exception("Código de barras não pode ser lido.");

                try
                {
                    string materialId = result.Text;

                    if (materialId.Length == 8)
                    {
                        materialId = materialId.Substring(2, 6);
                    }
                    CodigoMaterialPesquisa = materialId;
                    //var result2 = await Business.Current.GetItemPorCodigoBarras(result.Text);
                    //Item = await Business.Current.GetItemPorMaterial(materialId, TipoBuscaMaterialCodigoBarras.Codigo);
                    //if (Item == null)
                    //    throw new Exception();

                }
                catch
                {
                    await Owner.DisplayModal(new MaterialNaoEncontradoPage(async (itemDescricao) =>
                    {
                        await Protected(() =>
                        {
                            TipoBuscaMaterialCodigoBarras = Tipos.First(x => x.Codigo == itemDescricao.IdentificadorTipoId);
                            CodigoMaterialPesquisa = itemDescricao.Identificador;
                            Item = itemDescricao.Item;
                            return Task.FromResult(true);
                        });
                    }));
                }
            }, ProtectedFlags.SetLoading);
        }
        //private async Task Clean()
        //{
        //     CodigoMaterialPesquisa = string.Empty;

        //}

        private async Task PesquisarMaterial()
        {
            await Protected(async () =>
            {

                try
                {
                    Validate.Condition(TipoBuscaMaterialCodigoBarras != null, "O campo Tipo precisa ser preenchido.");
                    Validate.NotEmpty(CodigoMaterialPesquisa, "O campo Número precisa ser preenchido.");
                    Validate.StringLength(CodigoMaterialPesquisa, 6, "O campo Número precisa ter 6 digitos.");


                    Item = await Business.Current.GetItemPorMaterial(CodigoMaterialPesquisa, TipoBuscaMaterialCodigoBarras.Codigo);
                    if (Item == null)
                        throw new Exception();
                }
                catch (Exception e)
                {
                    await DisplayError(e.Message);
                    //await Owner.DisplayModal(new MaterialNaoEncontradoPage(async (itemDescricao) =>
                    //{
                    //    await Protected(() =>
                    //    {
                    //        TipoBuscaMaterialCodigoBarras = Tipos.First(x => x.Codigo == itemDescricao.IdentificadorTipoId);
                    //        CodigoMaterialPesquisa = itemDescricao.Identificador;
                    //        Item = itemDescricao.Item;
                    //        return Task.FromResult(true);
                    //    });
                    //}));
                }
            }, ProtectedFlags.SetLoading);
        }

        

       

        private Task IncluirItem()
        {
        return Protected(async () =>
        {
            if(Inventario.Itens != null && Inventario.Itens.Any(i => i.Codigo == Item.Codigo))
            {
                await DisplayError("Este item já foi adicionado ao inventário.");
                return;
            }
            if (Inventario.Estabelecimento.Codigo == Item.CodigoEstabelecimento && Inventario.Setor.Codigo == Item.CodigoSetor)
            {
                Inventario.Itens = Inventario.Itens ?? new List<Item>();
                Inventario.ItensNovos = Inventario.ItensNovos ?? new HashSet<Item>();
                Inventario.Itens.Add(Item);
                Inventario.ItensNovos.Add(Item);
                AtualizarListaMateriais?.Invoke();
                await Owner.Navigation.PopAsync();
            }
            else
            {
                await DisplayError("Este item não pertence ao estabelecimento/setor que está sendo inventariado.");
                return;
            }
        });
        }

        private TipoBuscaMaterial _TipoBuscaMaterialCodigoBarras;
        public TipoBuscaMaterial TipoBuscaMaterialCodigoBarras
        {
            get => _TipoBuscaMaterialCodigoBarras;

            set
            {
                if (_TipoBuscaMaterialCodigoBarras != value)
                {
                    _TipoBuscaMaterialCodigoBarras = value;
                    Validar();
                    FirePropertyChanged("TipoBuscaMaterialCodigoBarras");

                }
            }
        }
        private void Validar()
        {
            if (_CodigoMaterialPesquisa != string.Empty)
            {
                CameraIsVisible = false;
                LupaIsVisible = true;
            }
            else
            {
                CameraIsVisible = true;
                LupaIsVisible = false;
            }
        }
        private bool _CameraIsVisible = true;
        public bool CameraIsVisible
        {
            get => _CameraIsVisible;

            set
            {
                if (_CameraIsVisible != value)
                {
                    _CameraIsVisible = value;
                    FirePropertyChanged("CameraIsVisible");
                }
            }
        }
        private bool _LupaIsVisible = false;
        public bool LupaIsVisible
        {
            get => _LupaIsVisible;

            set
            {
                if (_LupaIsVisible != value)
                {
                    _LupaIsVisible = value;
                    FirePropertyChanged("LupaIsVisible");
                }
            }
        }

        


        private string _Barra;
        public string Barra
        {
            get => _Barra;

            set
            {
                if (_Barra != value)
                {
                    _Barra = value;
                    FirePropertyChanged("Barra");
                }
            }
        }

        private static readonly List<TipoBuscaMaterial> listTipoBuscaMaterial = new List<TipoBuscaMaterial>()
        {
            new TipoBuscaMaterial(TipoBuscaMaterial.NumeroPatrimonialCodigo, TipoBuscaMaterial.NumeroPatrimonial),
            new TipoBuscaMaterial(TipoBuscaMaterial.IBTCodigo, TipoBuscaMaterial.IBT),
            new TipoBuscaMaterial(TipoBuscaMaterial.IPBMCodigo, TipoBuscaMaterial.IPBM),
        };
        public List<TipoBuscaMaterial> Tipos => listTipoBuscaMaterial;

        private string _CodigoMaterialPesquisa = string.Empty;
        public string CodigoMaterialPesquisa
        {
            get => _CodigoMaterialPesquisa;
            set
            {
                if (_CodigoMaterialPesquisa != value)
                {

                    _CodigoMaterialPesquisa = value;
                    Validar();
                    FirePropertyChanged("CodigoMaterialPesquisa");
                }
            }
        }

        //ItemFrame
        private Item _Item = null;
        public Item Item
        {
            get => _Item;
            set => SetProperty(ref _Item, value, linkedPropertyName: "MostrarItem");

        }
        public bool MostrarItem => Item != null;

        public Action AtualizarListaMateriais { get; }
        public Inventario Inventario { get; }
        public ICommand PesquisarMaterialCommand { get; set; }
        public ICommand IncluirItemCommand { get; set; }
        public ICommand PesquisarCodigoBarrasCommand { get; set; }
        //public ICommand CleanCommand { get; set; }
    }
}
