﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

namespace Sirius.App
{
    public class InventarioCriadoController : Controller
    {
        public InventarioCriadoController(VContentPage owner) : base(owner)
        {
            NovoInventarioCommand = CreateCommand(async () => { await NovoInventario(); });
        }

        private async Task NovoInventario()
        {
            await Owner.Navigation.PushAsync(new InventarioPage());
            Owner.Navigation.RemovePage(Owner);
        }

        public ICommand NovoInventarioCommand { get; set; }
    }
}
