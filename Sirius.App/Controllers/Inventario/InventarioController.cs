﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using Xamarin.Forms;
using Xamarin.Forms.Internals;

namespace Sirius.App
{
    public class InventarioController : Controller
    {
        public InventarioController(VContentPage owner, Inventario inventario = null) : base(owner)
        {
            IsLoading = true;
            if (inventario != null)
            {
                Inventario = inventario;
                Observacao = Inventario.Observacao;
                ObservacaoIsEnabled = !inventario.Fechado;
                PodeEditar = false;
                EntryEstabelecimentoIsEnabled = false;
                EntrySetorIsEnabled = false;
                VPickerResponsaveisIsEnabled = false;
                
                Tipo = Tipos.FirstOrDefault(x => x.Codigo == Inventario.Estabelecimento.Tipo);
            }
            else
            {
                Inventario = new Inventario();
               // Tipo = Tipos.First();
                PodeEditar = true;
            }
            _LabelEstabelecimento = "Tipo";
            AdicionarMaterialCommand = CreateCommand(async () => await AdicionarMaterial());
        }
        
        public bool PodeEditar { get; set; }
        public bool NaoPodeEditar => !PodeEditar;

        protected override Task OnAppearing()
        {
            return Protected(async () =>
            {
                if (Inventario != null && Inventario.Id > 0)
                    Inventario = await Business.Current.GetInventarioPorId(Inventario.Id);
                try
                {
                    if (Inventario.Id <= 0)
                    {
                        //_Estabelecimento = Estabelecimentos.FirstOrDefault();
                        Estabelecimento = null;
                        _ListViewEstabelecimentoIsVisible = false;
                        _EntryEstabelecimento = string.Empty;

                        //_Setor = _Setores.FirstOrDefault();
                        Setor = null;
                        EntrySetorIsEnabled = false;
                        _ListViewSetoresIsVisible = false;
                        _EntrySetor = string.Empty;

                        //_Responsavel = Responsaveis.FirstOrDefault();
                        _Responsavel = null;
                        VPickerResponsaveisIsEnabled = false;
                    }
                    else
                    {
                        
                        Estabelecimentos = await Business.Current.GetEstabelecimentosPorTipo(Tipo.Codigo);
                        _Estabelecimento = Estabelecimentos.FirstOrDefault(x => x.Codigo == Inventario.Estabelecimento.Codigo);
                        LabelEstabelecimento = _Tipo?.Nome;
                        EntryEstabelecimento = _Tipo?.Nome;
                        //EntryEstabelecimentoIsEnabled = true;

                        Setores = await Business.Current.GetSetorPorEstabelecimento(_Estabelecimento.Codigo);
                        _Setor = Setores.FirstOrDefault(x => x.Codigo == Inventario.Setor.Codigo);
                        EntrySetor = _Setor.Nome;
                        //EntrySetorIsEnabled = true;

                        Responsaveis = await Business.Current.GetResponsavelAtestoPorEstabelecimento(_Estabelecimento.Codigo);
                        _Responsavel = Responsaveis.FirstOrDefault(x => x.Codigo == Inventario.ResponsavelAtesto.Codigo);
                        //VPickerResponsaveisIsEnabled = true;
                    }

                    if (Inventario.Id > 0)
                    {
                        Inventario.Estabelecimento = _Estabelecimento;
                        Inventario.Setor = _Setor;
                        Inventario.IdSetor = _Setor?.Codigo ?? 0;
                        Inventario.ResponsavelAtesto = _Responsavel;
                        Inventario.IdResponsavelAtesto = _Responsavel?.Codigo ?? 0;
                    }
                        FirePropertyChanged(nameof(Estabelecimento));
                        FirePropertyChanged(nameof(Setor));
                        FirePropertyChanged(nameof(Responsavel));
                        FirePropertyChanged(nameof(EntrySetor));
                        FirePropertyChanged(nameof(EntryEstabelecimento));
                        FirePropertyChanged(nameof(ListViewSetoresIsVisible));
                        FirePropertyChanged(nameof(ListViewEstabelecimentoIsVisible));
                }
                catch (Exception e)
                {
                    await Application.Current.MainPage.DisplayAlert("Erro!", e.Message, "Ok"); 
                }
                
            }, ProtectedFlags.SetLoading);
        }

        public Inventario Inventario { get; set; }

        private Responsavel _Responsavel = null;
        public Responsavel Responsavel
        {
            get => _Responsavel;
            set
            {
                if (value == null)
                    return;
                SetProperty(ref _Responsavel, value, action: () =>
                {
                    Inventario.IdResponsavelAtesto = value?.Codigo ?? 0;
                    Inventario.ResponsavelAtesto = value;
                    Carregar(_Tipo.Codigo, 4);
                });
                if (_Responsavel != null)
                {
                    EntrySetorIsEnabled = false;
                }
               
            }
        }

        private List<Responsavel> _Responsaveis = new List<Responsavel>();
        public List<Responsavel> Responsaveis
        {
            get => _Responsaveis.Where(x => x.Excluido == false).ToList();
            set
            {
                if (_Responsaveis != value)
                {
                    _Responsaveis = value;
                    FirePropertyChanged("Responsaveis");
                }
            }
        }

        private TipoEstabelecimento _Tipo = null;
        public TipoEstabelecimento Tipo
        {
            get => _Tipo;
            set
            {
                if (_Tipo != value)
                {
                    _Tipo = value;                
                    FirePropertyChanged("Tipo");
                    //if (HasAppeared)
                    //{
                    //    CarregarEstabelecimentos(_Tipo.Codigo);
                    //}
                    Estabelecimento = null;
                    LabelEstabelecimento = _Tipo?.Nome;
                    Carregar(_Tipo.Codigo, 1);
                    OnAppearing();
                }
            }
        }      

        private static readonly List<TipoEstabelecimento> listTipoEstabelecimentos = new List<TipoEstabelecimento>()
        {
            new TipoEstabelecimento(TipoEstabelecimento.CoordenacaoCodigo, TipoEstabelecimento.Coordenacao),
            new TipoEstabelecimento(TipoEstabelecimento.SubcoordenacaoCodigo, TipoEstabelecimento.Subcoordenacao),
            new TipoEstabelecimento(TipoEstabelecimento.DistritoSanitarioCodigo, TipoEstabelecimento.DistritoSanitario),
            new TipoEstabelecimento(TipoEstabelecimento.UnidadeDeSaudeCodigo, TipoEstabelecimento.UnidadeDeSaude)
        };
        public List<TipoEstabelecimento> Tipos => listTipoEstabelecimentos;

        private string _Observacao = null;
        public string Observacao
        {
            get => _Observacao;
            set
            {
                if (_Observacao != value)
                {
                    _Observacao = value;
                    Inventario.Observacao = value;
                    FirePropertyChanged("Observacao");
                }
            }
        }

        #region SelectedIndex
        //private int _TipTiposSelectedIndexo = -1;
        //public int TipTiposSelectedIndexo
        //{
        //    get => _TipTiposSelectedIndexo;
        //    set
        //    {
        //        if (_TipTiposSelectedIndexo != value)
        //        {
        //            _TipTiposSelectedIndexo = value;
        //            FirePropertyChanged("TipTiposSelectedIndexo");

        //            Estabelecimento = null;

        //            Setor = null;
        //            Responsavel = null;
        //        }
        //    }
        //}

        //private int _EstabelecimentoSelectedIndex = -1;
        //public int EstabelecimentoSelectedIndex
        //{
        //    get => _EstabelecimentoSelectedIndex;
        //    set
        //    {
        //        if (_EstabelecimentoSelectedIndex != value)
        //        {
        //            _EstabelecimentoSelectedIndex = value;
        //            FirePropertyChanged("EstabelecimentoSelectedIndex");
        //            Setor = null;
        //            Responsavel = null;
        //        }
        //    }
        //}

        //private int _SetorSelectedIndex = -1;
        //public int SetorSelectedIndex
        //{
        //    get => _SetorSelectedIndex;
        //    set
        //    {
        //        if (_SetorSelectedIndex != value)
        //        {
        //            _SetorSelectedIndex = value;
        //            FirePropertyChanged("SetorSelectedIndex");
        //            Responsavel = null;


        //        }
        //    }
        //}

        //private int _ResponsavelSelectedIndex = -1;
        //public int ResponsavelSelectedIndex
        //{
        //    get => _ResponsavelSelectedIndex;
        //    set
        //    {
        //        if (_ResponsavelSelectedIndex != value)
        //        {
        //            _ResponsavelSelectedIndex = value;
        //            FirePropertyChanged("ResponsavelSelectedIndex");

        //        }
        //    }
        //}


        #endregion
        //private async void CarregarResponsaveis(int codigoEstabelecimento)
        //{
        //    await Protected(async () =>
        //    {
        //        if (!HasAppeared)
        //            return;
        //        Responsaveis = await Business.Current.GetResponsavelAtestoPorEstabelecimento(codigoEstabelecimento);
        //        Responsavel = Responsaveis.FirstOrDefault();
        //        Inventario.IdResponsavelAtesto = Responsavel?.Codigo ?? 0;
        //        Inventario.ResponsavelAtesto = Responsavel;
        //    }, ProtectedFlags.SetLoading);
        //}

        //private async void CarregarSetores(int codigoEstabelecimento)
        //{
        //    await Protected(async () =>
        //    {
        //        if (!HasAppeared)
        //            return;
        //        Setores = await Business.Current.GetSetorPorEstabelecimento(codigoEstabelecimento);
        //        Setor = Setores.FirstOrDefault();
        //        Inventario.IdSetor = Setor?.Codigo ?? 0;
        //        Inventario.Setor = Setor;
        //    }, ProtectedFlags.SetLoading);
        //}

        //private async void CarregarEstabelecimentos(string tipo)
        //{
        //    await Protected(async () =>
        //    {
        //        if (!HasAppeared)
        //            return;

        //        //CarregandoEstabelecimentos = true;
        //        Estabelecimentos = await Business.Current.GetEstabelecimentosPorTipo(tipo);
        //        _Estabelecimento = Estabelecimentos.FirstOrDefault();
        //        Inventario.Estabelecimento = _Estabelecimento;
        //        Responsaveis = await Business.Current.GetResponsavelAtestoPorEstabelecimento(Estabelecimento.Codigo);
        //        _Responsavel = Responsaveis.FirstOrDefault();
        //        Inventario.IdResponsavelAtesto = _Responsavel?.Codigo ?? 0;
        //        Inventario.ResponsavelAtesto = _Responsavel;
        //        Setores = await Business.Current.GetSetorPorEstabelecimento(Estabelecimento.Codigo);
        //        _Setor = Setores.FirstOrDefault();
        //        Inventario.IdSetor = _Setor?.Codigo ?? 0;
        //        Inventario.Setor = _Setor;

        //        FirePropertyChanged(nameof(Estabelecimento));
        //        FirePropertyChanged(nameof(Responsavel));
        //        FirePropertyChanged(nameof(Setor));
        //    }, ProtectedFlags.SetLoading);
        //}

        public bool CarregandoLista { get; set; }

        #region Estabelecimento

        private Estabelecimento _Estabelecimento = null;
        public Estabelecimento Estabelecimento
        {
            get => _Estabelecimento;
            set
            {
                if (value == null)
                    return;

                SetProperty(ref _Estabelecimento, value, action: () =>
                {
                    Inventario.Estabelecimento = value;
                    if (HasAppeared)
                    {
                        //CarregarSetores(_Estabelecimento.Codigo);
                        //CarregarResponsaveis(_Estabelecimento.Codigo);
                        Carregar(_Tipo.Codigo, 2);
                        Setor = null;
                        EntryEstabelecimento = _Estabelecimento.NomeEstabelecimento;
                        ListViewEstabelecimentoIsVisible = false;
                    }
                });

            }
        }

        public List<Estabelecimento> TodosEstabelecimentos { set; get; }
        private List<Estabelecimento> _Estabelecimentos = new List<Estabelecimento>();
        public List<Estabelecimento> Estabelecimentos
        {
            get => _Estabelecimentos.Where(x => x.Excluido == false).ToList();
            set
            {
                if (_Estabelecimentos != value)
                {
                    _Estabelecimentos = value;
                    FirePropertyChanged("Estabelecimentos");
                }
            }
        }

        private bool _EntryEstabelecimentoIsEnabled = false;
        public bool EntryEstabelecimentoIsEnabled
        {
            get { return _EntryEstabelecimentoIsEnabled; }
            set { SetProperty(ref _EntryEstabelecimentoIsEnabled, value); }
        }

        private bool _ObservacaoIsEnabled = true;
        public bool ObservacaoIsEnabled
        {
            get { return _ObservacaoIsEnabled; }
            set { SetProperty(ref _ObservacaoIsEnabled, value); }
        }

        private bool _EntrySetorIsEnabled = true;
        public bool EntrySetorIsEnabled
        {
            get { return _EntrySetorIsEnabled; }
            set { SetProperty(ref _EntrySetorIsEnabled, value); }
        }

        private bool _VPickerResponsaveisIsEnabled = true;
        public bool VPickerResponsaveisIsEnabled
        {
            get { return _VPickerResponsaveisIsEnabled; }
            set { SetProperty(ref _VPickerResponsaveisIsEnabled, value); }
        }

        private string _LabelEstabelecimento = null;
        public string LabelEstabelecimento
        {
            get => _LabelEstabelecimento;
            set
            {
                if (_LabelEstabelecimento != value)
                {
                    _LabelEstabelecimento = value;
                    FirePropertyChanged("LabelEstabelecimento");
                }
            }
        }

        private bool _ListViewEstabelecimentoIsVisible = false;
        public bool ListViewEstabelecimentoIsVisible
        {
            get { return _ListViewEstabelecimentoIsVisible; }
            set { SetProperty(ref _ListViewEstabelecimentoIsVisible, value); }
        }     

        private string _EntryEstabelecimento = string.Empty;
        public string EntryEstabelecimento
        {
            
            get => _EntryEstabelecimento;
            set
            {
                if (_EntryEstabelecimento != value)
                {
                    _EntryEstabelecimento = value;                    
                    FirePropertyChanged("EntryEstabelecimento");
                    CarregarEstabelecimento(_Tipo.Codigo);
                }
            }
        }
        private string _EntrySetor = string.Empty;
        public string EntrySetor
        {

            get => _EntrySetor;
            set
            {
                if (_EntrySetor != value)
                {
                    _EntrySetor = value;
                    FirePropertyChanged("EntrySetor");
                    if (Estabelecimento != null)
                    {
                        CarregarSetor();
                    }

                    if (_EntrySetor != string.Empty)
                    {
                        EntryEstabelecimentoIsEnabled = false;
                    }
                    else
                    {
                        EntryEstabelecimentoIsEnabled = true;
                    }
                }
            }
        }
        private async void CarregarEstabelecimento(string tipo)
        {
            await Protected(async () =>
            {
                if (!HasAppeared)
                    return;

                CarregandoLista = true;

                try
                {
                    TodosEstabelecimentos = Estabelecimentos = await Business.Current.GetEstabelecimentosPorTipo(tipo);
                    //mostrar aviso de lista vazia
                    ListViewEstabelecimentoIsVisible = (Estabelecimentos.Count >1);
                    CarregarBuscaEstabelecimento();
                }
                finally
                {
                    CarregandoLista = false;
                }
            });
        }

        private async void CarregarBuscaEstabelecimento()
        {
            await Protected(async () =>
            {
                if (!HasAppeared)
                    return;
                CarregandoLista = true;
                try
                {                                     
                    Estabelecimentos = TodosEstabelecimentos.Where(f => f.NomeEstabelecimento.ToLower().Contains(EntryEstabelecimento.ToLower())).ToList();
                    //mostrar aviso de lista vazia
                    HeightRequestListViewEstabelecimento = (Estabelecimentos.Count <10 ? Estabelecimentos.Count * 25:150);

                    if (EntryEstabelecimento != string.Empty)
                    {
                        ListViewEstabelecimentoIsVisible = (Estabelecimentos.Count > 0);
                    }
                    else
                    {
                        ListViewEstabelecimentoIsVisible = false;
                    }
                    
                }
                finally
                {
                    CarregandoLista = false;
                }
            });
        }
        private async void CarregarBuscaSetor()
        {
            await Protected(async () =>
            {
                if (!HasAppeared)
                    return;
                CarregandoLista = true;
                try
                {
                    Setores = TodosSetores.Where(f => f.Nome.ToLower().Contains(EntrySetor.ToLower())).ToList();
                    //mostrar aviso de lista vazia
                    HeightRequestListViewSetor = (Setores.Count < 10 ? Setores.Count * 25 : 150);
                    if (EntrySetor != string.Empty)
                    {
                        ListViewSetoresIsVisible = (Setores.Count > 0);
                    }
                    else
                    {
                        ListViewSetoresIsVisible = false;
                    }
                    
                }
                finally
                {
                    CarregandoLista = false;
                }
            });
        }
        #endregion
        private int _HeightRequestListViewEstabelecimento = 0;
        public int HeightRequestListViewEstabelecimento
        {
            get { return _HeightRequestListViewEstabelecimento; }
            set { SetProperty(ref _HeightRequestListViewEstabelecimento, value); }
        }
        private int _HeightRequestListViewSetor = 0;
        public int HeightRequestListViewSetor
        {
            get { return _HeightRequestListViewSetor; }
            set { SetProperty(ref _HeightRequestListViewSetor, value); }
        }
        #region Setor

        private Setor _Setor = null;
        public Setor Setor
        {
            get => _Setor;
            set
            {
               
                if (value == null)
                    return;

                SetProperty(ref _Setor, value, action: () =>
                {
                    Inventario.IdSetor = value?.Codigo ?? 0;
                    Inventario.Setor = value;
                    if (HasAppeared)
                    {
                        _Setor = value;                  
                        Responsavel = null;
                        Carregar(_Tipo.Codigo, 3);
                        EntrySetor = _Setor.Nome;
                        ListViewSetoresIsVisible = false;
                    }
                });
            }
        }
        public List<Setor> TodosSetores { set; get; }
        private List<Setor> _Setores = new List<Setor>();
        public List<Setor> Setores
        {
            get => _Setores.Where(x => x.Excluido == false).ToList();
            set
            {
                if (_Setores != value)
                {
                    _Setores = value;
                    FirePropertyChanged("Setores");
                }
            }
        }

        private bool _ListViewSetoresIsVisible = false;
        public bool ListViewSetoresIsVisible
        {
            get { return _ListViewSetoresIsVisible; }
            set { SetProperty(ref _ListViewSetoresIsVisible, value); }
        }

       
        private async void CarregarSetor()
        {
            await Protected(async () =>
            {
                if (!HasAppeared)
                    return;
                CarregandoLista = true;
                try
                {
                    TodosSetores = Setores = await Business.Current.GetSetorPorEstabelecimento(Estabelecimento.Codigo);
                    //mostrar aviso de lista vazia

                    ListViewSetoresIsVisible = (Setores.Count >1);
                    CarregarBuscaSetor();
                }
                finally
                {
                    CarregandoLista = false;
                }
            });
        }

        
        #endregion


        private async void Carregar(string tipo, int call)
        {
            await Protected(async () =>
            {
                if (!HasAppeared)
                    return;

                //CarregandoEstabelecimentos = true;
                switch (call)
                {
                    case 1:
                        Estabelecimento = null;
                        Setor = null;
                        Responsavel = null;
                        EntrySetor = string.Empty;                        
                        EntrySetorIsEnabled = false;
                        VPickerResponsaveisIsEnabled = false;
                        //EstabelecimentoSelectedIndex = -1;
                        //SetorSelectedIndex = -1;
                        //ResponsavelSelectedIndex = -1;
                        //FirePropertyChanged("Estabelecimento");
                        //FirePropertyChanged("Setor");
                        //FirePropertyChanged("Responsavel");
                        //Observacao = null;
                        //FirePropertyChanged("Observacao");
                        CarregandoLista = true;
                        Estabelecimentos = await Business.Current.GetEstabelecimentosPorTipo(tipo);
                        CarregandoLista = false;
                        EntryEstabelecimentoIsEnabled = true;
                        break;
                    case 2:
                        Setor = null;
                        Responsavel = null;                        
                        FirePropertyChanged("Setor");
                        FirePropertyChanged("Responsavel");
                        VPickerResponsaveisIsEnabled = false;
                        //SetorSelectedIndex = -1;
                        //ResponsavelSelectedIndex = -1;
                        //Observacao = null;
                        //FirePropertyChanged("Observacao");
                        EntrySetorIsEnabled = true;
                       
                        Setores = await Business.Current.GetSetorPorEstabelecimento(Estabelecimento.Codigo);
                        break;
                    case 3:
                        Responsavel = null;
                        //ResponsavelSelectedIndex = -1;
                        FirePropertyChanged("Responsavel");
                        //Observacao = null;
                        //FirePropertyChanged("Observacao");
                        VPickerResponsaveisIsEnabled = true;
                        Responsaveis = await Business.Current.GetResponsavelAtestoPorEstabelecimento(Estabelecimento.Codigo);
                        break;
                    case 4:
                        //Observacao = null;
                        //FirePropertyChanged("Observacao");
                        break;
                    default:
                        break;
                }
            }, ProtectedFlags.SetLoading);
        }
        private Task AdicionarMaterial()
        {
            return Protected(async () =>
            {
                if (PodeEditar)
                {
                    Validate.Condition(Estabelecimento != null, "O campo Estabelecimento precisa ser preenchido.");
                    Validate.Condition(Setor != null, "O campo Setor precisa ser preenchido.");
                    Validate.Condition(Responsavel != null, "O campo Responsável precisa ser preenchido.");
                    //Validate.NotEmpty(Observacao, "O campo Observação precisa ser preenchido.");
                }
                await Owner.Navigation.PushAsync(new ListaMateriaisInventarioPage(Inventario));
            });
        }        
        public ICommand AdicionarMaterialCommand { get; set; }
    }
}
