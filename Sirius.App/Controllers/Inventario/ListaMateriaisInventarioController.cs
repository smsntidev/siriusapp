﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using Xamarin.Forms;

namespace Sirius.App
{
    public class ListaMateriaisInventarioController : Controller
    {
        public ListaMateriaisInventarioController(VContentPage owner, Inventario inventario) : base(owner)
        {
            if (inventario != null)
            {
                this.Inventario = inventario;
                this.Itens = new ObservableCollection<Item>(this.Inventario.Itens);
                PodeEditar = !inventario.Fechado;
            }
            else
                PodeEditar = true;
            
            FecharInventarioCommand = CreateCommand(async () => { await FecharInventario(); });
            AdicionarMaterialCommand = CreateCommand(async () => { await AdicionarMaterial(); });
            SalvarInventarioCommand = CreateCommand(async () => await SalvarInventario());
            //RemoverCommand = CreateCommand<Item>((i) => {Remover(i); });
            RemoverCommand = CreateCommand<Item>(async (i) => { await Remover(i); });
        }

        private Task SalvarInventario()
        {
            return Protected(async () =>
            {
                var registroInventario = new RegistroInventario()
                {
                    TipoEstabelecimento = Inventario.Estabelecimento.Tipo,
                    IdEstabelecimento = Inventario.Estabelecimento.Codigo,
                    IdSetor = Inventario.Setor.Codigo,
                    IdResponsavelAtesto = Inventario.ResponsavelAtesto.Codigo,
                    Observacao = Inventario.Observacao,
                    InventarioFechado = false,
                    Itens = Inventario.Itens.ToList(),
                    ItensExcluidos = Inventario.ItensExcluidos?.ToList() ?? new List<Item>(),
                    CodigoUsuario = Business.Current.UsuarioLogado.Codigo
                };

                await Business.Current.RegistrarInventario(registroInventario);
                await Owner.Navigation.PushAsync(new InventarioCriadoPage());

                //remove páginas anteriores
                var pagesToRemove = new List<Page>();
                foreach (var page in Owner.Navigation.NavigationStack)
                {
                    if (page is ListaMateriaisInventarioPage || page is InventarioPage || page is AdicionarMaterialInventarioPage)
                        pagesToRemove.Add(page);
                }
                foreach (var page in pagesToRemove)
                {
                    Owner.Navigation.RemovePage(page);
                }

            }, ProtectedFlags.SetLoading);
        }

        public bool PodeEditar { get; set; }
        public bool NaoPodeEditar => !PodeEditar;

        private ObservableCollection<Item> _Itens = new ObservableCollection<Item>();
        public ObservableCollection<Item> Itens
        {
            get => _Itens;
            set
            {
                if (_Itens != value)
                {
                    _Itens = value;
                    FirePropertyChanged("Itens");                    
                }
            }
        }

        private async Task Remover(Item i)
        {
            if (await Ask("Deseja remover?"))
            {
                this.Inventario.ItensNovos = this.Inventario.ItensNovos ?? new HashSet<Item>();
                this.Inventario.Itens = this.Inventario.Itens ?? new List<Item>();
                this.Inventario.ItensExcluidos = this.Inventario.ItensExcluidos ?? new List<Item>();

                if (this.Inventario.ItensNovos.Contains(i))
                {
                    this.Inventario.ItensNovos.Remove(i);
                    this.Inventario.Itens.Remove(i);
                }
                else
                {
                    this.Inventario.ItensExcluidos.Add(i);
                    this.Inventario.Itens.Remove(i);
                }
                Itens.Remove(i);
            }
        }

        private Task FecharInventario()
        {
            return Protected(async () =>
            {
                var registroInventario = new RegistroInventario()
                {
                    TipoEstabelecimento = Inventario.Estabelecimento.Tipo,
                    IdEstabelecimento = Inventario.Estabelecimento.Codigo,
                    IdSetor = Inventario.Setor.Codigo,
                    IdResponsavelAtesto = Inventario.ResponsavelAtesto.Codigo,
                    Observacao = Inventario.Observacao,
                    InventarioFechado = true,
                    Itens = Inventario.Itens.ToList(),
                    ItensExcluidos = Inventario.ItensExcluidos?.ToList() ?? new List<Item>(),
                    CodigoUsuario = Business.Current.UsuarioLogado.Codigo
                };

                await Business.Current.RegistrarInventario(registroInventario);
                await Owner.Navigation.PushAsync(new InventarioCriadoPage());

                //remove páginas anteriores
                var pagesToRemove = new List<Page>();
                foreach (var page in Owner.Navigation.NavigationStack)
                {
                    if (page is ListaMateriaisInventarioPage || page is InventarioPage || page is AdicionarMaterialInventarioPage)
                        pagesToRemove.Add(page);
                }
                foreach (var page in pagesToRemove)
                {
                    Owner.Navigation.RemovePage(page);
                }

            }, ProtectedFlags.SetLoading);
        }

        private async Task AdicionarMaterial()
        {
            await Owner.Navigation.PushAsync(new AdicionarMaterialInventarioPage(Inventario, () =>
            {
                Itens.Clear();
                foreach (var item in Inventario.Itens)
                {
                    Itens.Add(item);
                }
            }));
        }

        public Inventario Inventario { get; }
        public ICommand SalvarInventarioCommand { get; set; }
        public ICommand FecharInventarioCommand { get; set; }
        public ICommand AdicionarMaterialCommand { get; set; }
        public ICommand RemoverCommand { get; set; }
    }
}
