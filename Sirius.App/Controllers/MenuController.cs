﻿using Sirius.App;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

namespace Sirius.App
{
    public class MenuController : Controller
    {
        public MenuController(VContentPage owner) : base(owner)
        {
            InventarioCommand = CreateCommand(async () => await Iventario());
            OrdemServicoCommand = CreateCommand(async () => await OrdemServico());
            ConsultaOrdemServicoCommand = CreateCommand(async () => await ConsultaOrdemServico());
            MovimentacaoCommand = CreateCommand(async () => await Movimentacao());
            MaterialCommand = CreateCommand(async () => await Material());
            RelatorioCommand = CreateCommand(async () => await Relatorio());
        }

        private async Task ConsultaOrdemServico() => await Owner.Navigation.PushAsync(new PesquisarOrdemServicoPage());
        private async Task OrdemServico() => await Owner.Navigation.PushAsync(new AdicionarMaterialOrdemServicoPage());
        private async Task Iventario() => await Owner.Navigation.PushAsync(new ListaInventariosPage());
        private async Task Movimentacao() => await Owner.Navigation.PushAsync(new MovimentacaoListaPage());
        private async Task Material() => await Owner.Navigation.PushAsync(new MaterialPage());
        private async Task Relatorio() => await Owner.Navigation.PushAsync(new RelatorioListaPage());

        public ICommand InventarioCommand { get; set; }     
        public ICommand OrdemServicoCommand { get; set; }
        public ICommand ConsultaOrdemServicoCommand { get; set; }
        public ICommand MaterialCommand { get; set; }
        public ICommand MovimentacaoCommand { get; set; }
        public ICommand RelatorioCommand { get; set; }

        protected override Task OnAppearing()
        {
            ClearHistory();
            return Task.FromResult(true);
        }
    }
}
