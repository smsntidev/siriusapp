﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

namespace Sirius.App
{
    public class LoginController : Controller
    {
        public LoginController(VContentPage owner) : base(owner)
        {
            this.LogarCommand = CreateCommand(async () => { await Logar(); });
            //IsWriting = true;
        }

        public Task Logar()
        {
            return Protected(async () =>
            {
                Validate.NotEmpty(Username, "É necessário informar um login válido.");
                Validate.NotEmpty(Senha, "É necessário informar a senha");
                if (Username.Contains("@"))
                    Validate.Email(Username, "É necessário informar um e-mail válido.");

                await Business.Current.Login(Username, Senha);
                await Owner.Navigation.PushAsync(new SiriusMasterDetailPage());
                Owner.Navigation.RemovePage(Owner);
            }, ProtectedFlags.SetWriting);
        }        
        
        protected override Task OnAppearing()
        {
            return Protected(async () =>
            {
                if (await Business.Current.LogarAutomaticamente())
                {
                    await Owner.Navigation.PushAsync(new SiriusMasterDetailPage());
                }
            }, ProtectedFlags.SetWriting);
        }

#if DEBUG
        private string _Username = "jessicapereira"; //string.Empty;
        private string _Senha = "220595jgp"; //string.Empty;
#else
        private string _Username = string.Empty;
        private string _Senha = string.Empty;
#endif

        public string Username
        {
            get => _Username;
            set
            {
                if (_Username != value)
                {
                    _Username = value;
                    FirePropertyChanged("Username");
                }
            }
        }

        
        public string Senha
        {
            get => _Senha;
            set
            {
                if (_Senha != value)
                {
                    _Senha = value;
                    FirePropertyChanged("Senha");
                }
            }
        }

        public ICommand LogarCommand { get; private set; }
    }
}
