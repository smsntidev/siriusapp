﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

namespace Sirius.App
{
    public class PesquisarMaterialController : Controller
    {
        public PesquisarMaterialController(VContentPage owner, Action<ItemDescricao> onItem) : base(owner)
        {
            PesquisarMaterialCommand = CreateCommand(async () => { await PesquisarMaterial(); });
            SelecionarMaterialCommand = CreateCommand<ItemDescricao>(async (ItemDescricao i) => { await SelecionarMaterial(i); });
            this.OnItem = onItem;
        }

        private async Task SelecionarMaterial(ItemDescricao i)
        {
            OnItem(i);
            await Owner.Navigation.PopAsync();
        }

        private string _Codigo;
        public string Codigo
        {
            get => _Codigo;
            set
            {
                if (_Codigo != value)
                {
                    _Codigo = value;
                }
            }
        }

        private string _Nome;
        public string Nome
        {
            get => _Nome;
            set
            {
                if (_Nome != value)
                {
                    _Nome = value;
                }
            }
        }

        private List<ItemDescricao> _Materiais = null;
        public List<ItemDescricao> Materiais
        {
            get => _Materiais;
            set
            {
                if (_Materiais != value)
                {
                    _Materiais = value;
                    FirePropertyChanged("Materiais");
                }
            }
        }

        private async Task PesquisarMaterial()
        {
            await Protected(async () =>
            {
                Validate.Condition(Codigo.IsNotEmpty() || Nome.IsNotEmpty(), "É necessário informar um código ou nome.");
                //var materiais = await Business.Current.GetMateriais(Nome, Codigo);
                var materiais = await Business.Current.GetMateriais(Nome, Codigo);
                materiais = materiais?.Take(100).ToList();
                this.Materiais = materiais;
            }, ProtectedFlags.SetLoading);
        }

        public ICommand PesquisarMaterialCommand { get; set; }
        public ICommand SelecionarMaterialCommand { get; set; }
        public Action<ItemDescricao> OnItem { get; }
    }
}
