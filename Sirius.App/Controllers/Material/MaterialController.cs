﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

namespace Sirius.App
{
    public class MaterialController : Controller
    {
        public MaterialController(VContentPage owner) : base(owner)
        {
            IsLoading = true;
            PesquisarMaterialCommand = CreateCommand(async () => { await PesquisarMaterial(); });
            AtualizarMaterialCommand = CreateCommand(async () => { await AtualizarMaterial(); });
            PesquisarCodigoBarrasCommand = CreateCommand(async () => { await PesquisarCodigoBarras(); });
        }

        protected override Task OnAppearing()
        {
            return Protected(async () =>
            {
                _SituacoesFisicas = await Business.Current.GetSituacoesFisicas();
                //TipoBuscaMaterial = TiposBuscaMaterial.First();
            }, ProtectedFlags.SetLoading);
        }

        private TipoBuscaMaterial _TipoBuscaMaterial;
        public TipoBuscaMaterial TipoBuscaMaterial
        {
            get => _TipoBuscaMaterial;

            set
            {
                if (_TipoBuscaMaterial != value)
                {
                    _TipoBuscaMaterial = value;
                    Validar();
                    FirePropertyChanged("TipoBuscaMaterial");
                }
            }
        }

        private List<SituacaoFisica> _SituacoesFisicas = null;
        private List<TipoBuscaMaterial> _TiposBuscaMaterial = new List<TipoBuscaMaterial>()
        {
            new TipoBuscaMaterial(TipoBuscaMaterial.NumeroPatrimonialCodigo, TipoBuscaMaterial.NumeroPatrimonial),
            new TipoBuscaMaterial(TipoBuscaMaterial.IBTCodigo, TipoBuscaMaterial.IBT),
            new TipoBuscaMaterial(TipoBuscaMaterial.IPBMCodigo, TipoBuscaMaterial.IPBM),
        };
        public List<TipoBuscaMaterial> TiposBuscaMaterial
        {
            get => _TiposBuscaMaterial;
            set => SetProperty(ref _TiposBuscaMaterial, value);
        }

        private string _CodigoMaterialPesquisa = string.Empty;
        public string CodigoMaterialPesquisa
        {
            get => _CodigoMaterialPesquisa;
            set
            {
                if (_CodigoMaterialPesquisa != value)
                {

                    _CodigoMaterialPesquisa = value;
                    Validar();
                    FirePropertyChanged("CodigoMaterialPesquisa");
                }
            }
        }

        private bool _PesquisandoMaterial = false;
        public bool PesquisandoMaterial
        {
            get => _PesquisandoMaterial;
            set => SetProperty(ref _PesquisandoMaterial, value, linkedPropertyName: "NotPesquisandoMaterial");
        }

        public bool NotPesquisandoMaterial => !PesquisandoMaterial;

        private Item _Item = null;
        public Item Item
        {
            get => _Item;
            set
            {
                SetProperty(ref _Item, value, action: () =>
                {
                    Tombo = value.Tombo2;
                    if (Tombo == null && Item.IBT > 0)
                        Tombo = Item.IBT.ToString();
                    if (Tombo == null && Item.IDPBM > 0)
                        Tombo = Item.IDPBM.ToString();
                    SituacaoMaterial = _SituacoesFisicas.FirstOrDefault(a => a.Codigo == value.CodigoSituacaoFisica)?.Nome;
                    NomeMaterial = value.Material?.NomeMaterial;
                    Serial = string.IsNullOrWhiteSpace(value.Serial) ? "Sem Serial" : value.Serial.Trim();
                    Qnt = Item.Quantidade.ToString();
                    MostrarMaterial = true;
                    
                });
            }
        }

        private void Validar()
        {
            if (_CodigoMaterialPesquisa != string.Empty)
            {
                CameraIsVisible = false;
                LupaIsVisible = true;
            }
            else
            {
                CameraIsVisible = true;
                LupaIsVisible = false;
            }
        }
        private bool _CameraIsVisible = true;
        public bool CameraIsVisible
        {
            get => _CameraIsVisible;

            set
            {
                if (_CameraIsVisible != value)
                {
                    _CameraIsVisible = value;
                    FirePropertyChanged("CameraIsVisible");
                }
            }
        }
        private bool _LupaIsVisible = false;
        public bool LupaIsVisible
        {
            get => _LupaIsVisible;

            set
            {
                if (_LupaIsVisible != value)
                {
                    _LupaIsVisible = value;
                    FirePropertyChanged("LupaIsVisible");
                }
            }
        }

        private string _NomeMaterial;
        public string NomeMaterial
        {
            get => _NomeMaterial;
            set => SetProperty(ref _NomeMaterial, value);
        }

        private string _Serial;
        public string Serial
        {
            get { return _Serial; }
            set => SetProperty(ref _Serial, value);
        }


        private string _Qnt = null;
        public string Qnt
        {
            get => _Qnt;
            set
            {
                if (_Qnt != value)
                {
                    _Qnt = value;
                    FirePropertyChanged("Qnt");
                }
            }
        }
        
        private string _SituacaoMaterial = null;
        public string SituacaoMaterial
        {
            get => _SituacaoMaterial;
            set
            {
                if (_SituacaoMaterial != value)
                {
                    _SituacaoMaterial = value;
                    FirePropertyChanged("SituacaoMaterial");
                }
            }
        }
        
        private string _Tombo = null;
        public string Tombo
        {
            get => _Tombo;
            set
            {
                if (_Tombo != value)
                {
                    _Tombo = value;
                    FirePropertyChanged("Tombo");
                }
            }
        }

        private bool _MostrarMaterial = false;
        public bool MostrarMaterial
        {
            get => _MostrarMaterial;
            set
            {
                if (_MostrarMaterial != value)
                {
                    _MostrarMaterial = value;
                    FirePropertyChanged("MostrarMaterial");
                }
            }
        }

        private Task PesquisarCodigoBarras()
        {
            return Protected(async () =>
            {
                var result = await this.ScanBarcode();
                if (result == null)
                    throw new Exception("Código de barras não pode ser lido.");
                PesquisandoMaterial = true;
                try
                {
                    //Item = await Business.Current.GetItemPorCodigoBarras(result.Text);
                    //Barra = result.Text;
                   // Item = await Business.Current.GetItemPorMaterial(result.Text, TipoBuscaMaterialCodigoBarras.Codigo);
                    string materialId = result.Text;

                    if (materialId.Length == 8)
                    {
                        materialId = materialId.Substring(2, 6);
                    }
                    CodigoMaterialPesquisa = materialId;
                    //var result2 = await Business.Current.GetItemPorCodigoBarras(result.Text);
                    //var result2 = await Business.Current.GetItemPorMaterial(materialId, TipoBuscaMaterialCodigoBarras.Codigo);
                }
                finally
                {
                    PesquisandoMaterial = false;
                }
            }, ProtectedFlags.SetLoading);
        }

        private async Task PesquisarMaterial()
        {
            await Protected(async () =>
            {
                
                try
                {
                    Validate.Condition(TipoBuscaMaterial != null, "O campo Tipo precisa ser preenchido.");
                    Validate.NotEmpty(CodigoMaterialPesquisa, "O campo Número precisa ser preenchido.");
                    Validate.StringLength(CodigoMaterialPesquisa, 6, "O campo Número precisa ter 6 digitos.");
                    PesquisandoMaterial = true;

                    Item = await Business.Current.GetItemPorMaterial(CodigoMaterialPesquisa, TipoBuscaMaterial.Codigo);
                    if (Item == null) throw new Exception("Nenhum Material foi encontrado");
                }
                catch (Exception e)
                {
                    await DisplayError(e.Message);

                }
                finally
                {
                    PesquisandoMaterial = false;
                }
            });
        }
        //private TipoBuscaMaterial _TipoBuscaMaterialCodigoBarras;
        //public TipoBuscaMaterial TipoBuscaMaterialCodigoBarras
        //{
        //    get => _TipoBuscaMaterialCodigoBarras;

        //    set
        //    {
        //        if (_TipoBuscaMaterialCodigoBarras != value)
        //        {
        //            _TipoBuscaMaterialCodigoBarras = value;
        //            //TipoBuscaPesquisaCodigoBarras = true;
                    
        //            FirePropertyChanged("TipoBuscaMaterialCodigoBarras");
        //        }
        //    }
        //}
        //private bool _TipoBuscaPesquisaCodigoBarras = false;
        //public bool TipoBuscaPesquisaCodigoBarras
        //{
        //    get => _TipoBuscaPesquisaCodigoBarras;

        //    set
        //    {
        //        if (_TipoBuscaPesquisaCodigoBarras != value)
        //        {
        //            _TipoBuscaPesquisaCodigoBarras = value;
        //            FirePropertyChanged("TipoBuscaPesquisaCodigoBarras");
        //        }
        //    }
        //}

        private async Task AtualizarMaterial()
        {
            await Owner.Navigation.PushAsync(new AtualizarMaterialPage(Item));
        }

        public ICommand PesquisarMaterialCommand { get; set; }
        public ICommand AtualizarMaterialCommand { get; set; }
        public ICommand PesquisarCodigoBarrasCommand { get; set; }
    }
}
