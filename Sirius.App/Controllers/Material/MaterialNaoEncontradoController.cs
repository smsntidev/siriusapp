﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

namespace Sirius.App
{
    public class MaterialNaoEncontradoController : ModalController
    {
        public MaterialNaoEncontradoController(VContentView owner, Action<ItemDescricao> onItem) : base(owner)
        {
            this.OnItem = onItem;
            this.PesquisarCommand = CreateCommand(async () => await Pesquisar());
        }

        private async Task Pesquisar()
        {
            HideMe();
            await Owner.Navigation.PushAsync(new PesquisarMaterialPage(this.OnItem));
        }

        public Action<ItemDescricao> OnItem { get; }
        public ICommand PesquisarCommand { get; }
    }
}
