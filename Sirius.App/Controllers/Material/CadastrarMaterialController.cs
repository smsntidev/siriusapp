﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

namespace Sirius.App
{
    public class CadastrarMaterialController : Controller
    {
        public CadastrarMaterialController(VContentPage owner, Item item = null) : base(owner)
        {
            Item = item;
        }

        protected override Task OnAppearing()
        {
            return Protected(async () =>
            {
                if(Item == null)
                {
                    SituacaoFisicaLista = await Business.Current.GetSituacoesFisicas();
                    Estabelecimentos = await Business.Current.GetTodosEstabelecimentos();
                }
            }, ProtectedFlags.SetLoading);
        }

        private Item _Item = null;
        public Item Item
        {
            get => _Item;
            set
            {
                if (_Item != value)
                {
                    SetProperty(ref _Item, value, action: () => {

                        if (!HasAppeared)
                        {
                            CarregarEstabelecimentos();
                            CarregarSituacoesFisica();
                        }

                        Codigo = value.CodigoMaterial;
                        Descricao = value.DescricaoMaterialInformatica;
                        Classe = value.Material.NomeClasse;
                        SetorOrigem = value.NomeSetor;
                        SituacaoMaterial = SituacaoFisicaLista.First(a => a.Codigo == value.CodigoSituacaoFisica);
                        Estabelecimento = Estabelecimentos.FirstOrDefault(a => a.Codigo == value.CodigoEstabelecimento);
                    });
                }
            }
        }

        private int _Codigo;
        public int Codigo
        {
            get { return _Codigo; }
            set => SetProperty(ref _Codigo, value);
        }

        private string _Descricao;
        public string Descricao
        {
            get { return _Descricao; }
            set => SetProperty(ref _Descricao, value);
        }

        private string _Classe;
        public string Classe
        {
            get { return _Classe; }
            set => SetProperty(ref _Classe, value);
        }

        private string _Marca;
        public string Marca
        {
            get { return _Marca; }
            set => SetProperty(ref _Marca, value);
        }

        private string _Modelo;
        public string Modelo
        {
            get { return _Modelo; }
            set => SetProperty(ref _Modelo, value);
        }

        private string _Serial;
        public string Serial
        {
            get { return _Serial; }
            set => SetProperty(ref _Serial, value);
        }

        private List<SituacaoFisica> _SituacaoFisicaLista = new List<SituacaoFisica>();
        public List<SituacaoFisica> SituacaoFisicaLista
        {
            get { return _SituacaoFisicaLista = new List<SituacaoFisica>(); }
            set => SetProperty(ref _SituacaoFisicaLista, value);
        }

        private SituacaoFisica _SituacaoMaterial;
        public SituacaoFisica SituacaoMaterial
        {
            get { return _SituacaoMaterial; }
            set => SetProperty(ref _SituacaoMaterial, value, action: () => {
                if (Item != null)
                {
                    Item.SituacaoFisica = value.Nome;
                    Item.CodigoSituacaoFisica = value.Codigo;
                }
            });
        }


        private Estabelecimento _Estabelecimento = null;
        public Estabelecimento Estabelecimento
        {
            get => _Estabelecimento;
            set => SetProperty(ref _Estabelecimento, value);
        }

        private List<Estabelecimento> _Estabelecimentos = new List<Estabelecimento>();
        public List<Estabelecimento> Estabelecimentos
        {
            get => _Estabelecimentos;
            set
            {
                if (_Estabelecimentos != value)
                {
                    _Estabelecimentos = value;
                    FirePropertyChanged("Estabelecimentos");
                }
            }
        }

        private string _SetorOrigem;
        public string SetorOrigem
        {
            get { return _SetorOrigem; }
            set => SetProperty(ref _SetorOrigem, value);
        }

        private bool _CarregandoEstabelecimentos = false;
        public bool CarregandoEstabelecimentos
        {
            get => _CarregandoEstabelecimentos;
            set
            {
                if (_CarregandoEstabelecimentos != value)
                {
                    _CarregandoEstabelecimentos = value;
                    FirePropertyChanged("CarregandoEstabelecimentos");
                    FirePropertyChanged("NotCarregandoEstabelecimentos");
                }
            }
        }
        public bool NotCarregandoEstabelecimentos => !CarregandoEstabelecimentos;

        private bool _CarregandoSituacoesFisica = false;
        public bool CarregandoSituacoesFisica
        {
            get => _CarregandoSituacoesFisica;
            set
            {
                if (_CarregandoSituacoesFisica != value)
                {
                    _CarregandoEstabelecimentos = value;
                    FirePropertyChanged("CarregandoSituacoesFisica");
                    FirePropertyChanged("NotCarregandoSituacoesFisica");
                }
            }
        }
        public bool NotCarregandoSituacoesFisica => !CarregandoEstabelecimentos;

        private async void CarregarEstabelecimentos()
        {
            await Protected(async () =>
            {
                if (!HasAppeared)
                    return;

                CarregandoEstabelecimentos = true;
                try
                {
                    Estabelecimentos = await Business.Current.GetTodosEstabelecimentos();
                }
                finally
                {
                    CarregandoEstabelecimentos = false;
                }
            });
        }

        private async void CarregarSituacoesFisica()
        {
            await Protected(async () => {
                if (!HasAppeared)
                    return;
                CarregandoSituacoesFisica = true;
                try
                {
                    SituacaoFisicaLista = await Business.Current.GetSituacoesFisicas();
                }
                finally
                {
                    CarregandoSituacoesFisica = false;
                }
            });
        }


        private async Task SalvarItemMaterial()
        {
            await Protected(async () =>
            {
                if (!HasAppeared)
                    return;
                try
                {
                    //TODO: NÃO TEM METODO DISPONÍVEL NO SERVIDOR
                    //await Business.Current.RegistrarMovimentacao(registroMovimentacao);
                }
                finally
                { 
                    //await Owner.Navigation.PushAsync(new MovimentacaoCriadaPage(222366554));
                }
            }, ProtectedFlags.SetWriting);
        }

        public ICommand AtualizarMaterialCommand { get; set; }
    }
}
