﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

namespace Sirius.App
{
    public class AtualizarMaterialController : Controller
    {
        public AtualizarMaterialController(VContentPage owner, Item item) : base(owner)
        {
            IsLoading = true;
            this.AtualizarMaterialCommand = CreateCommand(async () => await SalvarItemMaterial());

            Item = item;
        }

        protected override Task OnAppearing()
        {
            return Protected(async () =>
            {
                SituacaoFisicaLista = await Business.Current.GetSituacoesFisicas();
                await Task.Delay(10);
                SituacaoMaterial = SituacaoFisicaLista.FirstOrDefault(x => x.Codigo == Item.CodigoSituacaoFisica);
            }, ProtectedFlags.SetLoading);
        }

        private Item _Item = null;
        public Item Item
        {
            get => _Item;
            set
            {
                if (_Item != value)
                {
                    SetProperty(ref _Item, value, action: () =>
                    {
                        //Codigo = value.CodigoMaterial;
                        Codigo = value.Material.Identificador;
                        Descricao = value.Material?.NomeMaterial;
                        Classe = value.Material.NomeClasse;
                        SetorOrigem = value.NomeSetor;
                        Estabelecimento = value.NomeEstabelecimento;
                        Marca = value.Marca;
                        Modelo = value.Modelo;
                        Serial = string.IsNullOrWhiteSpace(value.Serial) ? "Sem Serial" : value.Serial;
                        SetorOrigem = value.NomeSetor;
                        DescricaoDetalhada = value.DescricaoMaterialInformatica;
                    });
                }
            }
        }

        //private int _Codigo;
        //public int Codigo
        //{
        //    get { return _Codigo; }
        //    set => SetProperty(ref _Codigo, value);
        //}
        private long _Codigo;
        public long Codigo
        {
            get { return _Codigo; }
            set => SetProperty(ref _Codigo, value);
        }
        private string _Descricao;
        public string Descricao
        {
            get { return _Descricao; }
            set => SetProperty(ref _Descricao, value);
        }

        private string _DescricaoDetalhada;
        public string DescricaoDetalhada {
            get => _DescricaoDetalhada;
            set => SetProperty(ref _DescricaoDetalhada, value);
        }

        private string _Classe;
        public string Classe
        {
            get { return _Classe; }
            set => SetProperty(ref _Classe, value);
        }

        private string _Marca;
        public string Marca
        {
            get { return _Marca; }
            set => SetProperty(ref _Marca, value);
        }

        private string _Modelo;
        public string Modelo
        {
            get { return _Modelo; }
            set => SetProperty(ref _Modelo, value);
        }

        private string _Serial;
        public string Serial
        {
            get { return _Serial; }
            set => SetProperty(ref _Serial, value);
        }

        private List<SituacaoFisica> _SituacaoFisicaLista = new List<SituacaoFisica>();
        public List<SituacaoFisica> SituacaoFisicaLista
        {
            get { return _SituacaoFisicaLista; }
            set => SetProperty(ref _SituacaoFisicaLista, value);
        }

        private SituacaoFisica _SituacaoMaterial;
        public SituacaoFisica SituacaoMaterial
        {
            get { return _SituacaoMaterial; }
            set => SetProperty(ref _SituacaoMaterial, value, action: () =>
            {
                if (Item != null && value != null && HasAppeared)
                {
                    Item.SituacaoFisica = value.Nome;
                    Item.CodigoSituacaoFisica = value.Codigo;
                }
            });
        }


        private string _Estabelecimento = null;
        public string Estabelecimento
        {
            get => _Estabelecimento;
            set => SetProperty(ref _Estabelecimento, value);
        }

        private string _SetorOrigem;
        public string SetorOrigem
        {
            get { return _SetorOrigem; }
            set => SetProperty(ref _SetorOrigem, value);
        }
        
        private async Task SalvarItemMaterial()
        {
            await Protected(async () =>
            {
                try
                {
                    await Business.Current.AtualizarItemMaterial(Item);
                    Owner.Navigation.RemovePage(Owner.Navigation.NavigationStack[Owner.Navigation.NavigationStack.Count - 2]);
                    await DisplayMessage("Material atualizado com sucesso.");
                    await Owner.Navigation.PopAsync();
                }
                finally
                {
                    
                }
            }, ProtectedFlags.SetWriting);
        }

        public ICommand AtualizarMaterialCommand { get; set; }
    }
}
