﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

namespace Sirius.App
{
    public class AdicionarMaterialOrdemServicoController : Controller
    {
        public AdicionarMaterialOrdemServicoController(VContentPage owner, int? ordemServicoSuperiorId = null) : base(owner)
        {
            this.IsLoading = true;
            this.AdicionarMaterialCommand = CreateCommand(async () => await AdicionarMaterial());
            this.OrdemServico = new OrdemServico()
            {
                IdOrdemServicoSuperior = ordemServicoSuperiorId
            };
        }

        public AdicionarMaterialOrdemServicoController(VContentPage owner, OrdemServico ordemServico) : base(owner)
        {
            this.IsLoading = true;
            this.AdicionarMaterialCommand = CreateCommand(async () => await AdicionarMaterial());
            this.OrdemServico = ordemServico;
            this.Contato = this.OrdemServico.Contato;
            this.Telefone = this.OrdemServico.Telefone;
            this.Descricao = this.OrdemServico.DescricaoProblema;
        }

        protected override Task OnAppearing()
        {
            return Protected(async () =>
            {
                this.AreasResponsaveis = await Business.Current.GetAreasResponsaveis();
                this.Problemas = await Business.Current.GetProblemas();
                //await Task.Delay(10);
                //Não chamar o RefreshOrdemServico() com bugs?
                await RefreshOrdemServico();
                this.AreaResponsavel = this.AreasResponsaveis.FirstOrDefault(a => a.Codigo == this.OrdemServico.IdAreaResponsavel);
                if (this.AreaResponsavel == null)
                    this.AreaResponsavel = this.AreasResponsaveis.First();
                this.Problema = this.Problemas.FirstOrDefault(a => a.Codigo == this.OrdemServico.IdProblema);
                if (this.Problema == null)
                    this.Problema = this.Problemas.First();
                this.Contato = this.OrdemServico.Contato;
                this.Telefone = this.OrdemServico.Telefone;
                this.Descricao = this.OrdemServico.DescricaoProblema;
            }, ProtectedFlags.SetLoading);
        }

        private async Task RefreshOrdemServico()
        {
            if (this.OrdemServico.IdOrdemServico.HasValue)
            {
                var ordemServico = await Business.Current.GetOrdemServicoPorId(this.OrdemServico.IdOrdemServico.Value);
                this.OrdemServico.AreaResponsavel = ordemServico.AreaResponsavel;
                this.OrdemServico.ArquivoAnexo = ordemServico.ArquivoAnexo;
                this.OrdemServico.ArquivoDescricao = ordemServico.ArquivoDescricao;
                this.OrdemServico.ArquivoOcorrenciaId = ordemServico.ArquivoOcorrenciaId;
                this.OrdemServico.CodigoItem = ordemServico.CodigoItem;
                this.OrdemServico.Contato = ordemServico.Contato;
                this.OrdemServico.DataArquivoAnexo = ordemServico.DataArquivoAnexo;
                this.OrdemServico.DataOcorrencia = ordemServico.DataOcorrencia;
                this.OrdemServico.DescricaoMaterial = ordemServico.DescricaoMaterial;
                this.OrdemServico.DescricaoProblema = ordemServico.DescricaoProblema;
                //NÃO ESTÁ VINDO
                //this.OrdemServico.EstabelecimentoSaude = ordemServico.EstabelecimentoSaude;
                //this.OrdemServico.EstabelecimentoSaudeId = ordemServico.EstabelecimentoSaudeId;
                this.OrdemServico.IdAreaResponsavel = ordemServico.IdAreaResponsavel;
                this.OrdemServico.IdItem = ordemServico.IdItem;
                this.OrdemServico.IdOrdemServico = ordemServico.IdOrdemServico;
                this.OrdemServico.IdOrdemServicoSuperior = ordemServico.IdOrdemServicoSuperior;
                this.OrdemServico.IdProblema = ordemServico.IdProblema;
                this.OrdemServico.IdStatus = ordemServico.IdStatus;
                this.OrdemServico.IdTipoMaterial = ordemServico.IdTipoMaterial;
                this.OrdemServico.Problema = ordemServico.Problema;
                //NÃO ESTÁ VINDO
                //this.OrdemServico.Setor = ordemServico.Setor;
                //this.OrdemServico.SetorId = ordemServico.SetorId;
                this.OrdemServico.Status = ordemServico.Status;
                this.OrdemServico.Telefone = ordemServico.Telefone;
            }
        }

        private Task AdicionarMaterial()
        {
            return Protected(async () =>
            {
                Validate.NotEmpty(Contato, "O campo Contato é de preenchimento obrigatório.");
                Validate.NotEmpty(Telefone, "O campo Telefone é de preenchimento obrigatório.");
                if (Telefone.Length < 13) { Validate.NotEmpty("", "O campo Telefone é de preenchimento obrigatório."); }
                Validate.NotEmpty(Descricao, "O campo Descrição do Problema é de preenchimento obrigatório.");

                await Owner.Navigation.PushAsync(new AnexarArquivoOrdemServicoPage(this.OrdemServico));
            });
        }
        
        private AreaResponsavel _AreaResponsavel = null;
        public AreaResponsavel AreaResponsavel
        {
            get => _AreaResponsavel;
            set
            {
                if (_AreaResponsavel != value)
                {
                    _AreaResponsavel = value;
                    this.OrdemServico.AreaResponsavel = value.Nome;
                    this.OrdemServico.IdAreaResponsavel = value.Codigo;
                    FirePropertyChanged("AreaResponsavel");
                }
            }
        }

        private List<AreaResponsavel> _AreasResponsaveis = null;
        public List<AreaResponsavel> AreasResponsaveis
        {
            get => _AreasResponsaveis;
            set
            {
                if (_AreasResponsaveis != value)
                {
                    _AreasResponsaveis = value;
                    FirePropertyChanged("AreasResponsaveis");
                }
            }
        }

        private Problema _Problema = null;
        public Problema Problema
        {
            get => _Problema;
            set
            {
                if (_Problema != value)
                {
                    _Problema = value;
                    this.OrdemServico.Problema = value.Nome;
                    this.OrdemServico.IdProblema = value.Codigo;
                    FirePropertyChanged("Problema");
                }
            }
        }

        private List<Problema> _Problemas = null;
        public List<Problema> Problemas
        {
            get => _Problemas;
            set
            {
                if (_Problemas != value)
                {
                    _Problemas = value;
                    FirePropertyChanged("Problemas");
                }
            }
        }

        private string _Contato = null;
        public string Contato
        {
            get => _Contato;
            set
            {
                if (_Contato != value)
                {
                    _Contato = value;
                    this.OrdemServico.Contato = value;
                    FirePropertyChanged("Contato");
                }
            }
        }

        private string _Telefone = null;
        public string Telefone
        {
            get => _Telefone;
            set
            {
                if (_Telefone != value)
                {
                    _Telefone = value;
                    this.OrdemServico.Telefone = value.ApenasNumeros();
                    FirePropertyChanged("Telefone");
                }
            }
        }

        private string _Descricao = null;
        public string Descricao
        {
            get => _Descricao;
            set
            {
                if (_Descricao != value)
                {
                    _Descricao = value;
                    this.OrdemServico.DescricaoProblema = value;
                    FirePropertyChanged("Descricao");
                }
            }
        }

        public ICommand AdicionarMaterialCommand { get; set; }
        public OrdemServico OrdemServico { get; }
    }
}
