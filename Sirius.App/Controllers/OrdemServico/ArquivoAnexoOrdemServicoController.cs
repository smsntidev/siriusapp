﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

namespace Sirius.App
{
    public class ArquivoAnexoOrdemServicoController : Controller
    {
        public ArquivoAnexoOrdemServicoController(VContentPage owner, OrdemServico ordemServico) : base(owner)
        {
            this.AbrirOcorrenciaCommand = CreateCommand(async () => await AbrirOcorrencia());
            this.AdicionarArquivoCommand = CreateCommand(async () => await AdicionarArquivo());
            this.RemoverArquivoCommand = CreateCommand(async () => await RemoverArquivo());
            this.OrdemServico = ordemServico;

            if(ordemServico.ArquivoOcorrenciaId.HasValue || ordemServico.ArquivoAnexo != null)
            {
                this.NomeArquivo = this.OrdemServico.ArquivoDescricao;
                this.DataArquivo = this.OrdemServico.DataArquivoAnexo;
                this.MostrarAdicionarArquivo = false;
            }
        }

        private Task RemoverArquivo()
        {
            this.OrdemServico.ArquivoDescricao = null;
            this.OrdemServico.DataArquivoAnexo = null;
            this.OrdemServico.ArquivoOcorrenciaId = null;
            this.OrdemServico.ArquivoAnexo = null;
            this.MostrarAdicionarArquivo = true;
            return Task.FromResult(true);
        }

        private Task AdicionarArquivo()
        {
            return Protected(async () =>
            {
                try
                {
                    var arquivo = await Business.Current.CapturarArquivo();
                    if (arquivo.Mime.Contains("image"))
                        this.OrdemServico.ArquivoDescricao = $"Imagem {arquivo.Mime.Substring("image/".Length).ToUpperInvariant()}";
                    else if (arquivo.Mime.Contains("pdf"))
                        this.OrdemServico.ArquivoDescricao = "Arquivo PDF";
                    else
                        this.OrdemServico.ArquivoDescricao = "Arquivo WORD";
                    this.OrdemServico.DataArquivoAnexo = DateTime.Today.ToString("d");
                    this.OrdemServico.ArquivoOcorrenciaId = null;
                    this.OrdemServico.ArquivoAnexo = arquivo.Stream;
                    this.NomeArquivo = this.OrdemServico.ArquivoDescricao;
                    this.DataArquivo = this.OrdemServico.DataArquivoAnexo;
                    this.MostrarAdicionarArquivo = false;
                }
                catch (OperationCanceledException)
                {
                    //O usuário cancelou a operação
                }
            });
        }

        private Task AbrirOcorrencia()
        {
            return Protected(async () =>
            {
                //DAVID: Pegar o Id depois
                await Business.Current.Salvar(this.OrdemServico);
                await Owner.Navigation.PushAsync(new OrdemServicoCriadaPage(this.OrdemServico));
            }, ProtectedFlags.SetWriting);
        }
        
        private bool _MostrarAdicionarArquivo = true;
        public bool MostrarAdicionarArquivo
        {
            get => _MostrarAdicionarArquivo;
            set
            {
                if (_MostrarAdicionarArquivo != value)
                {
                    _MostrarAdicionarArquivo = value;
                    FirePropertyChanged("MostrarAdicionarArquivo");
                    FirePropertyChanged("NotMostrarAdicionarArquivo");
                }
            }
        }
        public bool NotMostrarAdicionarArquivo => !MostrarAdicionarArquivo;

        private string _NomeArquivo = null;
        public string NomeArquivo
        {
            get => _NomeArquivo;
            set
            {
                if (_NomeArquivo != value)
                {
                    _NomeArquivo = value;
                    FirePropertyChanged("NomeArquivo");
                }
            }
        }

        private string _DataArquivo = null;
        public string DataArquivo
        {
            get => _DataArquivo;
            set
            {
                if (_DataArquivo != value)
                {
                    _DataArquivo = value;
                    FirePropertyChanged("DataArquivo");
                }
            }
        }

        public ICommand AbrirOcorrenciaCommand { get; set; }
        public ICommand AdicionarArquivoCommand { get; set; }
        public ICommand RemoverArquivoCommand { get; set; }
        public OrdemServico OrdemServico { get; }
    }
}
