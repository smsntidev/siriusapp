﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

namespace Sirius.App
{
    public class OrdemServicoListaController : Controller
    {
        public OrdemServicoListaController(VContentPage owner, OrdemServicoFiltro ordemServicoFiltro) : base(owner)
        {
            this.IsLoading = true;
            this.AddCommand = CreateCommand(async () => await Add());
            this.OrdemSelecionadaCommand = CreateCommand(async (OrdemServico ordemServico) => await OrdemSelecionada(ordemServico) );
            this.OrdemServicoFiltro = ordemServicoFiltro;

            
            this.AbrirSubChamadoCommand = CreateCommand(async () => await AbrirSubChamado());
            this.EditarOcorrenciaCommand = CreateCommand(async () => await EditarOcorrencia());
            //this.OrdemServicoId = this.OrdemServico.IdOrdemServico.ToString();
            this.EncerrarOcorrenciaCommand = CreateCommand(async () => await EncerrarOcorrencia());
            this.CancelarOcorrenciaCommand = CreateCommand(async () => await CancelarOcorrencia());
            this.SalvarOcorrenciaCommand = CreateCommand(async () => await SalvarOcorrencia());
        }

        protected override Task OnAppearing()
        {
            return Protected(async () =>
            {
                if (!OrdemServicoSelected)
                {
                    OrdensServico = await Business.Current.PesquisarOrdemServico(OrdemServicoFiltro);
                }
                else
                {
                    OrdensServico = OrdensServicoSelected;
                }
            }, ProtectedFlags.SetLoading);
        }

        private async Task OrdemSelecionada(OrdemServico ordemServico)
        {
            if(ordemServico != null)
            {
                OrdemServicoSelected = true;
                OrdensServicoSelected = OrdensServico.FindAll(x => x.IdOrdemServico == ordemServico.IdOrdemServico);
                this.OrdemServico = OrdensServicoSelected[0];
                await OnAppearing();
                
                //await Owner.Navigation.PushAsync(new AdicionarMaterialOrdemServicoPage(ordemServico));
            }

        }

        private List<OrdemServico> _OrdensServico = null;
        public List<OrdemServico> OrdensServico
        {
            get => _OrdensServico;
            set
            {
                if (_OrdensServico != value)
                {
                    _OrdensServico = value;
                    FirePropertyChanged("OrdensServico");
                }
            }
        }

       
        private bool _OrdemServicoSelected = false;
        public bool OrdemServicoSelected
        {
            get => _OrdemServicoSelected;
            set
            {
                if (_OrdemServicoSelected != value)
                {
                    _OrdemServicoSelected = value;
                    FirePropertyChanged("OrdemServicoSelected");
                }
            }
        }

        private string _SolucaoJustificativaLabel = null;
        public string SolucaoJustificativaLabel
        {
            get => _SolucaoJustificativaLabel;
            set
            {
                if (_SolucaoJustificativaLabel != value)
                {
                    _SolucaoJustificativaLabel = value;
                    FirePropertyChanged("SolucaoJustificativaLabel");
                }
            }
        }

        private string _SolucaoJustificativa = null;
        public string SolucaoJustificativa
        {
            get => _SolucaoJustificativa;
            set
            {
                if (_SolucaoJustificativa != value)
                {
                    _SolucaoJustificativa = value;
                    FirePropertyChanged("SolucaoJustificativa");
                }
            }
        }
        private bool _SolucaoJustificativaIsVisible = false;
        public bool SolucaoJustificativaIsVisible
        {
            get => _SolucaoJustificativaIsVisible;
            set
            {
                if (_SolucaoJustificativaIsVisible != value)
                {
                    _SolucaoJustificativaIsVisible = value;
                    FirePropertyChanged("SolucaoJustificativaIsVisible");
                }
            }
        }
        //private bool _OrdensServicoIsRefreshing = false;
        //public bool OrdensServicoIsRefreshing
        //{
        //    get => _OrdensServicoIsRefreshing;
        //    set
        //    {
        //        if (_OrdensServicoIsRefreshing != value)
        //        {
        //            OrdensServicoIsRefreshing = value;
        //            FirePropertyChanged("_OrdensServicoIsRefreshing");
        //        }
        //    }
        //}
        private bool _OrdensServicoIsVisible = false;
        public bool OrdensServicoIsVisible
        {
            get => _OrdensServicoIsVisible;
            set
            {
                if (_OrdensServicoIsVisible != value)
                {
                    _OrdensServicoIsVisible = value;
                    FirePropertyChanged("SolucaoJustificativaIsVisible");
                }
            }
        }

        private async Task Add()
        {
            await Owner.Navigation.PushAsync(new AdicionarMaterialOrdemServicoPage());
        }

        private async Task EditarOcorrencia()
        {
            await Owner.Navigation.PushAsync(new AdicionarMaterialOrdemServicoPage(this.OrdemServico));
            Owner.Navigation.RemovePage(Owner);
        }

        private async Task AbrirSubChamado()
        {
            await Owner.Navigation.PushAsync(new AdicionarMaterialOrdemServicoPage(this.OrdemServico.IdOrdemServico ?? 0));
            Owner.Navigation.RemovePage(Owner);
        }

        private async Task EncerrarOcorrencia()
        {
           
            SolucaoJustificativaIsVisible = true;
            SolucaoJustificativaLabel = "Solução";


        }
        private async Task CancelarOcorrencia()
        {
            
            SolucaoJustificativaIsVisible = true;
            SolucaoJustificativaLabel = "Justificativa";
        }
        private async Task SalvarOcorrencia()
        {
            if (SolucaoJustificativaLabel == "Solução")
            {

            }else if (SolucaoJustificativaLabel == "Justificativa")
            {

            }
           
        }

        public List<OrdemServico> OrdensServicoSelected { get; set; }

        public ICommand AddCommand { get; set; }
        public ICommand OrdemSelecionadaCommand { get; set; }
        public OrdemServicoFiltro OrdemServicoFiltro { get; }

        public OrdemServico OrdemServico { get; set; }
        public ICommand AbrirSubChamadoCommand { get; set; }
        public ICommand EditarOcorrenciaCommand { get; set; }
        public ICommand EncerrarOcorrenciaCommand { get; set; }
        public ICommand CancelarOcorrenciaCommand { get; set; }
        public ICommand SalvarOcorrenciaCommand { get; set; }
    }
}
