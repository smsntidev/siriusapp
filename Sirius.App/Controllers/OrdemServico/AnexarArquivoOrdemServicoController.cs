﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

namespace Sirius.App
{
    public class AnexarArquivoOrdemServicoController : Controller
    {
        public AnexarArquivoOrdemServicoController(VContentPage owner, OrdemServico ordemServico) : base(owner)
        {
            this.IsLoading = true;
            this.OrdemServico = ordemServico;
            this.AbrirOcorrenciaCommand = CreateCommand(async () => await AbrirOcorrencia());
            this.AnexarArquivoCommand = CreateCommand(async () => await AnexarArquivo());
            this.PesquisarCodigoBarrasCommand = CreateCommand(async () => await PesquisarCodigoBarras());
            this.PesquisarMaterialCommand = CreateCommand(async () => await PesquisarMaterial());
            if (this.OrdemServico.CodigoItem.IsNotEmpty())
            {
                this.CodigoMaterial = this.OrdemServico.CodigoItem;
                this.Descricao = this.OrdemServico.DescricaoMaterial;
                this.EstabelecimentoSaude = this.OrdemServico.EstabelecimentoSaude;
                this.Setor = this.OrdemServico.Setor;
                this.MostrarOutrosDados = true;
            }
        }

        protected override Task OnAppearing()
        {
            return Protected(async () =>
            {
                TipoMateriais = await Business.Current.GetTipoMateriais();
                //Seta os valores vindos do model novamente
                if (this.OrdemServico.CodigoItem.IsNotEmpty())
                {
                    this.TipoMaterial = TipoMateriais.First(m => m.Codigo == this.OrdemServico.IdTipoMaterial);
                    this.CodigoMaterial = this.OrdemServico.CodigoItem;
                    this.Descricao = this.OrdemServico.DescricaoMaterial;
                    this.EstabelecimentoSaude = this.OrdemServico.EstabelecimentoSaude;
                    this.Setor = this.OrdemServico.Setor;
                    this.MostrarOutrosDados = true;
                }
                
                    //this.TipoMaterial = TipoMateriais.First();
            }, ProtectedFlags.SetLoading);
        }

        private Task PesquisarMaterial()
        {
            ProcurandoMateriais = true;
            return Protected(async () =>
            {
                try
                {
                    Item item;
                    try
                    {
                        Validate.Condition(TipoMaterial != null, "O campo Tipo precisa ser preenchido.");
                        Validate.NotEmpty(CodigoMaterial, "O campo Número precisa ser preenchido.");
                        Validate.StringLength(CodigoMaterial, 6, "O campo Número precisa ter 6 digitos.");

                        item = await Business.Current.GetItemPorMaterial(CodigoMaterial, TipoMaterial.Codigo);
                        if (item == null)
                        {
                            await DisplayError("Material não encontrado.");
                            return;
                        }

                        this.OrdemServico.IdItem = item.Codigo;
                        this.OrdemServico.IdTipoMaterial = TipoMaterial.Codigo;
                        this.OrdemServico.CodigoItem = CodigoMaterial;

                        this.Descricao = item.Material.NomeMaterial;
                        this.EstabelecimentoSaude = item.NomeEstabelecimento;
                        this.Setor = item.NomeSetor;
                        this.OrdemServico.DescricaoMaterial = item.Material.NomeMaterial;
                        this.OrdemServico.EstabelecimentoSaude = item.NomeEstabelecimento;
                        this.OrdemServico.EstabelecimentoSaudeId = item.CodigoEstabelecimento;
                        this.OrdemServico.Setor = item.NomeSetor;
                        this.OrdemServico.SetorId = item.CodigoSetor;
                        this.MostrarOutrosDados = true;
                    }
                    catch (Exception e)
                    {
                        await DisplayError(e.Message);
                    }
                }
                finally
                {
                    ProcurandoMateriais = false;
                }
            });
        }

        private Task PesquisarCodigoBarras()
        {
            return Protected(async () =>
            {
                var result = await this.ScanBarcode();
                if (result == null)
                    throw new Exception("Código de barras não pode ser lido.");

                LendoCodigoBarras = true;
                try
                {
                    try
                    {
                        string materialId = result.Text;

                        if (materialId.Length == 8)
                        {
                            materialId = materialId.Substring(2, 6);
                        }
                        CodigoMaterial = materialId;
                        //var result2 = await Business.Current.GetItemPorCodigoBarras(result.Text);
                        //item = await Business.Current.GetItemPorMaterial(materialId, TipoBuscaMaterialCodigoBarras.Codigo);

                        ////item = await Business.Current.GetItemPorCodigoBarras(result.Text);
                        //if (item == null)
                        //{
                        //    await DisplayError("Material não encontrado.");
                        //    return;
                        //}

                        //this.OrdemServico.IdItem = item.Codigo;
                        //this.OrdemServico.IdTipoMaterial = TipoMaterial.Codigo;
                        //this.OrdemServico.CodigoItem = CodigoMaterial;

                        //this.Descricao = item.Material.NomeMaterial;
                        //this.EstabelecimentoSaude = item.NomeEstabelecimento;
                        //this.Setor = item.NomeSetor;
                        //this.OrdemServico.DescricaoMaterial = item.Material.NomeMaterial;
                        //this.OrdemServico.EstabelecimentoSaude = item.NomeEstabelecimento;
                        //this.OrdemServico.EstabelecimentoSaudeId = item.CodigoEstabelecimento;
                        //this.OrdemServico.Setor = item.NomeSetor;
                        //this.OrdemServico.SetorId = item.CodigoSetor;
                        //this.MostrarOutrosDados = true;
                    }
                    catch
                    {
                        await DisplayError("Material não encontrado.");
                    }
                }
                finally
                {
                    LendoCodigoBarras = false;
                }
            });
        }

        private void Validar()
        {
            if (_CodigoMaterial != string.Empty)
            {
                CameraIsVisible = false;
                LupaIsVisible = true;
            }
            else
            {
                CameraIsVisible = true;
                LupaIsVisible = false;
            }
        }
        private bool _CameraIsVisible = true;
        public bool CameraIsVisible
        {
            get => _CameraIsVisible;

            set
            {
                if (_CameraIsVisible != value)
                {
                    _CameraIsVisible = value;
                    FirePropertyChanged("CameraIsVisible");
                }
            }
        }
        private bool _LupaIsVisible = false;
        public bool LupaIsVisible
        {
            get => _LupaIsVisible;

            set
            {
                if (_LupaIsVisible != value)
                {
                    _LupaIsVisible = value;
                    FirePropertyChanged("LupaIsVisible");
                }
            }
        }

        //private TipoBuscaMaterial _TipoBuscaMaterialCodigoBarras;
        //public TipoBuscaMaterial TipoBuscaMaterialCodigoBarras
        //{
        //    get => _TipoBuscaMaterialCodigoBarras;

        //    set
        //    {
        //        if (_TipoBuscaMaterialCodigoBarras != value)
        //        {
        //            _TipoBuscaMaterialCodigoBarras = value;
        //            TipoBuscaPesquisaCodigoBarras = true;
        //            FirePropertyChanged("TipoBuscaMaterialCodigoBarras");

        //        }
        //    }
        //}
        //private bool _TipoBuscaPesquisaCodigoBarras = false;
        //public bool TipoBuscaPesquisaCodigoBarras
        //{
        //    get => _TipoBuscaPesquisaCodigoBarras;

        //    set
        //    {
        //        if (_TipoBuscaPesquisaCodigoBarras != value)
        //        {
        //            _TipoBuscaPesquisaCodigoBarras = value;
        //            FirePropertyChanged("TipoBuscaPesquisaCodigoBarras");
        //        }
        //    }
        //}

        private Task AbrirOcorrencia()
        {
            return Protected(async () =>
            {
                //DAVID: Pegar o Id depois
                if (this.OrdemServico.IdItem <= 0)
                    Validate.Condition(false, "É necessário especificar um material.");
                await Business.Current.Salvar(this.OrdemServico);
                await Owner.Navigation.PushAsync(new OrdemServicoCriadaPage(this.OrdemServico));
            }, ProtectedFlags.SetWriting);
        }

        private Task AnexarArquivo()
        {
            return Protected(async () =>
            {
                if (this.OrdemServico.IdItem <= 0)
                    Validate.Condition(false, "É necessário especificar um material.");
                await Owner.Navigation.PushAsync(new ArquivoAnexoOrdemServicoPage(this.OrdemServico));
            });
        }
        
        private TipoBuscaMaterial _TipoMaterial = null;
        public TipoBuscaMaterial TipoMaterial
        {
            get => _TipoMaterial;
            set
            {
                if (_TipoMaterial != value)
                {
                    _TipoMaterial = value;
                    Validar();
                    FirePropertyChanged("TipoMaterial");
                }
            }
        }

        private List<TipoBuscaMaterial> _TipoMateriais = null;
        public List<TipoBuscaMaterial> TipoMateriais
        {
            get => _TipoMateriais;
            set
            {
                if (_TipoMateriais != value)
                {
                    _TipoMateriais = value;
                    FirePropertyChanged("TipoMateriais");
                }
            }
        }

        private string _CodigoMaterial = string.Empty;
        public string CodigoMaterial
        {
            get => _CodigoMaterial;
            set
            {
                if (_CodigoMaterial != value)
                {
                    _CodigoMaterial = value;
                    Validar();
                    FirePropertyChanged("CodigoMaterial");
                }
            }
        }

        private bool _LendoCodigoBarras = false;
        public bool LendoCodigoBarras
        {
            get => _LendoCodigoBarras;
            set
            {
                if (_LendoCodigoBarras != value)
                {
                    _LendoCodigoBarras = value;
                    FirePropertyChanged("LendoCodigoBarras");
                    FirePropertyChanged("NotLendoCodigoBarras");
                }
            }
        }
        public bool NotLendoCodigoBarras => !LendoCodigoBarras;

        private bool _ProcurandoMateriais = false;
        public bool ProcurandoMateriais
        {
            get => _ProcurandoMateriais;
            set
            {
                if (_ProcurandoMateriais != value)
                {
                    _ProcurandoMateriais = value;
                    FirePropertyChanged("ProcurandoMateriais");
                }
            }
        }

        private bool _MostrarOutrosDados = false;
        public bool MostrarOutrosDados
        {
            get => _MostrarOutrosDados;
            set
            {
                if (_MostrarOutrosDados != value)
                {
                    _MostrarOutrosDados = value;
                    FirePropertyChanged("MostrarOutrosDados");
                }
            }
        }

        private string _Descricao = null;
        public string Descricao
        {
            get => _Descricao;
            set
            {
                if (_Descricao != value)
                {
                    _Descricao = value;
                    FirePropertyChanged("Descricao");
                }
            }
        }

        private string _EstabelecimentoSaude = null;
        public string EstabelecimentoSaude
        {
            get => _EstabelecimentoSaude;
            set
            {
                if (_EstabelecimentoSaude != value)
                {
                    _EstabelecimentoSaude = value;
                    FirePropertyChanged("EstabelecimentoSaude");
                }
            }
        }

        private string _Setor = null;
        public string Setor
        {
            get => _Setor;
            set
            {
                if (_Setor != value)
                {
                    _Setor = value;
                    FirePropertyChanged("Setor");
                }
            }
        }
        
        public ICommand AbrirOcorrenciaCommand { get; set; }
        public ICommand AnexarArquivoCommand { get; set; }
        public ICommand PesquisarCodigoBarrasCommand { get; set; }
        public ICommand PesquisarMaterialCommand { get; set; }

        public OrdemServico OrdemServico { get; }
    }
}
