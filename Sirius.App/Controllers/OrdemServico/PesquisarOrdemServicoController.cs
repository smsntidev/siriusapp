﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

namespace Sirius.App
{
    public class PesquisarOrdemServicoController : Controller
    {
        public PesquisarOrdemServicoController(VContentPage owner) : base(owner)
        {
            this.IsLoading = true;
            this.AddCommand = CreateCommand(async () => await Add());
            this.PesquisarCommand = CreateCommand(async () => await Pesquisar());
            EntrySetorIsEnabled = false;
        }

        private Task Pesquisar()
        {
            return Protected(async () =>
            {
                await Owner.Navigation.PushAsync(new OrdemServicoListaPage(filtro: await CriarFiltro()));
            });
        }

        private async Task<OrdemServicoFiltro> CriarFiltro()
        {
            int? itemId = null;
            if (CodigoMaterial.IsNotEmpty())
            {
                try
                {
                    itemId = (await Business.Current.GetItemPorMaterial(CodigoMaterial, TipoMaterial.Codigo)).Codigo;
                }
                catch
                {
                    itemId = null;
                    await DisplayError("Material não encontrado. A busca irá ignorar o código do material informado.");
                }
            }

            return new OrdemServicoFiltro()
            {
                AreaResponsavelId = IfMinus1Null(AreaResponsavel?.Codigo ?? 0),
                Contato = Contato,
                EstabelecimentoSaudeId = IfMinus1Null(EstabelecimentoSaude?.Codigo ?? 0),
                ProblemaId = IfMinus1Null(Problema?.Codigo),
                SetorId = IfMinus1Null(Setor?.Codigo),
                StatusId = IfMinus1Null(Status?.Codigo),
                ItemId = itemId
            };
        }

        private int? IfMinus1Null(int? codigo)
        {
            if (codigo == -1)
                return null;
            return codigo;
        }

        private async Task Add()
        {
            await Owner.Navigation.PushAsync(new AdicionarMaterialOrdemServicoPage());
        }

        protected override Task OnAppearing()
        {
            return Protected(async () =>
            {
                this.AreasResponsaveis = (await Business.Current.GetAreasResponsaveis()).CloneAdd(new AreaResponsavel()
                {
                    Codigo = -1,
                    Nome = "Selecionar"
                });
                await Task.Delay(10);
                this.AreaResponsavel = this.AreasResponsaveis.First();

                this.Problemas = (await Business.Current.GetProblemas()).CloneAdd(new Problema()
                {
                    Codigo = -1,
                    Nome = "Selecionar"
                });
                await Task.Delay(10);
                this.Problema = this.Problemas.First();

                this.TipoMateriais = (await Business.Current.GetTipoMateriais());
                await Task.Delay(10);
                //this.TipoMaterial = this.TipoMateriais.First();

                this.EstabelecimentosSaude = (await Business.Current.GetTodosEstabelecimentos()).CloneAdd(new Estabelecimento()
                {
                    Codigo = -1,
                    NomeEstabelecimento = "Selecionar"
                });
                await Task.Delay(10);
                this._EstabelecimentoSaude = this.EstabelecimentosSaude.First();
                this.FirePropertyChanged(nameof(EstabelecimentoSaude));

                this.TodosSetores = new List<Setor>
                {
                    new Setor() { Codigo = -1, Nome = "Selecione" }
                };
                await Task.Delay(10);
                //this.Setor = this.TodosSetores.First();

                this.Statuses = (await Business.Current.GetOrdemServicoStatuses()).CloneAdd(new OrdemServicoStatus(-1, "Selecionar"));
                await Task.Delay(10);
                this.Status = this.Statuses.First();

            }, ProtectedFlags.SetLoading);
        }

        private async void CarregarSetores()
        {
            await Protected(async () =>
            {
                CarregandoSetores = true;
                try
                {
                    var estabelecimento = EstabelecimentoSaude?.Codigo ?? -1;
                    if (estabelecimento == -1)
                        this.TodosSetores = new List<Setor>
                        {
                            new Setor(){ Codigo = -1, Nome = "Selecione"}
                        };
                    else
                        this.TodosSetores = (await Business.Current.GetSetorPorEstabelecimento(estabelecimento)).CloneAdd(new Setor()
                        {
                            Codigo = -1,
                            Nome = "Selecione"
                        });

                    await Task.Delay(10);
                    //this.Setor = this.TodosSetores.First();
                }
                finally
                {
                    CarregandoSetores = false;
                }
            });
        }

        private AreaResponsavel _AreaResponsavel = null;
        public AreaResponsavel AreaResponsavel
        {
            get => _AreaResponsavel;
            set
            {
                if (_AreaResponsavel != value)
                {
                    _AreaResponsavel = value;
                    FirePropertyChanged("AreaResponsavel");
                }
            }
        }

        private List<AreaResponsavel> _AreasResponsaveis = null;
        public List<AreaResponsavel> AreasResponsaveis
        {
            get => _AreasResponsaveis;
            set
            {
                if (_AreasResponsaveis != value)
                {
                    _AreasResponsaveis = value;
                    FirePropertyChanged("AreasResponsaveis");
                }
            }
        }

        private Problema _Problema = null;
        public Problema Problema
        {
            get => _Problema;
            set
            {
                if (_Problema != value)
                {
                    _Problema = value;
                    FirePropertyChanged("Problema");
                }
            }
        }

        private List<Problema> _Problemas = null;
        public List<Problema> Problemas
        {
            get => _Problemas;
            set
            {
                if (_Problemas != value)
                {
                    _Problemas = value;
                    FirePropertyChanged("Problemas");
                }
            }
        }

        public ICommand AddCommand { get; set; }
        public ICommand PesquisarCommand { get; set; }

        private TipoBuscaMaterial _TipoMaterial ;
        public TipoBuscaMaterial TipoMaterial
        {
            get => _TipoMaterial;
            set
            {
                if (_TipoMaterial != value)
                {
                    _TipoMaterial = value;
                    FirePropertyChanged("TipoMaterial");
                }
            }
        }

        private List<TipoBuscaMaterial> _TipoMateriais = null;
        public List<TipoBuscaMaterial> TipoMateriais
        {
            get => _TipoMateriais;
            set
            {
                if (_TipoMateriais != value)
                {
                    _TipoMateriais = value;
                    FirePropertyChanged("TipoMateriais");
                }
            }
        }

        private string _CodigoMaterial = null;
        public string CodigoMaterial
        {
            get => _CodigoMaterial;
            set
            {
                if (_CodigoMaterial != value)
                {
                    _CodigoMaterial = value;
                    FirePropertyChanged("CodigoMaterial");
                }
            }
        }

        private List<Estabelecimento> _EstabelecimentosSaude = null;
        public List<Estabelecimento> EstabelecimentosSaude
        {
            get => _EstabelecimentosSaude;
            set
            {
                if (_EstabelecimentosSaude != value)
                {
                    _EstabelecimentosSaude = value;
                    FirePropertyChanged("EstabelecimentosSaude");
                }
            }
        }

        private Estabelecimento _EstabelecimentoSaude = null;
        public Estabelecimento EstabelecimentoSaude
        {
            get => _EstabelecimentoSaude;
            set
            {
                if (value == null)
                    return;
                SetProperty(ref _EstabelecimentoSaude, value, action: () =>
                {
                   
                    if (HasAppeared)
                    {
                        _EstabelecimentoSaude = value;
                        CarregarSetores();
                        EntryEstabelecimento = _EstabelecimentoSaude.NomeEstabelecimento;
                        ListViewEstabelecimentoIsVisible = false;
                        FirePropertyChanged("EstabelecimentoSaude");
                        EntrySetorIsEnabled = true;
                    }
                });
            }
        }
        
        private bool _CarregandoSetores = false;
        public bool CarregandoSetores
        {
            get => _CarregandoSetores;
            set
            {
                if (_CarregandoSetores != value)
                {
                    _CarregandoSetores = value;
                    FirePropertyChanged("CarregandoSetores");
                    FirePropertyChanged("NotCarregandoSetores");
                }
            }
        }
        public bool NotCarregandoSetores => !CarregandoSetores;

        private List<Setor> _TodosSetores = null;
        public List<Setor> TodosSetores
        {
            get => _TodosSetores;
            set
            {
                if (_TodosSetores != value)
                {
                    _TodosSetores = value;
                    FirePropertyChanged("Setores");
                }
            }
        }

        private Setor _Setor = null;
        public Setor Setor
        {
            get => _Setor;
            set
            {
                if (value == null)
                    return;

                SetProperty(ref _Setor, value, action: () =>
                {
                    if (HasAppeared)
                    {
                        _Setor = value;
                        FirePropertyChanged("Setor");
                        EntrySetor = value.Nome;
                        ListViewSetoresIsVisible = false;
                    }
                });
            }
        }

        private string _Contato = null;
        public string Contato
        {
            get => _Contato;
            set
            {
                if (_Contato != value)
                {
                    _Contato = value;
                    FirePropertyChanged("Contato");
                }
            }
        }

        private List<OrdemServicoStatus> _Statuses = null;
        public List<OrdemServicoStatus> Statuses
        {
            get => _Statuses;
            set
            {
                if (_Statuses != value)
                {
                    _Statuses = value;
                    FirePropertyChanged("Statuses");
                }
            }
        }

        private OrdemServicoStatus _Status = null;
        public OrdemServicoStatus Status
        {
            get => _Status;
            set
            {
                if (_Status != value)
                {
                    _Status = value;
                    FirePropertyChanged("Status");
                }
            }
        }



        private List<Setor> _Setores = new List<Setor>();
        public List<Setor> Setores
        {
            get => _Setores.Where(x => x.Excluido == false).ToList();
            set
            {
                if (_Setores != value)
                {
                    _Setores = value;
                    FirePropertyChanged("Setores");
                }
            }
        }
        private List<Estabelecimento> _Estabelecimentos = new List<Estabelecimento>();
        public List<Estabelecimento> Estabelecimentos
        {
            get => _Estabelecimentos.Where(x => x.Excluido == false).ToList();
            set
            {
                if (_Estabelecimentos != value)
                {
                    _Estabelecimentos = value;
                    FirePropertyChanged("Estabelecimentos");
                }
            }
        }
        private string _EntryEstabelecimento = string.Empty;
        public string EntryEstabelecimento
        {

            get => _EntryEstabelecimento;
            set
            {
                if (_EntryEstabelecimento != value)
                {
                    _EntryEstabelecimento = value;
                    FirePropertyChanged("EntryEstabelecimento");
                    CarregarBuscaEstabelecimento();
                }
            }
        }
        private string _EntrySetor = string.Empty;
        public string EntrySetor
        {

            get => _EntrySetor;
            set
            {
                if (_EntrySetor != value)
                {
                    _EntrySetor = value;
                    FirePropertyChanged("EntrySetor");
                    if (EstabelecimentoSaude != null)
                    {
                        CarregarBuscaSetor();
                    }

                    if (_EntrySetor != string.Empty)
                    {
                        EntryEstabelecimentoIsEnabled = false;
                    }
                    else
                    {
                        EntryEstabelecimentoIsEnabled = true;
                    }
                }
            }
        }
        private bool _EntryEstabelecimentoIsEnabled = true;
        public bool EntryEstabelecimentoIsEnabled
        {
            get { return _EntryEstabelecimentoIsEnabled; }
            set { SetProperty(ref _EntryEstabelecimentoIsEnabled, value); }
        }
        private bool _EntrySetorIsEnabled = false;
        public bool EntrySetorIsEnabled
        {
            get { return _EntrySetorIsEnabled; }
            set { SetProperty(ref _EntrySetorIsEnabled, value); }
        }

        private bool _ListViewEstabelecimentoIsVisible = false;
        public bool ListViewEstabelecimentoIsVisible
        {
            get { return _ListViewEstabelecimentoIsVisible; }
            set { SetProperty(ref _ListViewEstabelecimentoIsVisible, value); }
        }

        private bool _ListViewSetoresIsVisible = false;
        public bool ListViewSetoresIsVisible
        {
            get { return _ListViewSetoresIsVisible; }
            set { SetProperty(ref _ListViewSetoresIsVisible, value); }
        }
        private async void CarregarBuscaEstabelecimento()
        {
            await Protected(async () =>
            {
             
                Estabelecimentos = EstabelecimentosSaude.Where(f => f.NomeEstabelecimento.ToLower().Contains(EntryEstabelecimento.ToLower())).ToList();
                //mostrar aviso de lista vazia
                HeightRequestListViewEstabelecimento = (Estabelecimentos.Count < 10 ? Estabelecimentos.Count * 25 : 150);
                if (EntryEstabelecimento != string.Empty)
                {
                    ListViewEstabelecimentoIsVisible = (Estabelecimentos.Count > 0);
                }
                else
                {
                    ListViewEstabelecimentoIsVisible = false;
                }
               
               
            });
        }
        private async void CarregarBuscaSetor()
        {
            await Protected(async () =>
            {
                Setores = TodosSetores.Where(f => f.Nome.ToLower().Contains(EntrySetor.ToLower())).ToList();
                //mostrar aviso de lista vazia
                HeightRequestListViewSetor = (TodosSetores.Count < 10 ? TodosSetores.Count * 25 : 150);
                if (EntrySetor != string.Empty)
                { 
                    ListViewSetoresIsVisible = (TodosSetores.Count > 0);
                }
                else
                {
                    ListViewSetoresIsVisible = false;
                }
            });
        }

        private int _HeightRequestListViewEstabelecimento = 0;
        public int HeightRequestListViewEstabelecimento
        {
            get { return _HeightRequestListViewEstabelecimento; }
            set { SetProperty(ref _HeightRequestListViewEstabelecimento, value); }
        }
        private int _HeightRequestListViewSetor = 0;
        public int HeightRequestListViewSetor
        {
            get { return _HeightRequestListViewSetor; }
            set { SetProperty(ref _HeightRequestListViewSetor, value); }
        }
    }
}
