﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

namespace Sirius.App
{
    public class OrdemServicoCriadaController : Controller
    {
        public OrdemServicoCriadaController(VContentPage owner, OrdemServico ordemServico) : base(owner)
        {
            this.OrdemServico = ordemServico;
            this.AbrirSubChamadoCommand = CreateCommand(async () => await AbrirSubChamado());
            this.EditarOcorrenciaCommand = CreateCommand(async () => await EditarOcorrencia());
            this.NovaOcorrenciaCommand = CreateCommand(async () => await NovaOcorrencia());
            this.OrdemServicoId = this.OrdemServico.IdOrdemServico.ToString();
        }
        
        private string _OrdemServicoId = null;
        public string OrdemServicoId
        {
            get => _OrdemServicoId;
            set
            {
                if (_OrdemServicoId != value)
                {
                    _OrdemServicoId = value;
                    FirePropertyChanged("OrdemServicoId");
                }
            }
        }

        protected override Task OnAppearing()
        {
            List<Xamarin.Forms.Page> pagesToRemove = new List<Xamarin.Forms.Page>();
            foreach (var page in Owner.Navigation.NavigationStack)
            {
                if (page is AdicionarMaterialOrdemServicoPage || page is AnexarArquivoOrdemServicoPage || page is ArquivoAnexoOrdemServicoPage)
                    pagesToRemove.Add(page);
            }
            foreach (var page in pagesToRemove)
                Owner.Navigation.RemovePage(page);
            return Task.FromResult(true);
        }

        private async Task NovaOcorrencia()
        {
            await Owner.Navigation.PushAsync(new AdicionarMaterialOrdemServicoPage());
            Owner.Navigation.RemovePage(Owner);
        }

        private async Task EditarOcorrencia()
        {
            await Owner.Navigation.PushAsync(new AdicionarMaterialOrdemServicoPage(this.OrdemServico));
            Owner.Navigation.RemovePage(Owner);
        }

        private async Task AbrirSubChamado()
        {
            await Owner.Navigation.PushAsync(new AdicionarMaterialOrdemServicoPage(this.OrdemServico.IdOrdemServico ?? 0));
            Owner.Navigation.RemovePage(Owner);
        }

        public OrdemServico OrdemServico { get; }
        public ICommand AbrirSubChamadoCommand { get; set; }
        public ICommand EditarOcorrenciaCommand { get; set; }
        public ICommand NovaOcorrenciaCommand { get; set; }
    }
}
