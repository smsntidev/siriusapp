﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

namespace Sirius.App
{
    public class AlterarSenhaController : Controller
    {
        public AlterarSenhaController(VContentPage owner) : base(owner)
        {
            this.AlterarSenhaCommand = CreateCommand(async () => await AlterarSenha());
        }

        private Task AlterarSenha()
        {
            return Protected(async () =>
            {
                Validate.NotEmpty(Nome, "Nome precisa ser preenchido.");
                Validate.NotEmpty(Usuario, "Usuário precisa ser preenchido.");
                Validate.NotEmpty(SenhaAtual, "Senha Atual precisa ser preenchida.");
                Validate.NotEmpty(NovaSenha, "Nova Senha precisa ser preenchida.");
                Validate.NotEmpty(ConfirmarSenha, "confirmar Senha precisa ser preenchida.");
                Validate.Condition(NovaSenha.Length >= 6, "Nova Senha precisa ter pelo menos 6 caracteres.");
                Validate.Condition(NovaSenha == ConfirmarSenha, "Nova Senha e Confirmação são diferentes.");

                await Business.Current.AlterarSenha(Usuario, Nome, SenhaAtual, NovaSenha);
                await DisplayMessage("Senha alterada com sucesso.");
                await Owner.Navigation.PopAsync();
            }, ProtectedFlags.SetWriting);
        }

        private string _Nome = Business.Current.UsuarioLogado.Nome;
        public string Nome
        {
            get => _Nome;
            set
            {
                if (_Nome != value)
                {
                    _Nome = value;
                    FirePropertyChanged("Nome");
                }
            }
        }

        private string _Usuario = Business.Current.UsuarioLogado.Login;
        public string Usuario
        {
            get => _Usuario;
            set
            {
                if (_Usuario != value)
                {
                    _Usuario = value;
                    FirePropertyChanged("Usuario");
                }
            }
        }
        
        private string _SenhaAtual = null;
        public string SenhaAtual
        {
            get => _SenhaAtual;
            set
            {
                if (_SenhaAtual != value)
                {
                    _SenhaAtual = value;
                    FirePropertyChanged("SenhaAtual");
                }
            }
        }
        
        private string _NovaSenha = null;
        public string NovaSenha
        {
            get => _NovaSenha;
            set
            {
                if (_NovaSenha != value)
                {
                    _NovaSenha = value;
                    FirePropertyChanged("NovaSenha");
                }
            }
        }
        
        private string _ConfirmarSenha = null;
        public string ConfirmarSenha
        {
            get => _ConfirmarSenha;
            set
            {
                if (_ConfirmarSenha != value)
                {
                    _ConfirmarSenha = value;
                    FirePropertyChanged("ConfirmarSenha");
                }
            }
        }

        public ICommand AlterarSenhaCommand { get; set; }
    }
}
