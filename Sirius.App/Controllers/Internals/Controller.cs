﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using Xamarin.Forms;

namespace Sirius.App
{
    [Flags]
    public enum ProtectedFlags
    {
        None = 0,
        SetWriting = 1,
        SetLoading = 2
    }

    public class Controller : INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler PropertyChanged;

        protected virtual void FirePropertyChanged([CallerMemberName]string propertyName = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }

        public virtual bool OnBackButtonPressed()
        {
            return false;
        }

        protected bool SetProperty<T>(ref T storage, T value, string linkedPropertyName = null, Action action = null, [CallerMemberName] string propertyName = null)
        {
            if (EqualityComparer<T>.Default.Equals(storage, value))
                return false;

            storage = value;
            FirePropertyChanged(propertyName);
            if (!string.IsNullOrWhiteSpace(linkedPropertyName))
                FirePropertyChanged(linkedPropertyName);

            action?.Invoke();

            return true;
        }

        public VContentPage Owner { get; private set; }

        private bool _IsLoading = false;
        public bool IsLoading
        {
            get => _IsLoading;
            set
            {
                if (_IsLoading != value)
                {
                    _IsLoading = value;
                    FirePropertyChanged("IsLoading");
                    FirePropertyChanged("IsNotLoading");
                }
            }
        }

        public bool IsNotLoading
        {
            get
            {
                return !IsLoading;
            }
        }

        private bool _IsWriting = false;
        public bool IsWriting
        {
            get => _IsWriting;
            set
            {
                if (_IsWriting != value)
                {
                    _IsWriting = value;
                    FirePropertyChanged("IsWriting");
                    FirePropertyChanged("IsNotWriting");
                }
            }
        }

        public bool IsNotWriting
        {
            get
            {
                return !IsWriting;
            }
        }

        private string _Title = null;
        public string Title
        {
            get => _Title;
            set
            {
                if (_Title != value)
                {
                    _Title = value;
                    FirePropertyChanged("Title");
                }
            }
        }

        public ICommand GoBackCommand { get; private set; }
        public ICommand ShowMenuCommand { get; private set; }
        protected ICommand CreateCommand(Action action)
        {
            return new Command(action);
        }
        protected ICommand CreateCommand<T>(Action<T> action)
        {
            return new Command<T>(action);
        }

        protected ICommand CreateCommand<T>(Action<T> action, Func<T, bool> canExecute)
        {
            return new Command<T>(action, canExecute);
        }

        protected async Task DisplayError(string message)
        {
            await Owner.DisplayAlert("Erro", message, "OK");
        }
        protected async Task DisplayMessage(string message)
        {
            await Owner.DisplayAlert("Mensagem", message, "OK");
        }
        protected async Task<bool> Ask(string message)
        {
            var result = await Owner.DisplayActionSheet(message, "Não", "Sim");
            return result == "Sim";
        }

        private bool _GoneBack = false;
        public Controller(VContentPage owner)
        {
            this.Owner = owner;
            this.GoBackCommand = CreateCommand(async () =>
            {
                if (_GoneBack)
                    return;

                _GoneBack = true;
                if (Owner.Navigation.NavigationStack.Count > 1)
                    await Owner.Navigation.PopAsync();
            });
            this.ShowMenuCommand = CreateCommand(() => ShowMenu());
        }

        private void ShowMenu()
        {
            var masterDetail = GoToMasterPage();
            if (masterDetail != null)
                masterDetail.IsPresented = true;
        }
        private Xamarin.Forms.MasterDetailPage GoToMasterPage()
        {
            try
            {
                Element current = Owner;
                while (current != null && !(current is Xamarin.Forms.MasterDetailPage))
                {
                    current = current.Parent;
                }

                if (current != null)
                    return (Xamarin.Forms.MasterDetailPage)current;
                else
                {
                    foreach (var page in Owner.Navigation.NavigationStack)
                    {
                        if(page is Xamarin.Forms.MasterDetailPage)
                            return (Xamarin.Forms.MasterDetailPage)page;
                    }

                    return null;
                }
            }
            catch
            {
                return null;
            }
        }

        protected void ClearHistory()
        {
            while (Owner.Navigation.NavigationStack.Count > 1)
            {
                var page = Owner.Navigation.NavigationStack[0];
                Owner.Navigation.RemovePage(page);
            }
        }

        private bool _ShownOnce = false;
        public async Task FireShown()
        {
            try
            {
                HasAppeared = true;

                if (!_ShownOnce)
                    await OnAppearing();

                await OnShowing();
            }
            finally
            {
                _ShownOnce = true;
            }
        }

        protected bool HasAppeared { get; private set; }
        protected virtual Task OnAppearing() { return Task.FromResult(false); }
        protected virtual Task OnShowing() { return Task.FromResult(false); }

        protected ControllerValidation Validate { get; private set; } = new ControllerValidation();

        protected async Task Protected(Func<Task> task, ProtectedFlags flags = ProtectedFlags.None)
        {
            if ((flags & ProtectedFlags.SetLoading) == ProtectedFlags.SetLoading)
                IsLoading = true;
            if ((flags & ProtectedFlags.SetWriting) == ProtectedFlags.SetWriting)
                IsWriting = true;

            try
            {
                await task();
            }
            catch (Exception e)
            {
                await DisplayError(e.Message);
            }
            finally
            {
                if ((flags & ProtectedFlags.SetLoading) == ProtectedFlags.SetLoading)
                    IsLoading = false;
                if ((flags & ProtectedFlags.SetWriting) == ProtectedFlags.SetWriting)
                    IsWriting = false;
            }
        }
    }

    public class ControllerValidation
    {
        private static bool IsValidEmail(string strIn)
        {
            if (String.IsNullOrEmpty(strIn))
                return false;

            //Replace all unicode chars with ascii "A"
            strIn = System.Text.RegularExpressions.Regex.Replace(strIn, "[^\x0d\x0a\x20-\x7e\t]", "A");

            // Return true if strIn is in valid e-mail format. 
            try
            {
                return System.Text.RegularExpressions.Regex.IsMatch(strIn,
                      @"^(?("")("".+?(?<!\\)""@)|(([0-9a-z]((\.(?!\.))|[-!#\$%&'\*\+/=\?\^`\{\}\|~\w])*)(?<=[0-9a-z])@))" +
                      @"(?(\[)(\[(\d{1,3}\.){3}\d{1,3}\])|(([0-9a-z][-\w]*[0-9a-z]*\.)+[a-z0-9][\-a-z0-9]{0,22}[a-z0-9]))$",
                      System.Text.RegularExpressions.RegexOptions.IgnoreCase, TimeSpan.FromMilliseconds(250));
            }
            catch (System.Text.RegularExpressions.RegexMatchTimeoutException)
            {
                return false;
            }
        }

        public void Number(string text, string message)
        {
            for (int i = 0; i < text.Length; i++)
            {
                if (!char.IsDigit(text[i]))
                    throw new BusinessException(message);
            }
        }

        public void Email(string text, string message)
        {
            if (!IsValidEmail(text))
                throw new BusinessException(message);
        }

        public void NotEmpty(string text, string message)
        {
            if (string.IsNullOrWhiteSpace(text))
                throw new BusinessException(message);
        }

        public void Condition(bool condition, string message)
        {
            if (!condition)
                throw new BusinessException(message);
        }
        public void StringLength(string text,int length, string message)
        {
            if (text.Length < length)
                throw new BusinessException(message);            
        }

        public void PositiveInteger(string text, string message)
        {
            try
            {
                var i = int.Parse(text);
                if (i <= 0)
                    throw new BusinessException(message);
            }
            catch (BusinessException)
            {
                throw;
            }
            catch
            {
                throw new BusinessException(message);
            }
        }

        public void Phone(string telefone, string message)
        {
            if (telefone.Cast<char>().Any(c => !char.IsDigit(c) && c != '(' && c != ')' && c != '-' && !char.IsWhiteSpace(c)))
                throw new BusinessException(message);

            var phone = new string(telefone.Cast<char>().Where(c => char.IsDigit(c)).ToArray());
            if (phone.Length < 8 || phone.Length > 11)
                throw new BusinessException(message);
        }
    }
}
