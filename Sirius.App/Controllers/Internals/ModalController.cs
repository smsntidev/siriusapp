﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using Xamarin.Forms;

namespace Sirius.App
{
    public class ModalController : INotifyPropertyChanged
    {
        protected void HideMe()
        {
            Owner.ParentController.Owner.HideModal();
        }

        public virtual bool OnBackButtonPressed()
        {
            return false;
        }

        public event PropertyChangedEventHandler PropertyChanged;
        protected void FirePropertyChanged(string propertyName)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }

        public VContentView Owner { get; private set; }

        private bool _IsLoading = false;
        public bool IsLoading
        {
            get => _IsLoading;
            set
            {
                if (_IsLoading != value)
                {
                    _IsLoading = value;
                    FirePropertyChanged("IsLoading");
                    FirePropertyChanged("IsNotLoading");
                }
            }
        }

        public bool IsNotLoading
        {
            get
            {
                return !IsLoading;
            }
            set
            {
                if (_IsLoading == value)
                {
                    _IsLoading = !value;
                    FirePropertyChanged("IsLoading");
                    FirePropertyChanged("IsNotLoading");
                }
            }
        }

        private bool _IsWriting = false;
        public bool IsWriting
        {
            get => _IsWriting;
            set
            {
                if (_IsWriting != value)
                {
                    _IsWriting = value;
                    FirePropertyChanged("IsWriting");
                    FirePropertyChanged("IsNotWriting");
                }
            }
        }

        public bool IsNotWriting
        {
            get
            {
                return !IsWriting;
            }
        }

        public ICommand GoBackCommand { get; private set; }
        protected ICommand CreateCommand(Action action)
        {
            return new Command(action);
        }

        protected async Task DisplayError(string message)
        {
            await Owner.ParentController.Owner.DisplayAlert("Erro", message, "OK");
        }
        protected async Task DisplayMessage(string message)
        {
            await Owner.ParentController.Owner.DisplayAlert("Mensagem", message, "OK");
        }

        public ModalController(VContentView owner)
        {
            this.Owner = owner;
            this.GoBackCommand = CreateCommand(() => { Owner.ParentController.Owner.HideModal(); });
        }

        private bool _ShownOnce = false;
        public async Task FireShown()
        {
            try
            {
                if (!_ShownOnce)
                    await OnAppearing();

                await OnShowing();
            }
            finally
            {
                _ShownOnce = true;
            }
        }

        protected virtual Task OnAppearing()
        {
            return Task.FromResult(false);
        }
        protected virtual Task OnShowing()
        {
            return Task.FromResult(false);
        }

        protected ControllerValidation Validate { get; private set; } = new ControllerValidation();

        protected async Task Protected(Func<Task> task, ProtectedFlags flags = ProtectedFlags.None)
        {
            if ((flags & ProtectedFlags.SetLoading) == ProtectedFlags.SetLoading)
                IsLoading = true;
            if ((flags & ProtectedFlags.SetWriting) == ProtectedFlags.SetWriting)
                IsWriting = true;

            try
            {
                await task();
            }
            catch (Exception e)
            {
                await DisplayError(e.Message);
            }
            finally
            {
                if ((flags & ProtectedFlags.SetLoading) == ProtectedFlags.SetLoading)
                    IsLoading = false;
                if ((flags & ProtectedFlags.SetWriting) == ProtectedFlags.SetWriting)
                    IsWriting = false;
            }
        }
    }
}
