﻿using Sirius.App;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Xamarin.Forms;

namespace Sirius.App
{
    public partial class App : Application
    {
        public App()
        {
            InitializeComponent();

            var navigationPage = new VNavigationPage(new LoginPage());

            navigationPage.BarTextColor = Color.White;
            navigationPage.BarBackgroundColor = Color.FromHex("#1072bc");
            MainPage = navigationPage;
        }

        protected override void OnStart()
        {
            // Handle when your app starts
        }

        protected override void OnSleep()
        {
            // Handle when your app sleeps
        }

        protected override void OnResume()
        {
            // Handle when your app resumes
        }
    }
}