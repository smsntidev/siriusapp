﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace Sirius.App
{
    public class VWebView : WebView
    {
        public static readonly BindableProperty UriProperty = BindableProperty.Create(propertyName: "Uri",
            returnType: typeof(string),
            declaringType: typeof(VWebView),
            defaultValue: default(string));
        public static readonly BindableProperty PdfBytesProperty = BindableProperty.Create(propertyName: "PdfBytes",
            returnType: typeof(byte[]),
            declaringType: typeof(VWebView),
            defaultValue: default(byte[]));

        public string Uri
        {
            get { return (string)GetValue(UriProperty); }
            set { SetValue(UriProperty, value); }
        }

        public byte[] PdfBytes
        {
            get { return (byte[])GetValue(PdfBytesProperty); }
            set { SetValue(PdfBytesProperty, value); }
        }
    }
}
