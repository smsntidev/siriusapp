﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace Sirius.App
{
    public class VContentView : ContentView
    {
        public VContentView()
        {
            Controller = new ModalController(this);
        }

        public ModalController Controller
        {
            get => BindingContext as ModalController;
            set => BindingContext = value;
        }

        public Controller ParentController
        {
            get; set;
        }
    }
}
