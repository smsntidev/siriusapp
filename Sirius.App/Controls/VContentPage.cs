﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using Xamarin.Forms;

namespace Sirius.App
{
    public class VContentPage : ContentPage
    {
        public static readonly BindableProperty IsLoadingProperty =
           BindableProperty.Create("IsLoading", typeof(bool), typeof(VContentPage), false);
        public static readonly BindableProperty GoBackCommandProperty =
            BindableProperty.Create("GoBackCommand", typeof(ICommand), typeof(VContentPage), null);
        public static readonly BindableProperty ShowMenuCommandProperty =
            BindableProperty.Create("ShowMenuCommand", typeof(ICommand), typeof(VContentPage), null);
        public static readonly BindableProperty AddCommandProperty =
            BindableProperty.Create("AddCommand", typeof(ICommand), typeof(VContentPage), null);
        public static readonly BindableProperty IsPopupVisibleProperty =
            BindableProperty.Create("IsPopupVisible", typeof(bool), typeof(VContentPage), false);
        public static readonly BindableProperty PopupContentProperty =
            BindableProperty.Create("PopupContent", typeof(View), typeof(VContentPage), null);
        public static readonly BindableProperty ClosePopupCommandProperty =
            BindableProperty.Create("ClosePopupCommand", typeof(ICommand), typeof(VContentPage), null);

        public VContentPage()
        {
            Controller = new Controller(this);
            ClosePopupCommand = new Command(() =>
            {
                HideModal();
            });
        }

        public bool IsLoading
        {
            get { return (bool)GetValue(IsLoadingProperty); }
            set { SetValue(IsLoadingProperty, value); }
        }
        public ICommand GoBackCommand
        {
            get { return (ICommand)GetValue(GoBackCommandProperty); }
            set { SetValue(GoBackCommandProperty, value); }
        }
        public ICommand ShowMenuCommand
        {
            get { return (ICommand)GetValue(ShowMenuCommandProperty); }
            set { SetValue(ShowMenuCommandProperty, value); }
        }
        public ICommand AddCommand
        {
            get { return (ICommand)GetValue(AddCommandProperty); }
            set { SetValue(AddCommandProperty, value); }
        }
        public bool IsPopupVisible
        {
            get { return (bool)GetValue(IsPopupVisibleProperty); }
            set { SetValue(IsPopupVisibleProperty, value); }
        }
        public View PopupContent
        {
            get { return (View)GetValue(PopupContentProperty); }
            set { SetValue(PopupContentProperty, value); }
        }
        public ICommand ClosePopupCommand
        {
            get { return (ICommand)GetValue(ClosePopupCommandProperty); }
            set { SetValue(ClosePopupCommandProperty, value); }
        }

        public Controller Controller
        {
            get => BindingContext as Controller;
            set => BindingContext = value;
        }

        protected override async void OnAppearing()
        {
            if (Controller != null)
                await Controller.FireShown();
        }

        public async Task DisplayModal(VContentView content)
        {
            content.ParentController = Controller;
            this.PopupContent = content;
            this.IsPopupVisible = true;

            if (content.Controller != null)
                await content.Controller.FireShown();
        }
        public void HideModal()
        {
            this.PopupContent = null;
            this.IsPopupVisible = false;
        }

        protected override bool OnBackButtonPressed()
        {
            if (IsPopupVisible)
            {
                var content = PopupContent as VContentView;
                if (content != null && content.Controller != null)
                {
                    if (!content.Controller.OnBackButtonPressed())
                    {
                        HideModal();
                        return true;
                    }
                    else
                        return true;
                }
                else
                {
                    if (Controller != null)
                    {
                        if (!Controller.OnBackButtonPressed())
                        {
                            return base.OnBackButtonPressed();
                        }
                        else
                            return true;
                    }
                    else
                        return base.OnBackButtonPressed();
                }
            }
            else
            {
                if (Controller != null)
                {
                    if (!Controller.OnBackButtonPressed())
                    {
                        return base.OnBackButtonPressed();
                    }
                    else
                        return true;
                }
                else
                    return base.OnBackButtonPressed();
            }
        }
    }
}
