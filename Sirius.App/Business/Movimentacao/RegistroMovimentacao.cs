﻿using Sirius.App.Server;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sirius.App
{
    public class RegistroMovimentacao
    {
        public int Codigo { get; set; }

        public int CodigoTipoMovimentacao { get; set; }

        public string CodigoTipoDestinatario { get; set; }

        public DateTime DataHora { get; set; }

        public DateTime DataRetorno { get; set; }

        public string Observacao { get; set; }

        public Estabelecimento EstabelecimentoCedenteModel { get; set; }

        public Estabelecimento EstabelecimentoRequisitanteModel { get; set; }

        public int CodigoUnidadeSaude { get; set; }

        public UsuarioModel UsuarioCedenteModel { get; set; }

        public UsuarioModel UsuarioRequisitanteModel { get; set; }

        public int CodigoDestinatario { get; set; }

        public List<Item> Itens { get; set; }

        public List<Item> ItensExcluidos { get; set; }

        public List<Item> ItensSemIdentificacao { get; set; }

        public char TipoOutros { get; set; }

        public char TipoSolicitacao { get; set; }

        public RegistroMovimentacaoModel CreateServerModel()
        {
            return new RegistroMovimentacaoModel()
            {
                Codigo = Codigo,
                CodigoDestinatario = CodigoDestinatario,
                CodigoTipoDestinatario = CodigoTipoDestinatario,
                CodigoTipoMovimentacao = CodigoTipoMovimentacao,
                CodigoUnidadeSaude = CodigoUnidadeSaude,
                DataHora = DataHora,
                DataRetorno = DataRetorno,
                Observacao = Observacao,
                TipoSolicitacao = TipoSolicitacao,
                TipoOutros = TipoOutros,
                EstabelecimentoRequisitanteModel = EstabelecimentoRequisitanteModel != null ? new EstabelecimentoModel()
                {
                    Codigo = EstabelecimentoRequisitanteModel.Codigo,
                    NomeEstabelecimento = EstabelecimentoRequisitanteModel.NomeEstabelecimento,
                    Tipo = EstabelecimentoRequisitanteModel.Tipo,
                    Excluido = EstabelecimentoRequisitanteModel.Excluido,
                } : null,
                EstabelecimentoCedenteModel = EstabelecimentoCedenteModel  != null ? new EstabelecimentoModel()
                {
                    Codigo = EstabelecimentoCedenteModel.Codigo,
                    NomeEstabelecimento = EstabelecimentoCedenteModel.NomeEstabelecimento,
                    Tipo = EstabelecimentoCedenteModel.Tipo,
                    Excluido = EstabelecimentoCedenteModel.Excluido,
                }: null,
                UsuarioCedenteModel = UsuarioCedenteModel, 
                UsuarioRequisitanteModel = UsuarioRequisitanteModel,
                Itens = Itens.Select(x => new ItemModel()
                {
                    Codigo = x.Codigo,
                    CodigoEstabelecimento = x.CodigoEstabelecimento,
                    CodigoMarca = x.CodigoMarca,
                    CodigoMaterial = x.CodigoMaterial,
                    CodigoSetor = x.CodigoSetor,
                    CodigoSituacaoFisica = x.CodigoSituacaoFisica,
                    DescricaoMaterialInformatica = x.DescricaoMaterialInformatica,
                    IBT = x.IBT,
                    IdEntrada = x.IdEntrada,
                    IdInventario = x.IdInventario,
                    IDPBM = x.IDPBM,
                    IdTipoEntrada = x.IdTipoEntrada,
                    IPBM = x.IPBM,
                    Marca = x.Marca,
                    Modelo = x.Modelo,
                    NomeEstabelecimento = x.NomeEstabelecimento,
                    NomeSetor = x.NomeSetor,
                    NumeroControleEmpresa = x.NumeroControleEmpresa,
                    Quantidade = x.Quantidade,
                    SemIdentificacao = x.SemIdentificacao,
                    Serial = x.Serial,
                    SituacaoFisica = x.SituacaoFisica,
                    TipoIBT = x.TipoIBT,
                    Tombo = x.Tombo,
                    Tombo2 = x.Tombo2,
                    Material = new MaterialModel()
                    {
                        Codigo = x.Material.Codigo,
                        Identificador = x.Material.Identificador,
                        CodigoClasse = x.Material.CodigoClasse,
                        NomeClasse = x.Material.NomeClasse,
                        NomeMaterial = x.Material.NomeMaterial,
                        Excluido = x.Material.Excluido
                    }
                }).ToList(),
                ItensExcluidos = ItensExcluidos?.Select(x => new ItemModel()
                {
                    Codigo = x.Codigo,
                    CodigoEstabelecimento = x.CodigoEstabelecimento,
                    CodigoMarca = x.CodigoMarca,
                    CodigoMaterial = x.CodigoMaterial,
                    CodigoSetor = x.CodigoSetor,
                    CodigoSituacaoFisica = x.CodigoSituacaoFisica,
                    DescricaoMaterialInformatica = x.DescricaoMaterialInformatica,
                    IBT = x.IBT,
                    IdEntrada = x.IdEntrada,
                    IdInventario = x.IdInventario,
                    IDPBM = x.IDPBM,
                    IdTipoEntrada = x.IdTipoEntrada,
                    IPBM = x.IPBM,
                    Marca = x.Marca,
                    Modelo = x.Modelo,
                    NomeEstabelecimento = x.NomeEstabelecimento,
                    NomeSetor = x.NomeSetor,
                    NumeroControleEmpresa = x.NumeroControleEmpresa,
                    Quantidade = x.Quantidade,
                    SemIdentificacao = x.SemIdentificacao,
                    Serial = x.Serial,
                    SituacaoFisica = x.SituacaoFisica,
                    TipoIBT = x.TipoIBT,
                    Tombo = x.Tombo,
                    Tombo2 = x.Tombo2,
                    Material = new MaterialModel()
                    {
                        Codigo = x.Material.Codigo,
                        Identificador = x.Material.Identificador,
                        CodigoClasse = x.Material.CodigoClasse,
                        NomeClasse = x.Material.NomeClasse,
                        NomeMaterial = x.Material.NomeMaterial,
                        Excluido = x.Material.Excluido
                    }
                }).ToList(),
                ItensSemIdentificacao = ItensSemIdentificacao?.Select(x => new ItemModel()
                {
                    Codigo = x.Codigo,
                    CodigoEstabelecimento = x.CodigoEstabelecimento,
                    CodigoMarca = x.CodigoMarca,
                    CodigoMaterial = x.CodigoMaterial,
                    CodigoSetor = x.CodigoSetor,
                    CodigoSituacaoFisica = x.CodigoSituacaoFisica,
                    DescricaoMaterialInformatica = x.DescricaoMaterialInformatica,
                    IBT = x.IBT,
                    IdEntrada = x.IdEntrada,
                    IdInventario = x.IdInventario,
                    IDPBM = x.IDPBM,
                    IdTipoEntrada = x.IdTipoEntrada,
                    IPBM = x.IPBM,
                    Marca = x.Marca,
                    Modelo = x.Modelo,
                    NomeEstabelecimento = x.NomeEstabelecimento,
                    NomeSetor = x.NomeSetor,
                    NumeroControleEmpresa = x.NumeroControleEmpresa,
                    Quantidade = x.Quantidade,
                    SemIdentificacao = x.SemIdentificacao,
                    Serial = x.Serial,
                    SituacaoFisica = x.SituacaoFisica,
                    TipoIBT = x.TipoIBT,
                    Tombo = x.Tombo,
                    Tombo2 = x.Tombo2,
                    Material = new MaterialModel()
                    {
                        Codigo = x.Material.Codigo,
                        Identificador = x.Material.Identificador,
                        CodigoClasse = x.Material.CodigoClasse,
                        NomeClasse = x.Material.NomeClasse,
                        NomeMaterial = x.Material.NomeMaterial,
                        Excluido = x.Material.Excluido
                    }
                }).ToList()
            };
        }
    }
}