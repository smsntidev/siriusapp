﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sirius.App
{
    public class MovimentacaoFiltro
    {
        public int TipoMovimentacao { get; set; }

        public int Numero { get; set; }

        public DateTime Data { get; set; }

        public int TipoIdentificacao { get; set; }

        public string Tombo { get; set; }

        public long IBT { get; set; }

        public long IPBM { get; set; }
    }
}
