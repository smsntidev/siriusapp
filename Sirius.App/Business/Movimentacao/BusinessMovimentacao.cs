﻿using Sirius.App.Server;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sirius.App
{
    public partial class Business
    {
        public async Task<List<NaturezaModel>> GetNaturezas()
        {
            try
            {
                return await Access.Current.CachedGet<List<NaturezaModel>>("Dominio/GetNaturezas/");
            }
            catch (BusinessException)
            {
                throw;
            }
            catch (Exception)
            {
                throw new BusinessException("Servidor Offline.");
            }
        }

        public async Task<List<SituacaoFisica>> GetSituacoesFisicas()
        {
            try
            {
                var resposta = await Access.Current.CachedGet<List<SituacaoFisicaModel>>("Dominio/GetSituacoesFisicas/");

                return resposta.Select(x => new SituacaoFisica(x.Codigo, x.Nome, x.Excluido)).ToList();
            }
            catch (BusinessException)
            {
                throw;
            }
            catch (Exception)
            {
                throw new BusinessException("Servidor Offline.");
            }
        }

        public async Task<List<Setor>> GetSetores()
        {
            try
            {
                var resposta = await Access.Current.CachedGet<List<SetorModel>>("Dominio/GetSetores/");

                return resposta.Select(x => new Setor(x.Codigo, x.CodigoEstabelecimento, x.Nome, x.Excluido)).ToList();
            }
            catch (BusinessException)
            {
                throw;
            }
            catch (Exception)
            {
                throw new BusinessException("Servidor Offline.");
            }
        }

        public async Task<List<Destinatario>> GetDestinatarios()
        {
            try
            {
                var resposta = await Access.Current.CachedGet<List<Destinatario>>("Dominio/GetDestinatarios/");

                return resposta.Select(x => new Destinatario(x.Codigo, x.RazaoSocial, x.Excluido)).ToList();
            }
            catch (BusinessException)
            {
                throw;
            }
            catch (Exception)
            {
                throw new BusinessException("Servidor Offline.");
            }
        }

        public async Task<List<TipoMovimentacao>> GetTiposMovimentacoes()
        {
            try
            {
                var resposta = await Access.Current.CachedGet<List<TipoMovimentacaoModel>>("Movimentacao/GetTiposMovimentacoes/");

                return resposta.Select(x => new TipoMovimentacao(x.Codigo, x.Nome, x.Excluido)).ToList();
            }
            catch (BusinessException)
            {
                throw;
            }
            catch (Exception)
            {
                throw new BusinessException("Servidor Offline.");
            }
        }

        public async Task<List<Movimentacao>> GetMovimentacoesByFiltro(string identificador, int tipoIdentificacao)
        {
            try
            {
                var resposta = await Access.Current.Post<List<MovimentacaoModel>, MovimentacaoFiltroModel>
               ("Movimentacao/GetMovimentacoesByFiltro/", new MovimentacaoFiltroModel()
               {
                   Tombo = tipoIdentificacao == 0 ? identificador.ToString() : null,
                   IBT = tipoIdentificacao == 1 ? long.Parse(identificador.ToString()) : 0,
                   IPBM = tipoIdentificacao == 2 ? long.Parse(identificador.ToString()) : 0,
                   TipoIdentificacao = tipoIdentificacao
               });

                return resposta.Select(a => new Movimentacao()
                {
                    Codigo = a.Codigo,
                    CodigoTipoMovimentacao = a.CodigoTipoMovimentacao,
                    CodigoDestinatario = a.CodigoDestinatario,
                    Observacao = a.Observacao,
                    DataHora = a.DataHora,
                    DataRetorno = a.DataRetorno,
                    TipoSolicitacao = a.TipoSolicitacao,
                    TipoOutros = a.TipoOutros,
                    Destinatario = a.Destinatario != null ? new Destinatario(a.Destinatario.Codigo, a.Destinatario.RazaoSocial, a.Destinatario.Excluido) : null,
                    EstabelecimentoRequisitanteModel = a.EstabelecimentoRequisitanteModel != null ? new Estabelecimento()
                    {
                        Codigo = a.EstabelecimentoRequisitanteModel?.Codigo ?? 0,
                        NomeEstabelecimento = a.EstabelecimentoRequisitanteModel?.NomeEstabelecimento ?? "Teste",
                        Tipo = a.EstabelecimentoRequisitanteModel?.Tipo ?? "C",
                        Excluido = a.EstabelecimentoRequisitanteModel?.Excluido ?? false,
                    } : null,
                    EstabelecimentoCedenteModel = a.EstabelecimentoCedenteModel != null ? new Estabelecimento()
                    {
                        Codigo = a.EstabelecimentoCedenteModel.Codigo,
                        NomeEstabelecimento = a.EstabelecimentoCedenteModel.NomeEstabelecimento,
                        Tipo = a.EstabelecimentoCedenteModel.Tipo,
                        Excluido = a.EstabelecimentoCedenteModel.Excluido,
                    } : null,
                    UsuarioCedenteModel = a.UsuarioCedenteModel,
                    UsuarioRequisitanteModel = a.UsuarioRequisitanteModel,
                    Itens = a.Itens.Select(x => new Item()
                    {
                        Codigo = x.Codigo,
                        CodigoEstabelecimento = x.CodigoEstabelecimento,
                        CodigoMarca = x.CodigoMarca,
                        CodigoMaterial = x.CodigoMaterial,
                        CodigoSetor = x.CodigoSetor,
                        CodigoSituacaoFisica = x.CodigoSituacaoFisica,
                        DescricaoMaterialInformatica = x.DescricaoMaterialInformatica,
                        IBT = x.IBT,
                        IdEntrada = x.IdEntrada,
                        IdInventario = x.IdInventario,
                        IDPBM = x.IDPBM,
                        IdTipoEntrada = x.IdTipoEntrada,
                        IPBM = x.IPBM,
                        Marca = x.Marca,
                        Modelo = x.Modelo,
                        NomeEstabelecimento = x.NomeEstabelecimento,
                        NomeSetor = x.NomeSetor,
                        NumeroControleEmpresa = x.NumeroControleEmpresa ?? string.Empty,
                        Quantidade = x.Quantidade,
                        SemIdentificacao = x.SemIdentificacao,
                        Serial = x.Serial ?? string.Empty,
                        SituacaoFisica = x.SituacaoFisica,
                        TipoIBT = x.TipoIBT,
                        Tombo = x.Tombo,
                        Tombo2 = x.Tombo2 ?? string.Empty,
                        Material = new Material(x.Material.Codigo, x.Material.Identificador, x.Material.CodigoClasse, x.Material.NomeClasse, x.Material.NomeMaterial, x.Material.Excluido)
                    }).ToList()
                }).ToList();
            }
            catch (BusinessException)
            {
                throw;
            }
            catch (Exception)
            {
                throw new BusinessException("Servidor Offline.");
            }
        }

        public async Task<int> RegistrarMovimentacao(RegistroMovimentacao movimentacao)
        {
            try
            {
                return await Access.Current.Post<int, RegistroMovimentacaoModel>("Movimentacao/RegistrarMovimentacao/", movimentacao.CreateServerModel());

            }
            catch (BusinessException ex)
            {
                throw ex;
            }
            catch (Exception)
            {
                throw new BusinessException("Servidor Offline.");
            }
        }

    }
}