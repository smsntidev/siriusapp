﻿using Sirius.App.Server;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sirius.App
{
    public class Movimentacao
    {
        public Movimentacao()
        {
            Itens = new List<Item>();
            ItensExcluidos = new List<Item>();
        }

        public int Codigo { get; set; }

        public int CodigoTipoMovimentacao { get; set; }

        public DateTime DataHora { get; set; }

        public DateTime DataRetorno { get; set; }

        public string Observacao { get; set; }

        public Estabelecimento EstabelecimentoCedenteModel { get; set; }

        public Estabelecimento EstabelecimentoRequisitanteModel { get; set; }

        public Setor SetorModel { get; set; }

        public UsuarioModel UsuarioCedenteModel { get; set; }

        public UsuarioModel UsuarioRequisitanteModel { get; set; }

        public int CodigoDestinatario { get; set; }

        public Destinatario Destinatario { get; set; }

        public char TipoOutros { get; set; }

        public char TipoSolicitacao { get; set; }

        public List<Item> Itens { get; set; }
        public List<Item> ItensExcluidos { get; set; }
    }
}
