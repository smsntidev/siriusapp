﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sirius.App
{
    public class SituacaoFisica
    {
        public SituacaoFisica(int codigo, string nome, bool excluido)
        {
            Codigo = codigo;
            Nome = nome;
            Excluido = excluido;
        }

        public int Codigo { get; set; }
        public string Nome { get; set; }
        public bool Excluido { get; set; }

        public override string ToString() => Nome;
    }
}
