﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sirius.App
{
    public class Usuario
    {
        public int Codigo { get; set; }

        public string Login { get; set; }

        public string Nome { get; set; }

        public int CodigoPerfil { get; set; }

        public bool Ativo { get; set; }
    }
}
