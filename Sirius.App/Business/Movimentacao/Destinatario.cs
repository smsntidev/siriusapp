﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sirius.App
{
    public class Destinatario
    {
        public Destinatario(int codigo, string razaoSocial, bool excluido)
        {
            Codigo = codigo;
            RazaoSocial = razaoSocial;
            Excluido = excluido;
        }

        public int Codigo { get; set; }

        public string RazaoSocial { get; set; }

        public bool Excluido { get; set; }

        public override string ToString() => RazaoSocial;
    }
}
