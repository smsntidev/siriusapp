﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Sirius.App.Server;
using Xamarin.Forms;

namespace Sirius.App
{
    public partial class Business
    {
        private async Task SalvarUsuarioLogado()
        {
            try
            {
                var usuario = await Access.Current.Get<UsuarioModel>("Dominio/GetUsuario");
                usuario.Login = usuario.Login?.Trim();
                usuario.Nome = usuario.Nome?.Trim();
                Set<UsuarioModel>(SavedValues.UserData, usuario);
                await Save();
            }
            catch (BusinessException)
            {
                throw;
            }
            catch (Exception)
            {
                throw new BusinessException("Servidor Offline.");
            }
        }

        public async Task AlterarSenha(string login, string nome, string senhaAtual, string novaSenha)
        {
            try
            {
                await Access.Current.Post<string, AlterarSenhaModel>("Autentica/AlteraSenha", new AlterarSenhaModel()
                {
                    ConfirmarSenha = novaSenha,
                    SenhaAtual = senhaAtual,
                    Login = login,
                    Nome = nome,
                    NovaSenha = novaSenha
                });
                Set<string>(SavedValues.Password, novaSenha);
                await Save();
            }
            catch (BusinessException)
            {
                throw;
            }
            catch (Exception)
            {
                throw new BusinessException("Servidor Offline.");
            }
        }

        private async Task LoginInterno(string usuario, string senha, bool apenasLive)
        {
            try
            {
                var resposta = await Access.Current.Login(usuario, senha);

                if (resposta == false)
                {
                    //Login offline
                    if (!apenasLive &&
                        Get<string>(SavedValues.Username) != null &&
                        Get<string>(SavedValues.Password) != null)
                    {
                        return;
                    }
                    else
                        throw new BusinessException("Usuário/Senha inválidos.");
                }
                else
                {
                    await SalvarUsuarioLogado();
                }
            }
            catch (BusinessException)
            {
                throw;
            }
            catch
            {
                //Login offline
                if (!apenasLive &&
                    Get<string>(SavedValues.Username) != null &&
                    Get<string>(SavedValues.Password) != null)
                {
                    return;
                }
                else
                    throw new BusinessException();
            }
        }

        public Task Login(string login, string senha)
        {
            return LoginInterno(login, senha, false);
        }

        public async Task<bool> Relogar()
        {
            if (Get<string>(SavedValues.Username) != null && Get<string>(SavedValues.Password) != null)
            {
                try
                {
                    await LoginInterno(
                        Get<string>(SavedValues.Username),
                        Get<string>(SavedValues.Password), 
                        true);
                    return true;
                }
                catch
                {
                    //qualquer exceção deve ser silenciada...
                    return false;
                }
            }
            else
                return false;
        }
        public async Task<bool> LogarAutomaticamente()
        {
            if (Get<string>(SavedValues.Username) != null &&
                Get<string>(SavedValues.Password) != null)
            {
                try
                {
                    //Teoricamente nunca ocorrerá um erro no login abaixo já que a função
                    //LoginInterno fará login offline. Mas nós colocamos dentro de um try/catch
                    //de todo modo.
                    await LoginInterno(Get<string>(SavedValues.Username), Get<string>(SavedValues.Password), false);
                    return true;
                }
                catch
                {
                    return false;
                }
            }
            else
                return false;
        }

        public async Task LogOff()
        {
            Application.Current.Properties.Clear();
            await Save();
        }

        public bool IsLogado
        {
            get
            {
                return Get<string>(SavedValues.Username) != null &&
                       Get<string>(SavedValues.Password) != null;
            }
        }

        public UsuarioModel UsuarioLogado => Get<UsuarioModel>(SavedValues.UserData);
    }
}
