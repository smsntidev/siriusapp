﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Sirius.App.Server;

namespace Sirius.App
{
    public partial class Business
    {

        public async Task AtualizarItemMaterial(Item item)
        {
            try
            {
                await Access.Current.Post<string, ItemModel>("Dominio/AtualizarItemMaterial/", item.CreateServerModel());
            }
            catch (BusinessException ex)
            {
                throw ex;
            }
            catch (Exception)
            {
                throw new BusinessException("Servidor Offline.");
            }
        }
    }
}
