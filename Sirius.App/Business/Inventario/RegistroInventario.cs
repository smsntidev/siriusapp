﻿using Sirius.App.Server;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sirius.App
{
    public class RegistroInventario
    {
        public string TipoEstabelecimento { get; set; }

        public int? IdEstabelecimento { get; set; }

        public int? IdSetor { get; set; }

        public int? IdResponsavelAtesto { get; set; }

        public string Observacao { get; set; }

        //public string CodigoBarras { get; set; }

        public bool InventarioFechado { get; set; }

        public int CodigoUsuario { get; set; }

        public List<Item> Itens { get; set; }

        public List<Item> ItensExcluidos { get; set; }

        public string Status { get; set; }

        public RegistroInventarioModel CreateServerModel()
        {
            return new RegistroInventarioModel()
            {
                TipoEstabelecimento = TipoEstabelecimento,
                IdEstabelecimento = IdEstabelecimento,
                IdSetor = IdSetor,
                IdResponsavelAtesto = IdResponsavelAtesto,
                Observacao = Observacao,
                //CodigoBarras = CodigoBarras,
                InventarioFechado = InventarioFechado,
                CodigoUsuario = CodigoUsuario,
                Itens = Itens.Select(x => new ItemModel()
                {
                    Codigo = x.Codigo,
                    CodigoEstabelecimento = x.CodigoEstabelecimento,
                    CodigoMarca = x.CodigoMarca,
                    CodigoMaterial = x.CodigoMaterial,
                    CodigoSetor = x.CodigoSetor,
                    CodigoSituacaoFisica = x.CodigoSituacaoFisica,
                    DescricaoMaterialInformatica = x.DescricaoMaterialInformatica,
                    IBT = x.IBT,
                    IdEntrada = x.IdEntrada,
                    IdInventario = x.IdInventario,
                    IDPBM = x.IDPBM,
                    IdTipoEntrada = x.IdTipoEntrada,
                    IPBM = x.IPBM,
                    Marca = x.Marca,
                    Modelo = x.Modelo,
                    NomeEstabelecimento = x.NomeEstabelecimento,
                    NomeSetor = x.NomeSetor,
                    NumeroControleEmpresa = x.NumeroControleEmpresa,
                    Quantidade = x.Quantidade,
                    SemIdentificacao = x.SemIdentificacao,
                    Serial = x.Serial,
                    SituacaoFisica = x.SituacaoFisica,
                    TipoIBT = x.TipoIBT,
                    Tombo = x.Tombo,
                    Tombo2 = x.Tombo2,
                    Material = new MaterialModel()
                    {
                        Codigo = x.Material.Codigo,
                        Identificador = x.Material.Identificador,
                        CodigoClasse = x.Material.CodigoClasse,
                        NomeClasse = x.Material.NomeClasse,
                        NomeMaterial = x.Material.NomeMaterial,
                        Excluido = x.Material.Excluido
                    }
                }).ToList(),
                ItensExcluidos = ItensExcluidos != null ? ItensExcluidos.Select(x => x.Codigo).ToList() : new List<int>()                
            };
        }

    }
}
