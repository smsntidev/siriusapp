﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using Xamarin.Forms;
using Sirius.App.Server;

namespace Sirius.App
{
    public class Item
    {
        public Item()
        {
        }

        public int Codigo { get; set; }

        public bool SemIdentificacao { get; set; }

        public int Tombo { get; set; }

        public int TipoIBT { get; set; }

        public long IBT { get; set; }

        public bool IPBM { get; set; }

        public long IDPBM { get; set; }

        public string NumeroDescricao => Tombo2.IsNotEmpty() ? Tombo2 : (IBT > 0 ? IBT.ToString() : IDPBM.ToString());

        #region Material
        public int CodigoMaterial { get; set; }

        public Material Material { get; set; }
        #endregion

        #region Marca
        public int CodigoMarca { get; set; }

        public string Marca { get; set; }
        #endregion

        public string Modelo { get; set; }

        public string Serial { get; set; }

        public string SerialDescricao => string.IsNullOrWhiteSpace(Serial) ? "Sem Serial" : Serial;

        #region SituacaoFisica
        public int CodigoSituacaoFisica { get; set; }

        public string SituacaoFisica { get; set; }
        #endregion 

        public int Quantidade { get; set; }

        public int IdInventario { get; set; }

        #region ReferenteAEntrada
        public int IdEntrada { get; set; }

        public int IdTipoEntrada { get; set; }
        #endregion

        #region Setor Origem
        public int CodigoSetor { get; set; }

        public string NomeSetor { get; set; }

        #endregion

        #region

        public int CodigoEstabelecimento { get; set; }

        public string NomeEstabelecimento { get; set; }

        #endregion
        #region Setor Destino  (lucas oliveira)
        public int CodigoSetorDestino { get; set; }

        public string NomeSetorDestino { get; set; }

        #endregion

        public string DescricaoMaterialInformatica { get; set; }

        public string NumeroControleEmpresa { get; set; }

        public string TomboToString { get; set; }

        public string Tombo2 { get; set; }

        public ItemModel CreateServerModel()
        {
            return new ItemModel()
            {
                Codigo = Codigo,
                CodigoEstabelecimento = CodigoEstabelecimento,
                CodigoMarca = CodigoMarca,
                CodigoMaterial = CodigoMaterial,
                CodigoSetor = CodigoSetor,
                CodigoSituacaoFisica = CodigoSituacaoFisica,
                DescricaoMaterialInformatica = DescricaoMaterialInformatica,
                IBT = IBT,
                IdEntrada = IdEntrada,
                IdInventario = IdInventario,
                IDPBM = IDPBM,
                IdTipoEntrada = IdTipoEntrada,
                IPBM = IPBM,
                Marca = Marca,
                Modelo = Modelo,
                NomeEstabelecimento = NomeEstabelecimento,
                NomeSetor = NomeSetor,
                NumeroControleEmpresa = NumeroControleEmpresa,
                Quantidade = Quantidade,
                SemIdentificacao = SemIdentificacao,
                Serial = Serial,
                SituacaoFisica = SituacaoFisica,
                TipoIBT = TipoIBT,
                Tombo = Tombo,
                Tombo2 = Tombo2,
                Material = new MaterialModel()
                {
                    Codigo = Material.Codigo,
                    Identificador = Material.Identificador,
                    CodigoClasse = Material.CodigoClasse,
                    NomeClasse = Material.NomeClasse,
                    NomeMaterial = Material.NomeMaterial,
                    Excluido = Material.Excluido
                }
            };
        }
    }
}
