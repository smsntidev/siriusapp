﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sirius.App
{
    public class ItemDescricao
    {
        public int ItemId { get; set; }
        public string Identificador { get; set; }
        public int IdentificadorTipoId { get; set; }
        public Item Item { get; set; }
        public string Serial { get; set; }
        public string NomeMaterial { get; set; }
        public string SituacaoFisica { get; set; }
        public string Quantidade { get; set; }
    }
}
