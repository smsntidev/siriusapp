﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sirius.App
{
    public class Estabelecimento
    {
        public Estabelecimento()
        {

        }

        public Estabelecimento(int codigo, string nomeEstabelecimento, string tipo, bool excluido)
        {
            Codigo = codigo;            
            NomeEstabelecimento = nomeEstabelecimento;
            Tipo = tipo;
            Excluido = excluido;
        }

        public int Codigo { get; set; }

        public string NomeEstabelecimento { get; set; }

        public string Tipo { get; set; }

        public bool Excluido { get; set; }

        public override string ToString() => NomeEstabelecimento;

        

    }
}
