﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace Sirius.App
{
    public class TipoInventario
    {
        public TipoInventario()
        {

        }

        public TipoInventario(string codigo, string nome)
        {
            Codigo = codigo;
            Nome = nome;
        }

        public string Codigo;
        public string Nome;


        public const string CoordenacaoCodigo = "C";
        public const string SubcoordenacaoCodigo = "S";
        public const string DistritoSanitarioCodigo = "D";
        public const string UnidadeDeSaudeCodigo = "U";

        public const string Coordenacao = "Coordenação";
        public const string Subcoordenacao = "Subcoordenação";
        public const string DistritoSanitario = "Distrito Sanitário";
        public const string UnidadeDeSaude = "Unidade de Saúde";

        public override string ToString() => Nome;

    }
}
