﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sirius.App
{
    public class Inventario
    {
        public Inventario()
        {
            Itens = new List<Item>();
        }

        public int Id { get; set; }

        public DateTime DataAbertura { get; set; }

        public int IdSetor { get; set; }

        public Setor Setor { get; set; }

        public Estabelecimento Estabelecimento { get; set; }

        public int IdResponsavelAtesto { get; set; }

        public Responsavel ResponsavelAtesto { get; set; }

        public DateTime? DataFechamento { get; set; }

        public bool Fechado { get; set; }

        public string Observacao { get; set; }

        public string MostraFechado { get; set; }

        public List<Item> Itens { get; set; }

        public List<Item> ItensExcluidos { get; set; }

        public HashSet<Item> ItensNovos { get; set; }

        public string Status
        {
            get
            {
                return Fechado ? "Fechado" : "Salvo";
            }

        }

    }
}
