﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sirius.App
{
    public class Setor
    {
        public Setor()
        {

        }
        public Setor(int codigo, int codigoEstabelecimento, string nome, bool excluido)
        {
            Codigo = codigo;
            CodigoEstabelecimento = codigoEstabelecimento;
            Nome = nome;
            Excluido = excluido;
        }

        public int Codigo { get; set; }

        public int CodigoEstabelecimento { get; set; }

        public string Nome { get; set; }

        public bool Excluido { get; set; }

        public override string ToString()
        {
            return Nome;
        }
    }
}
