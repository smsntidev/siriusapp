﻿using Sirius.App.Server;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sirius.App
{
    public partial class Business
    {
        public async Task<List<Inventario>> GetInventarios(int? idSetor, int? idEstabelecimento, string observacao)
        {
            try
            {
                var resposta = await Access.Current.Post<List<InventarioModel>, InventarioHeaderModel>
                    ("Inventario/Get", new InventarioHeaderModel()
                    {
                        IdSetor = idSetor,
                        IdEstabelecimento = idEstabelecimento,
                        Observacao = observacao
                    });

                return resposta.Select(x => new Inventario()
                {
                    Observacao = x.Observacao
                }).ToList();
            }
            catch (BusinessException)
            {
                throw;
            }
            catch (Exception)
            {
                throw new BusinessException("Servidor Offline.");
            }
        }

        public async Task<List<Responsavel>> GetResponsavelAtestoPorEstabelecimento(int codigoEstabelecimento)
        {
            try
            {
                var resposta = await Access.Current.CachedGet<List<ResponsavelAtestoModel>>
               ("Dominio/GetResponsaveis/" + codigoEstabelecimento);

                return resposta.Select(x => new Responsavel()
                {
                    Codigo = x.Codigo,
                    Nome = x.Nome,
                    CodigoEstabelecimento = x.CodigoEstabelecimento,
                    Excluido = x.Excluido
                }).ToList();

            }
            catch (BusinessException)
            {
                throw;
            }
            catch (Exception)
            {
                throw new BusinessException("Servidor Offline.");
            }
        }

        public async Task<List<Setor>> GetSetorPorEstabelecimento(int codigoEstabelecimento)
        {
            try
            {
                var resposta = await Access.Current.CachedGet<List<SetorModel>>
               ("Dominio/GetSetores/" + codigoEstabelecimento);

                return resposta.Select(x => new Setor(x.Codigo, x.CodigoEstabelecimento, x.Nome, x.Excluido)).ToList();

            }
            catch (BusinessException)
            {
                throw;
            }
            catch (Exception)
            {
                throw new BusinessException("Servidor Offline.");
            }
        }

        public async Task<List<Estabelecimento>> GetEstabelecimentosPorTipo(string tipo)
        {
            try
            {
                var resposta = await Access.Current.CachedGet<List<EstabelecimentoModel>>
               ("Dominio/GetEstabelecimentos/" + tipo);

                return resposta.Select(x => new Estabelecimento()
                {
                    NomeEstabelecimento = x.NomeEstabelecimento,
                    Codigo = x.Codigo,
                    Tipo = x.Tipo,
                    Excluido = x.Excluido
                }).ToList();

            }
            catch (BusinessException ex)
            {
                throw ex;
            }
            catch (Exception)
            {
                throw new BusinessException("Servidor Offline.");
            }
        }

        public async Task<Item> GetItemPorMaterial(string identificador, int tipoIdentificacao)
        {
            try
            {
                var resposta = await Access.Current.CachedPost<ItemModel, MaterialFiltroModel>
               ("Dominio/GetItemPorMaterial/", new MaterialFiltroModel()
               {
                   Tombo = tipoIdentificacao == 0 ? identificador.ToString() : null,
                   IBT = tipoIdentificacao == 1 ? long.Parse(identificador.ToString()) : 0,
                   IPBM = tipoIdentificacao == 2 ? long.Parse(identificador.ToString()) : 0,
                   TipoIdentificacao = tipoIdentificacao
               });

                return new Item()
                {
                    Codigo = resposta.Codigo,
                    CodigoEstabelecimento = resposta.CodigoEstabelecimento,
                    CodigoMarca = resposta.CodigoMarca,
                    CodigoMaterial = resposta.CodigoMaterial,
                    CodigoSetor = resposta.CodigoSetor,
                    CodigoSituacaoFisica = resposta.CodigoSituacaoFisica,
                    DescricaoMaterialInformatica = resposta.DescricaoMaterialInformatica,
                    IBT = resposta.IBT,
                    IdEntrada = resposta.IdEntrada,
                    IdInventario = resposta.IdInventario,
                    IDPBM = resposta.IDPBM,
                    IdTipoEntrada = resposta.IdTipoEntrada,
                    IPBM = resposta.IPBM,
                    Marca = resposta.Marca,
                    Modelo = resposta.Modelo,
                    NomeEstabelecimento = resposta.NomeEstabelecimento,
                    NomeSetor = resposta.NomeSetor,
                    NumeroControleEmpresa = resposta.NumeroControleEmpresa,
                    Quantidade = resposta.Quantidade,
                    SemIdentificacao = resposta.SemIdentificacao,
                    Serial = string.IsNullOrWhiteSpace(resposta.Serial) ? "Sem Serial" : resposta.Serial,
                    SituacaoFisica = resposta.SituacaoFisica,
                    TipoIBT = resposta.TipoIBT,
                    Tombo = resposta.Tombo,
                    Tombo2 = resposta.Tombo2,
                    Material = new Material(resposta.Material.Codigo, resposta.Material.Identificador, resposta.Material.CodigoClasse, resposta.Material.NomeClasse, resposta.Material.NomeMaterial, resposta.Material.Excluido)
                };
            }
            catch (BusinessException ex)
            {
                throw ex;
            }
            catch (Exception e)
            {
                throw new BusinessException(e.Message);
            }
        }

        //public Task<Item> GetItemPorCodigoBarras(string identificacao)
        //{
        //    if (identificacao.Length == 8)
        //    {
        //        string numPatrimonial = identificacao.Substring(2, 6);
        //        return GetItemPorMaterial(numPatrimonial, 0);
        //    }
        //    else
        //    {
        //        if (identificacao[0] == '4') //IPBM
        //            return GetItemPorMaterial(identificacao, 2);
        //        else //IBT
        //            return GetItemPorMaterial(identificacao, 1);
        //    }
        //}

        //TODO: Perguntar a Davi depois
      
public async Task<List<ItemDescricao>> GetMateriais(string nome, string codigo)
        {
            try
            {
                var resposta = await Access.Current.CachedPost<List<ItemModel>, ItemMaterialModel>
               ("Dominio/GetItensByCodigoOuNome", new ItemMaterialModel()
               {
                   NomeMaterial = nome,
                   Codigo = codigo
               });

                var items = resposta.Select(x => new Item()
                {
                    Codigo = x.Codigo,
                    CodigoEstabelecimento = x.CodigoEstabelecimento,
                    CodigoMarca = x.CodigoMarca,
                    CodigoMaterial = x.CodigoMaterial,
                    CodigoSetor = x.CodigoSetor,
                    CodigoSituacaoFisica = x.CodigoSituacaoFisica,
                    DescricaoMaterialInformatica = x.DescricaoMaterialInformatica,
                    IBT = x.IBT,
                    IdEntrada = x.IdEntrada,
                    IdInventario = x.IdInventario,
                    IDPBM = x.IDPBM,
                    IdTipoEntrada = x.IdTipoEntrada,
                    IPBM = x.IPBM,
                    Marca = x.Marca,
                    Modelo = x.Modelo,
                    NomeEstabelecimento = x.NomeEstabelecimento,
                    NomeSetor = x.NomeSetor,
                    NumeroControleEmpresa = x.NumeroControleEmpresa,
                    Quantidade = x.Quantidade,
                    SemIdentificacao = x.SemIdentificacao,
                    Serial = x.Serial,
                    SituacaoFisica = x.SituacaoFisica,
                    TipoIBT = x.TipoIBT,
                    Tombo = x.Tombo,
                    Tombo2 = x.Tombo2,
                    Material = new Material(x.Material.Codigo, x.Material.Identificador, x.Material.CodigoClasse, x.Material.NomeClasse, x.Material.NomeMaterial, x.Material.Excluido)
                }).ToList();

                List<ItemDescricao> respostaFinal = new List<ItemDescricao>();
                foreach (var item in items)
                {
                    var desc = new ItemDescricao()
                    {
                        ItemId = item.Codigo
                    };
                    var idTipoMaterial = item.IBT > 0 ? 1 : (item.IDPBM > 0 ? 2 : 0);
                    desc.IdentificadorTipoId = idTipoMaterial;
                    switch (idTipoMaterial)
                    {
                        case 0:
                            desc.Identificador = item.Tombo2;
                            break;
                        case 1:
                            desc.Identificador = item.IBT.ToString();
                            break;
                        case 2:
                            desc.Identificador = item.IDPBM.ToString();
                            break;
                        default:
                            break;
                    }
                    desc.Item = item;
                    desc.NomeMaterial = item.Material?.NomeMaterial ?? "Sem nome";
                    desc.Quantidade = item.Quantidade.ToString();
                    desc.Serial = item.Serial ?? "Sem Serial";
                    desc.SituacaoFisica = item.SituacaoFisica ?? "Sem Situação Física";
                    respostaFinal.Add(desc);
                }
                return respostaFinal;
            }
            catch (BusinessException ex)
            {
                throw ex;
            }
            catch (Exception)
            {
                throw new BusinessException("Servidor Offline.");
            }
        }

        public async Task<Inventario> GetInventarioPorId(int inventarioId)
        {
            try
            {
                var resposta = await Access.Current.Get<InventarioModel>
                    ($"Inventario/Get/{inventarioId}");

                return new Inventario()
                {
                    Id = resposta.Id,
                    Setor = new Setor(resposta.SetorModel.Codigo, resposta.SetorModel.CodigoEstabelecimento, resposta.SetorModel.Nome, resposta.SetorModel.Excluido),
                    IdSetor = resposta.IdSetor,
                    Estabelecimento = new Estabelecimento(resposta.EstabelecimentoModel.Codigo, resposta.EstabelecimentoModel.NomeEstabelecimento, resposta.EstabelecimentoModel.Tipo, resposta.EstabelecimentoModel.Excluido),
                    ResponsavelAtesto = new Responsavel(resposta.ResponsavelAtesto.Codigo, resposta.ResponsavelAtesto.CodigoEstabelecimento, resposta.ResponsavelAtesto.Nome, resposta.ResponsavelAtesto.Excluido),
                    IdResponsavelAtesto = resposta.IdResponsavelAtesto,
                    Observacao = resposta.Observacao,
                    MostraFechado = resposta.MostraFechado,
                    Fechado = resposta.Fechado,
                    Itens = resposta.Itens.Select(im => new Item()
                    {
                        Codigo = im.Codigo,
                        CodigoEstabelecimento = im.CodigoEstabelecimento,
                        CodigoMarca = im.CodigoMarca,
                        CodigoMaterial = im.CodigoMaterial,
                        CodigoSetor = im.CodigoSetor,
                        CodigoSituacaoFisica = im.CodigoSituacaoFisica,
                        DescricaoMaterialInformatica = im.DescricaoMaterialInformatica,
                        IBT = im.IBT,
                        IdEntrada = im.IdEntrada,
                        IdInventario = im.IdInventario,
                        IDPBM = im.IDPBM,
                        IdTipoEntrada = im.IdTipoEntrada,
                        IPBM = im.IPBM,
                        Marca = im.Marca,
                        Modelo = im.Modelo,
                        NomeEstabelecimento = im.NomeEstabelecimento,
                        NomeSetor = im.NomeSetor,
                        NumeroControleEmpresa = im.NumeroControleEmpresa ?? string.Empty,
                        Quantidade = im.Quantidade,
                        SemIdentificacao = im.SemIdentificacao,
                        Serial = im.Serial ?? string.Empty,
                        SituacaoFisica = im.SituacaoFisica,
                        TipoIBT = im.TipoIBT,
                        Tombo = im.Tombo,
                        Tombo2 = im.Tombo2 ?? string.Empty,
                        Material = new Material(im.Material.Codigo, im.Material.Identificador, im.Material.CodigoClasse, im.Material.NomeClasse, im.Material.NomeMaterial, im.Material.Excluido)
                    }).ToList()
                };
            }
            catch (BusinessException)
            {
                throw;
            }
            catch (Exception)
            {
                throw new BusinessException("Servidor Offline.");
            }
        }

        public async Task<List<Inventario>> GetListaInventarios()
        {
            try
            {
                var resposta = await Access.Current.CachedPost<List<InventarioModel>, InventarioHeaderModel>
                    ("Inventario/Get", new InventarioHeaderModel()
                    {
                        //IdSetor = idSetor,
                        //IdEstabelecimento = idEstabelecimento,
                        //Observacao = observacao
                        
                    });
                
                return resposta.Select(x => new Inventario()
                {
                    Id = x.Id,
                    Setor = new Setor(x.SetorModel.Codigo, x.SetorModel.CodigoEstabelecimento, x.SetorModel.Nome, x.SetorModel.Excluido),
                    IdSetor = x.IdSetor,
                    Estabelecimento = new Estabelecimento(x.EstabelecimentoModel.Codigo, x.EstabelecimentoModel.NomeEstabelecimento, x.EstabelecimentoModel.Tipo, x.EstabelecimentoModel.Excluido),
                    ResponsavelAtesto = new Responsavel(x.ResponsavelAtesto.Codigo, x.ResponsavelAtesto.CodigoEstabelecimento, x.ResponsavelAtesto.Nome, x.ResponsavelAtesto.Excluido),
                    IdResponsavelAtesto = x.IdResponsavelAtesto,
                    Observacao = x.Observacao,
                    MostraFechado = x.MostraFechado,
                    Fechado = x.Fechado
                }).ToList();
            }
            catch (BusinessException)
            {
                throw;
            }
            catch (Exception)
            {
                throw new BusinessException("Servidor Offline.");
            }
        }
        public async Task RegistrarInventario(RegistroInventario inventario)
        {
            try
            {
                var resposta = await Access.Current.Post<String, RegistroInventarioModel>
               ("Inventario/RegistrarInventario/", inventario.CreateServerModel());

            }
            catch (BusinessException ex)
            {
                throw ex;
            }
            catch (Exception ex)
            {
                throw new BusinessException("Servidor Offline.");
            }
        }
    }
}
