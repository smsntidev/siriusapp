﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sirius.App
{
    public class Material
    {
        public Material()
        {

        }

        public Material(int codigo, long identificador, int codigoClasse, string nomeClasse, string nomeMaterial, bool excluido)
        {
            Codigo = codigo;
            Identificador = identificador;
            CodigoClasse = codigoClasse;
            NomeClasse = nomeClasse;
            NomeMaterial = nomeMaterial;
            Excluido = excluido;
        }

        public int Codigo { get; set; }

        public long Identificador { get; set; }

        public int CodigoClasse { get; set; }

        public string NomeClasse { get; set; }

        public string NomeMaterial { get; set; }

        public bool Excluido { get; set; }

        public override string ToString() => NomeMaterial;
    }
}
