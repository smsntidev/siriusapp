﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sirius.App
{
    public class TipoBuscaMaterial
    {
        public TipoBuscaMaterial()
        {

        }
        public TipoBuscaMaterial(int codigo, string nome)
        {
            Codigo = codigo;
            Nome = nome;
        }

        public string Nome { get; set; }
        public int Codigo { get; set; }

        public const int NumeroPatrimonialCodigo = 0;
        public const int IBTCodigo = 1;
        public const int IPBMCodigo = 2;

        public const string NumeroPatrimonial = "Nº Patrimonial";
        public const string IBT = "IBT";
        public const string IPBM = "IPBM";

        public override string ToString() => Nome;
    }
}
