﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sirius.App
{
    public class OrdemServicoFiltro
    {
        public int? AreaResponsavelId { get; set; }
        public int? ProblemaId { get; set; }
        public int? ItemId { get; set; }
        public int? EstabelecimentoSaudeId { get; set; }
        public int? SetorId { get; set; }
        public string Contato { get; set; }
        public int? StatusId { get; set; }
    }
}
