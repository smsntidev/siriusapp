﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sirius.App
{
    public class Problema
    {
        public int Codigo { get; set; }

        public string Nome { get; set; }

        public bool Excluido { get; set; }

        public override string ToString()
        {
            return Nome;
        }
    }
}
