﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sirius.App
{
    public class OrdemServico
    {
        //No servidor é o código
        public int? IdOrdemServico { get; set; }
        public string IdOrdemServicoFormatado => $"Ordem de Serviço: {IdOrdemServico?.ToString() ?? string.Empty}";

        public int IdAreaResponsavel { get; set; }
        public string AreaResponsavel { get; set; }

        public int IdProblema { get; set; }
        public string Problema { get; set; }

        public string Contato { get; set; }
        public string Telefone { get; set; }
        public string DescricaoProblema { get; set; }

        public int IdStatus { get; set; }
        public string Status { get; set; }

        public string DataOcorrencia { get; set; }

        public int IdItem { get; set; }
        public string DescricaoMaterial { get; set; }

        public string CodigoItem { get; set; }
        public int IdTipoMaterial { get; set; }

        public int SetorId { get; set; }
        public string Setor { get; set; }

        public int EstabelecimentoSaudeId { get; set; }
        public string EstabelecimentoSaude { get; set; }
        public string EstabelecimentoSaudeFormatado => $"Estabelecimento: {EstabelecimentoSaude ?? string.Empty}";

        public int? IdOrdemServicoSuperior { get; set; }

        public Func<System.IO.Stream> ArquivoAnexo { get; set; }
        public int? ArquivoOcorrenciaId { get; set; }
        public string ArquivoDescricao { get; set; }
        public string DataArquivoAnexo { get; set; }
    }
}
