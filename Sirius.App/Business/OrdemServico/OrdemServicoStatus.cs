﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sirius.App
{
    public class OrdemServicoStatus
    {
        public int Codigo { get; set; }

        public string Nome { get; set; }

        public override string ToString()
        {
            return Nome;
        }

        public OrdemServicoStatus(int codigo, string nome)
        {
            Codigo = codigo;
            Nome = nome;
        }
        public OrdemServicoStatus()
        {

        }
    }
}
