﻿using Sirius.App.Server;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace Sirius.App
{
    public partial class Business
    {
        private static readonly IImportFile ImportFile = DependencyService.Get<IImportFile>();

        public async Task<List<AreaResponsavel>> GetAreasResponsaveis()
        {
            try
            {
                return (await Access.Current.CachedGet<List<AreaResponsavelModel>>("Dominio/GetAreasResponsaveis"))
                    .Select(x => new AreaResponsavel()
                    {
                        Codigo = x.Codigo,
                        Excluido = x.Excluido,
                        Nome = x.Nome
                    }).Where(x => !x.Excluido).ToList();
            }
            catch (BusinessException)
            {
                throw;
            }
            catch (Exception)
            {
                throw new BusinessException("Servidor Offline.");
            }
        }

        public async Task<List<Problema>> GetProblemas()
        {
            try
            {
                return (await Access.Current.CachedGet<List<ProblemaModel>>("Dominio/GetProblemas"))
                    .Select(x => new Problema()
                    {
                        Codigo = x.Codigo,
                        Excluido = x.Excluido,
                        Nome = x.Nome
                    }).Where(x => !x.Excluido).ToList();
            }
            catch (BusinessException)
            {
                throw;
            }
            catch (Exception)
            {
                throw new BusinessException("Servidor Offline.");
            }
        }

        public Task<List<TipoBuscaMaterial>> GetTipoMateriais()
        {
            return Task.FromResult(new List<TipoBuscaMaterial>()
            {
                new TipoBuscaMaterial(TipoBuscaMaterial.NumeroPatrimonialCodigo, TipoBuscaMaterial.NumeroPatrimonial),
                new TipoBuscaMaterial(TipoBuscaMaterial.IBTCodigo, TipoBuscaMaterial.IBT),
                new TipoBuscaMaterial(TipoBuscaMaterial.IPBMCodigo, TipoBuscaMaterial.IPBM),
            });
        }

        //BusinessInventario.GetItemPorMaterial (busca materiais)

        public async Task<List<Estabelecimento>> GetTodosEstabelecimentos()
        {
            try
            {
                //Mudar para pegar todos os estabelecimentos
                var resposta = await Access.Current.CachedGet<List<EstabelecimentoModel>>("Dominio/GetEstabelecimentos");

                return resposta.Select(x => new Estabelecimento()
                {
                    NomeEstabelecimento = x.NomeEstabelecimento,
                    Codigo = x.Codigo,
                    Excluido = x.Excluido
                }).Where(x => !x.Excluido).ToList();

            }
            catch (BusinessException ex)
            {
                throw ex;
            }
            catch (Exception)
            {
                throw new BusinessException("Servidor Offline.");
            }
        }

        //BusinessInventario.GetSetorPorEstabelecimento

        public Task<List<OrdemServicoStatus>> GetOrdemServicoStatuses()
        {
            return Task.FromResult(new List<OrdemServicoStatus>()
            {
                new OrdemServicoStatus(1, "Aguardando atendimento"),
                new OrdemServicoStatus(2, "Encerrado"),
                new OrdemServicoStatus(3, "Cancelado"),
            });
        }

        public Task<ImportedFile> CapturarArquivo()
        {
            try
            {
                return ImportFile.OpenFile();
            }
            catch (OperationCanceledException)
            {
                throw;
            }
            catch (BusinessException)
            {
                throw;
            }
            catch (Exception)
            {
                throw new BusinessException("Servidor Offline.");
            }
        }

        public async Task<List<OrdemServico>> PesquisarOrdemServico(OrdemServicoFiltro filtro)
        {
            try
            {
                //Mudar para pegar todos os estabelecimentos
                var resposta = await Access.Current.Post<List<ChamadoModel>, BuscarChamadoModel>("Chamado/BuscarChamados", new BuscarChamadoModel()
                {
                    CodigoAreaResponsavel = filtro.AreaResponsavelId ?? 0,
                    CodigoEstabelecimento = filtro.EstabelecimentoSaudeId ?? 0,
                    CodigoItem = filtro.ItemId ?? 0,
                    CodigoProblema = filtro.ProblemaId ?? 0,
                    CodigoSetor = filtro.SetorId ?? 0,
                    CodigoStatusOcorrencia = filtro.StatusId ?? 0,
                    //DAVID: Perguntar se devo passar string vazia
                    Contato = filtro.Contato ?? string.Empty
                });

                var ordem = resposta.Select(x => new OrdemServico()
                {
                    ArquivoOcorrenciaId = x.CodigoArquivoOcorrencia > 0 ? x.CodigoAreaResponsavel : (int?)null,
                    AreaResponsavel = x.AreaResponsavel,
                    ArquivoAnexo = null,
                    //DAVID: Falar depois
                    ArquivoDescricao = "Arquivo Anexado",
                    //DAVID: Falar depois
                    DataArquivoAnexo = x.DataOcorrencia.ToString("d"),
                    IdAreaResponsavel = x.CodigoAreaResponsavel,
                    CodigoItem = null,
                    Contato = x.Contato,
                    DataOcorrencia = x.DataOcorrencia.ToString("d"),
                    DescricaoMaterial = null,
                    DescricaoProblema = x.Descricao,
                    EstabelecimentoSaude = x.Estabelecimento,
                    EstabelecimentoSaudeId = x.CodigoEstabelecimento,
                    IdItem = x.CodigoItem,
                    IdOrdemServico = x.Codigo,
                    IdOrdemServicoSuperior = x.CodigoOcorrenciaSuperior,
                    IdProblema = x.CodigoProblema,
                    IdStatus = x.CodigoStatusOcorrencia,
                    IdTipoMaterial = 0,
                    Problema = x.Problema,
                    Setor = x.Setor,
                    SetorId = x.CodigoSetor,
                    Status = x.StatusOcorrencia,
                    Telefone = x.Telefone.ToString()
                }).ToList();

                return ordem;

            }
            catch (BusinessException ex)
            {
                throw ex;
            }
            catch (Exception)
            {
                throw new BusinessException("Servidor Offline.");
            }
        }

        public async Task<ItemModel> GetItemPorId(int itemId)
        {
            try
            {
                return await Access.Current.Get<ItemModel>($"Dominio/GetItem/{itemId}");
            }
            catch (BusinessException ex)
            {
                throw ex;
            }
            catch (Exception)
            {
                throw new BusinessException("Servidor Offline.");
            }
        }

        public async Task<OrdemServico> GetOrdemServicoPorId(int ordemServicoId)
        {
            try
            {
                //Mudar para pegar todos os estabelecimentos
                var resposta = await Access.Current.Get<ChamadoModel>($"Chamado/GetChamadoPorCodigo/{ordemServicoId}");

                var ordem = new OrdemServico()
                {
                    ArquivoOcorrenciaId = resposta.CodigoArquivoOcorrencia > 0 ? resposta.CodigoArquivoOcorrencia : (int?)null,
                    AreaResponsavel = resposta.AreaResponsavel,
                    ArquivoAnexo = null,
                    //DAVID: Falar depois
                    ArquivoDescricao = "Arquivo Anexado",
                    //DAVID: Falar depois
                    DataArquivoAnexo = resposta.DataOcorrencia.ToString("d"),
                    IdAreaResponsavel = resposta.CodigoAreaResponsavel,
                    CodigoItem = null,
                    Contato = resposta.Contato,
                    DataOcorrencia = resposta.DataOcorrencia.ToString("d"),
                    DescricaoMaterial = null,
                    DescricaoProblema = resposta.Descricao,
                    EstabelecimentoSaude = resposta.Estabelecimento,
                    EstabelecimentoSaudeId = resposta.CodigoEstabelecimento,
                    IdItem = resposta.CodigoItem,
                    IdOrdemServico = resposta.Codigo,
                    IdOrdemServicoSuperior = resposta.CodigoOcorrenciaSuperior,
                    IdProblema = resposta.CodigoProblema,
                    IdStatus = resposta.CodigoStatusOcorrencia,
                    IdTipoMaterial = 0,
                    Problema = resposta.Problema,
                    Setor = resposta.Setor,
                    SetorId = resposta.CodigoSetor,
                    Status = resposta.StatusOcorrencia,
                    Telefone = resposta.Telefone.ToString(),
                };

                var item = await Access.Current.Get<ItemModel>($"Dominio/GetItem/{ordem.IdItem}");
                if(item != null)
                {
                    ordem.IdTipoMaterial = item.IBT > 0 ? 1 : (item.IDPBM > 0 ? 2 : 0);
                    switch (ordem.IdTipoMaterial)
                    {
                        case 0:
                            ordem.CodigoItem = item.Tombo2;
                            break;
                        case 1:
                            ordem.CodigoItem = item.IBT.ToString();
                            break;
                        case 2:
                            ordem.CodigoItem = item.IDPBM.ToString();
                            break;
                        default:
                            break;
                    }
                    //DAVID: Está correto pegar o nome do material?
                    ordem.DescricaoMaterial = item.Material.NomeMaterial;
                }

                return ordem;
            }
            catch (BusinessException ex)
            {
                throw ex;
            }
            catch (Exception)
            {
                throw new BusinessException("Servidor Offline.");
            }
        }

        public async Task Salvar(OrdemServico ordemServico)
        {
            try
            {
                ordemServico.IdOrdemServico = await Access.Current.Post<int, ChamadoModel>("Chamado/Salvar", new ChamadoModel()
                {
                    Codigo = ordemServico.IdOrdemServico ?? 0,
                    //DAVID: Mudar para Base64
                    ArquivoOcorrencia = !ordemServico.ArquivoOcorrenciaId.HasValue ? ordemServico.ArquivoAnexo?.Invoke().ReadToEnd() : null,
                    CodigoAreaResponsavel = ordemServico.IdAreaResponsavel,
                    CodigoArquivoOcorrencia = ordemServico.ArquivoOcorrenciaId ?? 0,
                    CodigoEstabelecimento = ordemServico.EstabelecimentoSaudeId,
                    CodigoItem = ordemServico.IdItem,
                    CodigoOcorrenciaSuperior = ordemServico.IdOrdemServicoSuperior ?? 0,
                    CodigoSetor = ordemServico.SetorId,
                    CodigoProblema = ordemServico.IdProblema,
                    CodigoStatusOcorrencia = ordemServico.IdStatus,
                    Contato = ordemServico.Contato,
                    DataOcorrencia = DateTime.Now,
                    Descricao = ordemServico.DescricaoProblema,
                    SubChamado = ordemServico.IdOrdemServicoSuperior.HasValue,
                    Telefone = ordemServico.Telefone.ApenasNumeros().TryToLong() ?? 0
                });
            }
            catch (BusinessException ex)
            {
                throw ex;
            }
            //catch (Exception)
            //{
            //    throw new BusinessException("Servidor Offline.");
            //}
            catch (Exception e)
            {
                throw new BusinessException(e.Message);
            }
        }

    }
}
