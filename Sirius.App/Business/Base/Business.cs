﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace Sirius.App
{
    public partial class Business
    {
        private static Business _Business = new Business();
        public static Business Current => _Business;
        private Business() { }

        private static T Get<T>(string nome) where T : class
        {
            try
            {
                if (!Application.Current.Properties.ContainsKey(nome))
                    return null;

                var resultado = Application.Current.Properties[nome] as string;
                if (string.IsNullOrWhiteSpace(resultado))
                    return null;

                try
                {
                    return JsonConvert.DeserializeObject<T>(resultado);
                }
                catch
                {
                    if (typeof(T) == typeof(string))
                        return (T)(object)resultado;
                    else
                        throw;
                }
            }
            catch
            {
                return null;
            }
        }
        private static void Set<T>(string nome, T valor) where T : class
        {
            try
            {
                Application.Current.Properties[nome] = JsonConvert.SerializeObject(valor);
            }
            catch { }
        }
        private static void Remove(string nome)
        {
            try
            {
                if (Application.Current.Properties.ContainsKey(nome))
                    Application.Current.Properties.Remove(nome);
            }
            catch { }
        }
        private static async Task Save()
        {
            try
            {
                await Application.Current.SavePropertiesAsync();
            }
            catch
            {

            }
        }
    }
}
