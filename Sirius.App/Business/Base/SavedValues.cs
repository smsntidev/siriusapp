﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sirius.App
{
    public static class SavedValues
    {
        public const string Username = "Access.Username";
        public const string Password = "Access.Password";
        public const string UserData = "Access.UserData";
    }
}
