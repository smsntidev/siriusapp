﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Sirius.App
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class AdicionarMaterialInventarioPage : VContentPage
	{
		public AdicionarMaterialInventarioPage(Inventario inventario = null, Action atualizar = null)
		{
			InitializeComponent ();
            Controller = new AdicionarMaterialInventarioController(this, inventario, atualizar);
        }
	}
}