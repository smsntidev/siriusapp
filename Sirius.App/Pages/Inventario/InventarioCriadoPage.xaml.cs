﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Sirius.App
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class InventarioCriadoPage : VContentPage
    {
		public InventarioCriadoPage ()
		{
			InitializeComponent ();
            Controller = new InventarioCriadoController(this);
        }
	}
}