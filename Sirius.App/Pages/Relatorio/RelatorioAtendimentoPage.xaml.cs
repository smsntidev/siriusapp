﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Sirius.App
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class RelatorioAtendimentoPage : VContentPage
    {
        public RelatorioAtendimentoPage()
        {
            InitializeComponent();
            btnPesquisar.Clicked += BtnPesquisar_Clicked;
            IsLoading = true;

            pckTipo.SelectedIndexChanged += PckTipo_SelectedIndexChanged;
            pckEstabelecimento.SelectedIndexChanged += PckEstabelecimento_SelectedIndexChanged;
        }

        private bool Estabelecimento_Protect = false;
        private async void PckEstabelecimento_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (IsLoading)
                return;
            if (!Estabelecimento_Protect)
            {
                aiSetor.IsVisible = true;
                pckSetor.IsVisible = false;

                try
                {
                    var estabelecimento = pckEstabelecimento.SelectedItem as Estabelecimento;
                    var setores = await Business.Current.GetSetorPorEstabelecimento(estabelecimento.Codigo);
                    pckSetor.ItemsSource = setores;
                    pckSetor.SelectedItem = setores.FirstOrDefault();
                }
                catch (Exception exception)
                {
                    await DisplayAlert("Erro", exception.Message, "Ok");
                }
                finally
                {
                    aiSetor.IsVisible = false;
                    pckSetor.IsVisible = true;
                }
            }
        }

        private async void PckTipo_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (IsLoading)
                return;
            Estabelecimento_Protect = true;

            aiEstabelecimento.IsVisible = true;
            pckEstabelecimento.IsVisible = false;

            aiSetor.IsVisible = true;
            pckSetor.IsVisible = false;

            try
            {
                var tipo = pckTipo.SelectedItem as TipoEstabelecimento;
                var estabelecimentos = await Business.Current.GetEstabelecimentosPorTipo(tipo.Codigo);
                pckEstabelecimento.ItemsSource = estabelecimentos;
                pckEstabelecimento.SelectedItem = estabelecimentos.First();

                var setores = await Business.Current.GetSetorPorEstabelecimento(estabelecimentos.First().Codigo);
                pckSetor.ItemsSource = setores;
                pckSetor.SelectedItem = setores.FirstOrDefault();
            }
            catch(Exception exception)
            {
                await DisplayAlert("Erro", exception.Message, "Ok");
            }
            finally
            {
                Estabelecimento_Protect = false;

                aiEstabelecimento.IsVisible = false;
                pckEstabelecimento.IsVisible = true;

                aiSetor.IsVisible = false;
                pckSetor.IsVisible = true;
            }
        }

        private bool OnAppearing_Guard = false;
        protected override async void OnAppearing()
        {
            if (!OnAppearing_Guard)
            {
                OnAppearing_Guard = true;
                IsLoading = true;
                try
                {
                    var tipos = new List<TipoEstabelecimento>()
                {
                    new TipoEstabelecimento(TipoEstabelecimento.CoordenacaoCodigo, TipoEstabelecimento.Coordenacao),
                    new TipoEstabelecimento(TipoEstabelecimento.SubcoordenacaoCodigo, TipoEstabelecimento.Subcoordenacao),
                    new TipoEstabelecimento(TipoEstabelecimento.DistritoSanitarioCodigo, TipoEstabelecimento.DistritoSanitario),
                    new TipoEstabelecimento(TipoEstabelecimento.UnidadeDeSaudeCodigo, TipoEstabelecimento.UnidadeDeSaude)
                };
                    pckTipo.ItemsSource = tipos;
                    pckTipo.SelectedItem = tipos.First();

                    var estabelecimentos = await Business.Current.GetEstabelecimentosPorTipo(tipos.First().Codigo);
                    pckEstabelecimento.ItemsSource = estabelecimentos;
                    pckEstabelecimento.SelectedItem = estabelecimentos.First();

                    var setores = await Business.Current.GetSetorPorEstabelecimento(estabelecimentos.First().Codigo);
                    pckSetor.ItemsSource = setores;
                    pckSetor.SelectedItem = setores.FirstOrDefault();
                }
                catch (Exception exception)
                {
                    await DisplayAlert("Erro", exception.Message, "Ok");
                }
                finally
                {
                    IsLoading = false;
                }
            }
        }

        private async void BtnPesquisar_Clicked(object sender, EventArgs e)
        {
            try
            {
                if (pckTipo.SelectedItem == null)
                    throw new Exception("O campo Tipo é de preenchimento obrigatório.");
                if (pckEstabelecimento.SelectedItem == null)
                    throw new Exception("O campo Estabelecimento é de preenchimento obrigatório.");
                if (pckSetor.SelectedItem == null)
                    throw new Exception("O campo Setor é de preenchimento obrigatório.");

                if (txtIdentificador.Text.IsNotEmpty()) {
                    if (!long.TryParse(txtIdentificador.Text, out var idMaterial))
                        throw new Exception("O campo Identificador não é um número válido.");
                }

                if (dtInicio.NullableDate == null)
                    throw new Exception("O campo Período (Início) é de preenchimento obrigatório.");
                if (dtFim.NullableDate == null)
                    throw new Exception("O campo Período (Fim) é de preenchimento obrigatório.");

                if (dtInicio.NullableDate >= dtFim.NullableDate)
                    throw new Exception("O campo Período (Início) precisa ser menor que o campo Período (Fim).");

                var estabelecimentoId = (pckEstabelecimento.SelectedItem as Estabelecimento).Codigo;
                var setorId = (pckSetor.SelectedItem as Setor).Codigo;

                var url = Proxy.AtendimentosUrl(
                    estabelecimentoId,
                    setorId,
                    txtIdentificador.Text.IsNotEmpty() ? long.Parse(txtIdentificador.Text) : (long?)null,
                    dtInicio.NullableDate.Value,
                    dtFim.NullableDate.Value);
                await Navigation.PushAsync(new RelatorioPage("ATENDIMENTO", url));
            }
            catch (Exception exception)
            {
                await DisplayAlert("Erro", exception.Message, "Ok");
            }
        }

        private bool IsValidNumber(string text)
        {
            return long.TryParse(text, out var result);
        }
    }
}