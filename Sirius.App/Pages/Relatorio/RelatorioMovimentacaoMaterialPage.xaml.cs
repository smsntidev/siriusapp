﻿using Sirius.App.Server;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Sirius.App
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class RelatorioMovimentacaoMaterialPage : VContentPage
    {
        public RelatorioMovimentacaoMaterialPage()
        {
            InitializeComponent();
            btnPesquisar.Clicked += BtnPesquisar_Clicked;
            IsLoading = true;
        }

        private bool OnAppearing_Guard = false;
        protected override async void OnAppearing()
        {
            if (!OnAppearing_Guard)
            {
                OnAppearing_Guard = true;
                IsLoading = true;
                try
                {
                    var tipos = await Business.Current.GetTiposMovimentacoes();
                    pckTipo.ItemsSource = tipos;
                    pckTipo.SelectedItem = tipos.First();

                    var naturezas = await Business.Current.GetNaturezas();
                    pckNatureza.ItemsSource = naturezas;
                    pckNatureza.SelectedItem = naturezas.First();
                }
                catch (Exception exception)
                {
                    await DisplayAlert("Erro", exception.Message, "Ok");
                }
                finally
                {
                    IsLoading = false;
                }
            }
        }

        private async void BtnPesquisar_Clicked(object sender, EventArgs e)
        {
            try
            {
                if (pckTipo.SelectedItem == null)
                    throw new Exception("O campo Tipo é de preenchimento obrigatório.");
                if (pckNatureza.SelectedItem == null)
                    throw new Exception("O campo Natureza é de preenchimento obrigatório.");

                if (dtInicio.NullableDate == null)
                    throw new Exception("O campo Período (Início) é de preenchimento obrigatório.");
                if (dtFim.NullableDate == null)
                    throw new Exception("O campo Período (Fim) é de preenchimento obrigatório.");

                if (dtInicio.NullableDate >= dtFim.NullableDate)
                    throw new Exception("O campo Período (Início) precisa ser menor que o campo Período (Fim).");

                var tipoId = (pckTipo.SelectedItem as TipoMovimentacao).Codigo;
                var naturezaId = (pckNatureza.SelectedItem as NaturezaModel).Codigo;

                var url = Proxy.MovimentacaoMaterialUrl(
                    tipoId,
                    naturezaId,
                    dtInicio.NullableDate.Value,
                    dtFim.NullableDate.Value);
                await Navigation.PushAsync(new RelatorioPage("MOVIMENTAÇÃO", url));
            }
            catch (Exception exception)
            {
                await DisplayAlert("Erro", exception.Message, "Ok");
            }
        }
    }
}