﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Sirius.App
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class RelatorioSolucoesPage : VContentPage
    {
        public RelatorioSolucoesPage()
        {
            InitializeComponent();
            btnPesquisar.Clicked += BtnPesquisar_Clicked;
            IsLoading = true;
        }

        private bool OnAppearing_Guard = false;
        protected override async void OnAppearing()
        {
            if (!OnAppearing_Guard)
            {
                OnAppearing_Guard = true;
                IsLoading = true;
                try
                {
                    var problemas = await Business.Current.GetProblemas();
                    pckProblema.ItemsSource = problemas;
                    pckProblema.SelectedItem = problemas.First();
                }
                catch (Exception exception)
                {
                    await DisplayAlert("Erro", exception.Message, "Ok");
                }
                finally
                {
                    IsLoading = false;
                }
            }
        }

        private async void BtnPesquisar_Clicked(object sender, EventArgs e)
        {
            try
            {
                if (pckProblema.SelectedItem == null)
                    throw new Exception("O campo Problema é de preenchimento obrigatório.");

                if (dtInicio.NullableDate == null)
                    throw new Exception("O campo Período (Início) é de preenchimento obrigatório.");
                if (dtFim.NullableDate == null)
                    throw new Exception("O campo Período (Fim) é de preenchimento obrigatório.");

                if (dtInicio.NullableDate >= dtFim.NullableDate)
                    throw new Exception("O campo Período (Início) precisa ser menor que o campo Período (Fim).");

                var problemaId = (pckProblema.SelectedItem as Problema).Codigo;

                var url = Proxy.SolucoesUrl(problemaId, dtInicio.NullableDate.Value, dtFim.NullableDate.Value);
                await Navigation.PushAsync(new RelatorioPage("SOLUÇÕES", url));
            }
            catch(Exception exception)
            {
                await DisplayAlert("Erro", exception.Message, "Ok");
            }
        }
    }
}