﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Sirius.App
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class RelatorioPage : VContentPage
    {
        public RelatorioPage()
        {
            InitializeComponent();
            GoBackCommand = new Command(async () => await Navigation.PopAsync());
        }

        private void WebView_Navigating(object sender, WebNavigatingEventArgs e)
        {
            aIndicator.IsVisible = true;
            webView.IsVisible = false;
        }

        private void WebView_Navigated(object sender, WebNavigatedEventArgs e)
        {
            aIndicator.IsVisible = false;
            webView.IsVisible = true;
        }

        public RelatorioPage(string title, string url) : this()
        {
            webView.Navigating += WebView_Navigating;
            webView.Navigated += WebView_Navigated;

            Title = title;
            webView.Source = url;
        }
    }
}