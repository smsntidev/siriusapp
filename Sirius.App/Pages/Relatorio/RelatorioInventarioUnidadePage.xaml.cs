﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Sirius.App
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class RelatorioInventarioUnidadePage : VContentPage
    {
        public RelatorioInventarioUnidadePage()
        {
            InitializeComponent();
            btnPesquisar.Clicked += BtnPesquisar_Clicked;
            IsLoading = true;

            pckTipo.SelectedIndexChanged += PckTipo_SelectedIndexChanged;
        }

        private async void PckTipo_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (IsLoading)
                return;

            aiEstabelecimento.IsVisible = true;
            pckEstabelecimento.IsVisible = false;

            try
            {
                var tipo = pckTipo.SelectedItem as TipoEstabelecimento;
                var estabelecimentos = await Business.Current.GetEstabelecimentosPorTipo(tipo.Codigo);
                pckEstabelecimento.ItemsSource = estabelecimentos;
                pckEstabelecimento.SelectedItem = estabelecimentos.First();
            }
            catch (Exception exception)
            {
                await DisplayAlert("Erro", exception.Message, "Ok");
            }
            finally
            {
                aiEstabelecimento.IsVisible = false;
                pckEstabelecimento.IsVisible = true;
            }
        }

        private bool OnAppearing_Guard = false;
        protected override async void OnAppearing()
        {
            if (!OnAppearing_Guard)
            {
                OnAppearing_Guard = true;
                IsLoading = true;
                try
                {
                    var tipos = new List<TipoEstabelecimento>()
                {
                    new TipoEstabelecimento(TipoEstabelecimento.CoordenacaoCodigo, TipoEstabelecimento.Coordenacao),
                    new TipoEstabelecimento(TipoEstabelecimento.SubcoordenacaoCodigo, TipoEstabelecimento.Subcoordenacao),
                    new TipoEstabelecimento(TipoEstabelecimento.DistritoSanitarioCodigo, TipoEstabelecimento.DistritoSanitario),
                    new TipoEstabelecimento(TipoEstabelecimento.UnidadeDeSaudeCodigo, TipoEstabelecimento.UnidadeDeSaude)
                };
                    pckTipo.ItemsSource = tipos;
                    pckTipo.SelectedItem = tipos.First();

                    var estabelecimentos = await Business.Current.GetEstabelecimentosPorTipo(tipos.First().Codigo);
                    pckEstabelecimento.ItemsSource = estabelecimentos;
                    pckEstabelecimento.SelectedItem = estabelecimentos.First();
                }
                catch (Exception exception)
                {
                    await DisplayAlert("Erro", exception.Message, "Ok");
                }
                finally
                {
                    IsLoading = false;
                }
            }
        }

        private async void BtnPesquisar_Clicked(object sender, EventArgs e)
        {
            try
            {
                if (pckTipo.SelectedItem == null)
                    throw new Exception("O campo Tipo é de preenchimento obrigatório.");
                if (pckEstabelecimento.SelectedItem == null)
                    throw new Exception("O campo Estabelecimento é de preenchimento obrigatório.");
               

                if (dtInicio.NullableDate == null)
                    throw new Exception("O campo Período (Início) é de preenchimento obrigatório.");
                if (dtFim.NullableDate == null)
                    throw new Exception("O campo Período (Fim) é de preenchimento obrigatório.");

                if (dtInicio.NullableDate >= dtFim.NullableDate)
                    throw new Exception("O campo Período (Início) precisa ser menor que o campo Período (Fim).");

                var estabelecimentoId = (pckEstabelecimento.SelectedItem as Estabelecimento).Codigo;

                var url = Proxy.InventarioPorUnidadeUrl(
                    estabelecimentoId,
                    dtInicio.NullableDate.Value,
                    dtFim.NullableDate.Value);
                await Navigation.PushAsync(new RelatorioPage("INVENTÁRIO POR UNIDADE", url));
            }
            catch (Exception exception)
            {
                await DisplayAlert("Erro", exception.Message, "Ok");
            }
        }
    }
}