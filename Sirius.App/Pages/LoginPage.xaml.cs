﻿using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Sirius.App
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class LoginPage : VContentPage
    {
        public LoginPage()
        {
            InitializeComponent();
            Controller = new LoginController(this);
        }
    }
}