﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Sirius.App
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class AdicionarMaterialOrdemServicoPage : VContentPage
    {
		public AdicionarMaterialOrdemServicoPage ()
		{
			InitializeComponent ();
            Controller = new AdicionarMaterialOrdemServicoController(this);
        }

        public AdicionarMaterialOrdemServicoPage(int ordemSuperiorId)
        {
            InitializeComponent();
            Controller = new AdicionarMaterialOrdemServicoController(this, ordemServicoSuperiorId: ordemSuperiorId);
        }

        public AdicionarMaterialOrdemServicoPage(OrdemServico ordemServico)
        {
            InitializeComponent();
            Controller = new AdicionarMaterialOrdemServicoController(this, ordemServico);
        }
    }
}