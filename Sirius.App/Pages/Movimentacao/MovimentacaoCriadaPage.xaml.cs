﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Sirius.App
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class MovimentacaoCriadaPage : VContentPage
    {
		public MovimentacaoCriadaPage (int numeroMovimentacao, bool newMovimentacao = false)
		{
			InitializeComponent ();
            Controller = new MovimentacaoCriadaController(this, numeroMovimentacao, newMovimentacao);
        }
	}
}