﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Sirius.App
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class MovimentacaoPage : VContentPage
    {
        public Movimentacao Movimentacao { get; }
        private ObservableCollection<Item> Items { get; } = new ObservableCollection<Item>();
        private HashSet<Item> ItemsNovos { get; } = new HashSet<Item>();
        private List<Item> ItemsExcluidos { get; } = new List<Item>();

        public MovimentacaoPage(Movimentacao movimentacao = null)
		{
			InitializeComponent ();
            Controller = new MovimentacaoController(this);

            this.IsLoading = true;
            this.Movimentacao = movimentacao;
            if (this.Movimentacao != null)
                DesabilitarTudo();
            else
            {
                irMateriais.ItemsSource = Items;
                irReadOnlyMateriais.IsVisible = false;
                irMateriais.IsVisible = true;
            }
            ListViewEstabelecimento.IsVisible = false;
            ListViewEstabelecimentoDest.IsVisible = false;

            btnAdicionarMateriais.Clicked += BtnAdicionarMateriais_Clicked;
            btnFinalizar.Clicked += BtnFinalizar_Clicked;
            irMateriais.SelectCommand = new Command<Item>((item) => RemoverMaterial(item));

            pckTiposEstabelecimentos.SelectedIndexChanged += PckTiposEstabelecimentos_SelectedIndexChanged;
            pckDestTiposEstabelecimentos.SelectedIndexChanged += PckDestTiposEstabelecimentos_SelectedIndexChanged;
            pckTipoMovimentacao.SelectedIndexChanged += PckTipoMovimentacao_SelectedIndexChanged;
            pckUnidades.SelectedIndexChanged += PckUnidades_SelectedIndexChanged;
            pckEntradaSaida.SelectedIndexChanged += PckEntradaSaida_SelectedIndexChanged;

            EntryEstabelecimento.TextChanged += EntryEstabelecimento_TextChanged;
            ListViewEstabelecimento.ItemSelected += ListViewEstabelecimento_SelectedItem;


            EntryEstabelecimentoDest.TextChanged += EntryEstabelecimentoDest_TextChanged;
            ListViewEstabelecimentoDest.ItemSelected += ListViewEstabelecimentoDest_SelectedItem;
        }

        

        #region [ Carregamento ]
        private bool OnAppearingGuard = false;
        protected override async void OnAppearing()
        {
            IsLoading = true;
            try
            {
                if (!OnAppearingGuard)
                {
                    OnAppearingGuard = true;
                    await CarregarDadosTela();
                    if (Movimentacao != null)
                        await CarregarMovimentacao();
                    ConfigurarPaginaPorTipo();
                }
            }
            catch (Exception e)
            {
                await DisplayAlert("Erro", e.Message, "Ok");
            }
            finally
            {
                IsLoading = false;
            }
        }
        #endregion

        #region [ Carregamento de Estabelecimentos e Tipos ]
        private void PckEntradaSaida_SelectedIndexChanged(object sender, EventArgs e)
        {
            ConfigurarPaginaPorTipo();
        }

        private void PckUnidades_SelectedIndexChanged(object sender, EventArgs e)
        {
            ConfigurarPaginaPorTipo();
        }
        private void PckTipoMovimentacao_SelectedIndexChanged(object sender, EventArgs e)
        {
            ConfigurarPaginaPorTipo();
        }
        private async void PckDestTiposEstabelecimentos_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (IsLoading)
                return;

            aiCarregandoDestEstabelecimentos.IsVisible = true;
            EntryEstabelecimentoDest.IsVisible = false;
            
            try
            {
                var tipo = pckDestTiposEstabelecimentos.SelectedItem as TipoEstabelecimento;
                var estabelecimentosDest = TodosEstabelecimentosDest = await Business.Current.GetEstabelecimentosPorTipo(tipo.Codigo);
                txtDestEstabelecimento.Text = tipo.Nome;
                ListViewEstabelecimentoDest.ItemsSource = estabelecimentosDest;
                //ListViewEstabelecimentoDest.SelectedItem = estabelecimentos.First();
                txtDestEstabelecimento.IsVisible = true;
                EntryEstabelecimentoDest.IsVisible = true;
            }
            catch (Exception exception)
            {
                await DisplayAlert("Erro", exception.Message, "Ok");
            }
            finally
            {
                aiCarregandoDestEstabelecimentos.IsVisible = false;
                EntryEstabelecimentoDest.IsVisible = true;
            }
        }

        private void EntryEstabelecimento_TextChanged(object sender, TextChangedEventArgs e)
        {
            string esta = e.NewTextValue;
            List<Estabelecimento> ListEstabelecimentos;
            ListViewEstabelecimento.ItemsSource = ListEstabelecimentos = TodosEstabelecimentos.Where(f => f.NomeEstabelecimento.ToLower().Contains(esta.ToLower())).ToList();
            //mostrar aviso de lista vazia
            ListViewEstabelecimento.HeightRequest = (ListEstabelecimentos.Count < 10 ? ListEstabelecimentos.Count * 25 : 150);
            if (esta != string.Empty)
            {
                ListViewEstabelecimento.IsVisible = (ListEstabelecimentos.Count > 0);
            }
            else
            {
                ListViewEstabelecimento.IsVisible = false;
            }

        }
        private void ListViewEstabelecimento_SelectedItem(object sender, EventArgs e)
        {
            var tipo = ListViewEstabelecimento.SelectedItem as Estabelecimento;
            EntryEstabelecimento.Text = tipo.NomeEstabelecimento;
            ListViewEstabelecimento.IsVisible = false;
        }

        private void EntryEstabelecimentoDest_TextChanged(object sender, TextChangedEventArgs e)
        {
            string esta = e.NewTextValue;
            List<Estabelecimento> ListEstabelecimentosDest;
            ListViewEstabelecimentoDest.ItemsSource = ListEstabelecimentosDest = TodosEstabelecimentosDest.Where(f => f.NomeEstabelecimento.ToLower().Contains(esta.ToLower())).ToList();
            //mostrar aviso de lista vazia
            ListViewEstabelecimentoDest.HeightRequest = (ListEstabelecimentosDest.Count < 10 ? ListEstabelecimentosDest.Count * 25 : 150);
            if (esta != string.Empty)
            {
                ListViewEstabelecimentoDest.IsVisible = (ListEstabelecimentosDest.Count > 0);
            }
            else
            {
                ListViewEstabelecimentoDest.IsVisible = false;
            }
            
        }
        private void ListViewEstabelecimentoDest_SelectedItem(object sender, EventArgs e)
        {
            var tipo = ListViewEstabelecimentoDest.SelectedItem as Estabelecimento;
            EntryEstabelecimentoDest.Text = tipo.NomeEstabelecimento;
            ListViewEstabelecimentoDest.IsVisible = false;
        }

        public List<Estabelecimento> TodosEstabelecimentos { set; get; }
        public List<Estabelecimento> TodosEstabelecimentosDest { set; get; }
        private async void PckTiposEstabelecimentos_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (IsLoading)
                return;

            aiCarregandoEstabelecimentos.IsVisible = true;
            //pckEstabelecimento.IsVisible = false;
            EntryEstabelecimento.IsVisible = false;

            try
            {
                var tipo = pckTiposEstabelecimentos.SelectedItem as TipoEstabelecimento;
                 TodosEstabelecimentos = await Business.Current.GetEstabelecimentosPorTipo(tipo.Codigo);
                //pckEstabelecimento.ItemsSource = TodosEstabelecimentos;
                //pckEstabelecimento.SelectedItem = estabelecimentos.First();
                txtEstabelecimento.Text = tipo.Nome;
                txtEstabelecimento.IsVisible = true;
                //pckEstabelecimento.IsVisible = true;
                EntryEstabelecimento.IsVisible = true;
            }
            catch (Exception exception)
            {
                await DisplayAlert("Erro", exception.Message, "Ok");
            }
            finally
            {
                aiCarregandoEstabelecimentos.IsVisible = false;
                //pckEstabelecimento.IsVisible = true;
                EntryEstabelecimento.IsVisible = true;
            }
        }
        #endregion

        #region [ Ações ]
        private async void RemoverMaterial(Item i)
        {
            if (await DisplayActionSheet("Deseja remover?", "Não", "Sim") == "Sim")
            {
                if (this.ItemsNovos.Contains(i))
                {
                    this.ItemsNovos.Remove(i);
                    this.Items.Remove(i);
                }
                else
                {
                    this.ItemsExcluidos.Add(i);
                    this.Items.Remove(i);
                }
                Items.Remove(i);
            }
        }
        private async void BtnFinalizar_Clicked(object sender, EventArgs e)
        {
            aiFinalizando.IsVisible = true;
            btnFinalizar.IsVisible = false;
            try
            {
                ValidarCampos();

                var registroMovimentacao = new RegistroMovimentacao()
                {
                    Codigo = Movimentacao?.Codigo ?? 0,
                    DataHora = Movimentacao?.DataHora ?? DateTime.Now,
                    DataRetorno = dtRetorno.NullableDate ?? DateTime.MinValue,
                    CodigoTipoMovimentacao = (pckTipoMovimentacao.SelectedItem as TipoMovimentacao).Codigo,
                    CodigoDestinatario = (pckDestinatario.SelectedItem as Destinatario)?.Codigo ?? 0,
                    Itens = Items.ToList(),
                    ItensExcluidos = ItemsExcluidos.ToList(),
                    Observacao = txtObservacoes.Text,
                    //EstabelecimentoCedenteModel = (pckEstabelecimento.SelectedItem as Estabelecimento),
                    EstabelecimentoCedenteModel = (ListViewEstabelecimento.SelectedItem as Estabelecimento),
                    UsuarioCedenteModel = Business.Current.UsuarioLogado,
                    EstabelecimentoRequisitanteModel = (ListViewEstabelecimentoDest.SelectedItem as Estabelecimento),
                    UsuarioRequisitanteModel = Business.Current.UsuarioLogado, //TODO: Provavelmente está errado
                    TipoOutros = (pckOutros.SelectedItem as TipoEstabelecimento)?.Codigo?[0] ?? '0',
                    TipoSolicitacao = (pckEntradaSaida.SelectedItem as TipoEstabelecimento)?.Codigo?[0] ?? ' '
                };

                var newMovimentacao = registroMovimentacao.Codigo == 0 ? true : false;
                var number = await Business.Current.RegistrarMovimentacao(registroMovimentacao);
                await Navigation.PushAsync(new MovimentacaoCriadaPage(number, newMovimentacao));

                //remove páginas anteriores
                var pagesToRemove = new List<Page>();
                foreach (var page in Navigation.NavigationStack)
                {
                    if (page is AdicionarMaterialMovimentacaoPage || page is MovimentacaoPage)
                        pagesToRemove.Add(page);
                }
                foreach (var page in pagesToRemove)
                {
                    Navigation.RemovePage(page);
                }
            }
            catch(Exception ex)
            {
                await DisplayAlert("Erro", ex.Message, "Ok");
            }
            finally
            {
                aiFinalizando.IsVisible = false;
                btnFinalizar.IsVisible = true;
            }
        }

        private void ValidarCampos()
        {
            switch ((pckTipoMovimentacao.SelectedItem as TipoMovimentacao).Codigo)
            {
                //Alienação
                case 1:
                    if (ListViewEstabelecimento.SelectedItem == null)
                        throw new Exception("É necessário informar um estabelecimento (ORIGEM).");
                    if (pckDestinatario.SelectedItem == null)
                        throw new Exception("É necessário informar um destinatário.");
                    //if (txtObservacoes.Text.IsEmpty())
                    //    throw new Exception("É necessário colocar uma observação.");
                    break;
                //Cessão de Uso
                case 2:
                    if (ListViewEstabelecimento.SelectedItem == null)
                        throw new Exception("É necessário informar um estabelecimento (ORIGEM).");
                    if (pckDestinatario.SelectedItem == null)
                        throw new Exception("É necessário informar um destinatário.");
                    if (dtRetorno.NullableDate == null)
                        throw new Exception("É necessário informar uma Data de Retorno.");
                    //if (txtObservacoes.Text.IsEmpty())
                    //    throw new Exception("É necessário colocar uma observação.");
                    break;
                //Devolução
                case 3:
                    if (ListViewEstabelecimento.SelectedItem == null)
                        throw new Exception("É necessário informar um estabelecimento (ORIGEM).");
                    //if (txtObservacoes.Text.IsEmpty())
                    //    throw new Exception("É necessário colocar uma observação.");
                    if ((pckUnidades.SelectedItem as TipoEstabelecimento)?.Codigo == "E") //Externa
                    {
                        if (pckEntradaSaida.SelectedItem == null)
                            throw new Exception("É necessário informar um tipo de solicitação.");
                        if (pckDestinatario.SelectedItem == null)
                            throw new Exception("É necessário informar um destinatário.");
                    }
                    else
                    {
                        if (ListViewEstabelecimentoDest.SelectedItem == null)
                            throw new Exception("É necessário informar um estabelecimento (DESTINO).");
                    }
                    break;
                //Evento
                case 4:
                    if (ListViewEstabelecimento.SelectedItem == null)
                        throw new Exception("É necessário informar um estabelecimento (ORIGEM).");
                    //if (txtObservacoes.Text.IsEmpty())
                    //    throw new Exception("É necessário colocar uma observação.");
                    if ((pckUnidades.SelectedItem as TipoEstabelecimento)?.Codigo == "E") //Externa
                    {
                        if (pckEntradaSaida.SelectedItem == null)
                            throw new Exception("É necessário informar um tipo de solicitação.");
                        if (pckDestinatario.SelectedItem == null)
                            throw new Exception("É necessário informar um destinatário.");
                    }
                    else
                    {
                        if (ListViewEstabelecimentoDest.SelectedItem == null)
                            throw new Exception("É necessário informar um estabelecimento (DESTINO).");
                    }
                    break;
                //Reforma
                case 5:
                    if (ListViewEstabelecimento.SelectedItem == null)
                        throw new Exception("É necessário informar um estabelecimento (ORIGEM).");
                    if (pckDestinatario.SelectedItem == null)
                        throw new Exception("É necessário informar um destinatário.");
                    //if (txtObservacoes.Text.IsEmpty())
                    //    throw new Exception("É necessário colocar uma observação.");
                    break;
                //Manutenção
                case 6:
                    if (ListViewEstabelecimento.SelectedItem == null)
                        throw new Exception("É necessário informar um estabelecimento (ORIGEM).");
                    //if (txtObservacoes.Text.IsEmpty())
                    //    throw new Exception("É necessário colocar uma observação.");
                    if ((pckUnidades.SelectedItem as TipoEstabelecimento)?.Codigo == "E") //Externa
                    {
                        if (pckDestinatario.SelectedItem == null)
                            throw new Exception("É necessário informar um destinatário.");
                    }
                    else
                    {
                        if (ListViewEstabelecimentoDest.SelectedItem == null)
                            throw new Exception("É necessário informar um estabelecimento (DESTINO).");
                    }
                    break;
                //Transferência Interna
                case 7:
                    if (ListViewEstabelecimento.SelectedItem == null)
                        throw new Exception("É necessário informar um estabelecimento (ORIGEM).");
                    //if (txtObservacoes.Text.IsEmpty())
                    //    throw new Exception("É necessário colocar uma observação.");
                    if (ListViewEstabelecimentoDest.SelectedItem == null)
                        throw new Exception("É necessário informar um estabelecimento (DESTINO).");
                    break;
                //Empréstimo
                case 8:
                    if (ListViewEstabelecimento.SelectedItem == null)
                        throw new Exception("É necessário informar um estabelecimento (ORIGEM).");
                    //if (txtObservacoes.Text.IsEmpty())
                    //    throw new Exception("É necessário colocar uma observação.");
                    if ((pckUnidades.SelectedItem as TipoEstabelecimento)?.Codigo == "E") //Externa
                    {
                        if (pckEntradaSaida.SelectedItem == null)
                            throw new Exception("É necessário informar um tipo de solicitação.");
                        if (pckDestinatario.SelectedItem == null)
                            throw new Exception("É necessário informar um destinatário.");
                    }
                    else
                    {
                        if (ListViewEstabelecimentoDest.SelectedItem == null)
                            throw new Exception("É necessário informar um estabelecimento (DESTINO).");
                    }
                    break;
                //Outros
                case 9:
                    if (ListViewEstabelecimento.SelectedItem == null)
                        throw new Exception("É necessário informar um estabelecimento (ORIGEM).");
                    if (pckOutros.SelectedItem == null)
                        throw new Exception("É necessário informar uma razão.");
                    break;
                default:
                    break;
            }
        }

        private async void BtnAdicionarMateriais_Clicked(object sender, EventArgs e)
        {
            aiFinalizando.IsVisible = true;
            btnFinalizar.IsVisible = false;
            try
            {
                    ValidarCampos();

                // pckEstabelecimento
                // pckDestEstabelecimento
                Estabelecimento EstabelesimentoOrigem = ListViewEstabelecimento.SelectedItem as Estabelecimento;
                Estabelecimento EstabelesimentoDestino = ListViewEstabelecimentoDest.SelectedItem as Estabelecimento;
                

                await Navigation.PushAsync(new AdicionarMaterialMovimentacaoPage( EstabelesimentoOrigem, EstabelesimentoDestino, Movimentacao, async (Item i) =>
                {
                    if(Items.Any(a => a.Codigo == i.Codigo))
                    {
                        await DisplayAlert("Erro", "Este item já foi adicionado.", "Ok");
                        return;
                    }
                    ItemsNovos.Add(i);
                    Items.Add(i);
                }));
            }
            catch(Exception ex)
            {
                await DisplayAlert("Erro", ex.Message, "Ok");
            }

            finally
            {
                aiFinalizando.IsVisible = false;
                btnFinalizar.IsVisible = true;
            }
        }
        #endregion

        private async Task CarregarMovimentacao()
        {
            pckTipoMovimentacao.SelectedItem = pckTipoMovimentacao.ItemsSource.Cast<TipoMovimentacao>().FirstOrDefault(t => t.Codigo == Movimentacao.CodigoTipoMovimentacao);
            if (Movimentacao.EstabelecimentoCedenteModel != null)
            {
                var tipoEstabelecimento = pckTiposEstabelecimentos.ItemsSource.Cast<TipoEstabelecimento>().FirstOrDefault(a => a.Codigo == Movimentacao.EstabelecimentoCedenteModel?.Tipo);
                pckTiposEstabelecimentos.SelectedItem = tipoEstabelecimento;
                txtEstabelecimento.Text = tipoEstabelecimento?.Nome;

                if(tipoEstabelecimento != null)
                {
                    var estabelecimentos = await Business.Current.GetEstabelecimentosPorTipo(tipoEstabelecimento.Codigo);
                    ListViewEstabelecimento.ItemsSource = estabelecimentos;
                    //pckEstabelecimento.SelectedItem = estabelecimentos.FirstOrDefault(a => a.Codigo == Movimentacao.EstabelecimentoCedenteModel?.Codigo);
                }
            }
            pckOutros.SelectedItem = pckOutros.ItemsSource.Cast<TipoEstabelecimento>().FirstOrDefault(t => t.Codigo == Movimentacao.TipoOutros.ToString());
            pckDestinatario.SelectedItem = pckDestinatario.ItemsSource.Cast<Destinatario>().FirstOrDefault(t => t.Codigo == Movimentacao.Destinatario?.Codigo);
            var unidade = (Movimentacao.TipoSolicitacao == 'S' || Movimentacao.TipoSolicitacao == 'E') ? "E" : "I";
            pckUnidades.SelectedItem = pckUnidades.ItemsSource.Cast<TipoEstabelecimento>().FirstOrDefault(t => t.Codigo == unidade);
            pckEntradaSaida.SelectedItem = pckEntradaSaida.ItemsSource.Cast<TipoEstabelecimento>().FirstOrDefault(t => t.Codigo == Movimentacao.TipoSolicitacao.ToString());
            dtRetorno.NullableDate = Movimentacao.DataRetorno == DateTime.MinValue || Movimentacao.DataRetorno == new DateTime() ? (DateTime?)null : Movimentacao.DataRetorno;
            if (Movimentacao.EstabelecimentoRequisitanteModel != null)
            {
                var tipoEstabelecimento = pckDestTiposEstabelecimentos.ItemsSource.Cast<TipoEstabelecimento>().FirstOrDefault(a => a.Codigo == Movimentacao.EstabelecimentoRequisitanteModel.Tipo);
                pckDestTiposEstabelecimentos.SelectedItem = tipoEstabelecimento;
                txtDestEstabelecimento.Text = tipoEstabelecimento?.Nome;

                if (tipoEstabelecimento != null)
                {
                    var estabelecimentos = await Business.Current.GetEstabelecimentosPorTipo(tipoEstabelecimento.Codigo);
                    ListViewEstabelecimentoDest.ItemsSource = estabelecimentos;
                   // ListViewEstabelecimentoDest.SelectedItem = estabelecimentos.FirstOrDefault(a => a.Codigo == Movimentacao.EstabelecimentoRequisitanteModel.Codigo);
                }
            }
            txtObservacoes.Text = Movimentacao.Observacao;
            if(Movimentacao.Itens != null)
            {
                foreach (var item in Movimentacao.Itens)
                {
                    Items.Add(item);
                }
                irReadOnlyMateriais.ItemsSource = Items;
                irReadOnlyMateriais.IsVisible = true;
                irMateriais.IsVisible = false;
            }
        }
        private async Task CarregarDadosTela()
        {
            pckTiposEstabelecimentos.ItemsSource = new List<TipoEstabelecimento>()
                {
                    new TipoEstabelecimento(TipoEstabelecimento.CoordenacaoCodigo, TipoEstabelecimento.Coordenacao),
                    new TipoEstabelecimento(TipoEstabelecimento.SubcoordenacaoCodigo, TipoEstabelecimento.Subcoordenacao),
                    new TipoEstabelecimento(TipoEstabelecimento.DistritoSanitarioCodigo, TipoEstabelecimento.DistritoSanitario),
                    new TipoEstabelecimento(TipoEstabelecimento.UnidadeDeSaudeCodigo, TipoEstabelecimento.UnidadeDeSaude)
                };
            pckDestTiposEstabelecimentos.ItemsSource = new List<TipoEstabelecimento>()
                {
                    new TipoEstabelecimento(TipoEstabelecimento.CoordenacaoCodigo, TipoEstabelecimento.Coordenacao),
                    new TipoEstabelecimento(TipoEstabelecimento.SubcoordenacaoCodigo, TipoEstabelecimento.Subcoordenacao),
                    new TipoEstabelecimento(TipoEstabelecimento.DistritoSanitarioCodigo, TipoEstabelecimento.DistritoSanitario),
                    new TipoEstabelecimento(TipoEstabelecimento.UnidadeDeSaudeCodigo, TipoEstabelecimento.UnidadeDeSaude)
                };
            pckOutros.ItemsSource = new List<TipoEstabelecimento>()
                {
                    new TipoEstabelecimento("E", "Extravio"),
                    new TipoEstabelecimento("P", "Perda"),
                    new TipoEstabelecimento("R", "Roubo"),
                    new TipoEstabelecimento("S", "Sinistro")
                };

            pckDestinatario.ItemsSource = await Business.Current.GetDestinatarios();
            var tiposMovimentacoes = await Business.Current.GetTiposMovimentacoes();
            pckTipoMovimentacao.ItemsSource = tiposMovimentacoes;
            pckTipoMovimentacao.SelectedItem = tiposMovimentacoes.First();
            var unidades = new List<TipoEstabelecimento>()
                    {
                        new TipoEstabelecimento("I", "Interna"),
                        new TipoEstabelecimento("E", "Externa"),
                    };
            pckUnidades.ItemsSource = unidades;
            pckUnidades.SelectedItem = unidades.First();
            pckEntradaSaida.ItemsSource = new List<TipoEstabelecimento>()
                    {
                        new TipoEstabelecimento("E", "Entrada"),
                        new TipoEstabelecimento("S", "Saída"),
                    };
        }
        private void DesabilitarTudo()
        {
            pckTipoMovimentacao.IsEnabled = false;
            pckTiposEstabelecimentos.IsEnabled = false;
            //pckEstabelecimento.IsEnabled = false;
            EntryEstabelecimento.IsEnabled = false;
            pckOutros.IsEnabled = false;
            pckDestinatario.IsEnabled = false;
            pckUnidades.IsEnabled = false;
            pckEntradaSaida.IsEnabled = false;
            dtRetorno.IsEnabled = false;
            pckDestTiposEstabelecimentos.IsEnabled = false;
            //pckDestEstabelecimento.IsEnabled = false;
            EntryEstabelecimentoDest.IsEnabled = false;
            txtObservacoes.IsEnabled = false;
            btnAdicionarMateriais.IsVisible = false;
            btnFinalizar.IsVisible = false;
            irMateriais.IsVisible = false;
            irReadOnlyMateriais.IsVisible = true;
        }
        private void ConfigurarPaginaPorTipo()
        {
            txtDestinatario.IsVisible = false;
            pckDestinatario.IsVisible = false;
            txtUnidades.IsVisible = false;
            pckUnidades.IsVisible = false;
            txtEntradaSaida.IsVisible = false;
            pckEntradaSaida.IsVisible = false;
            txtRetorno.IsVisible = false;
            dtRetorno.IsVisible = false;
            txtDestTiposEstabelecimentos.IsVisible = false;
            pckDestTiposEstabelecimentos.IsVisible = false;
            txtDestEstabelecimento.IsVisible = false;
            //pckDestEstabelecimento.IsVisible = false;
            EntryEstabelecimentoDest.IsVisible = false;
            stkDestino.IsVisible = false;
            txtObservacoes.IsVisible = false;
            lblObservacoes.IsVisible = false;
            txtOutros.IsVisible = false;
            pckOutros.IsVisible = false;
            txtEstabelecimento.IsVisible = false;
            //pckEstabelecimento.IsVisible = false;
            EntryEstabelecimento.IsVisible = false;

            if (ListViewEstabelecimento.SelectedItem != null)
            {
                txtEstabelecimento.IsVisible = true;
               // pckEstabelecimento.IsVisible = true;
                EntryEstabelecimento.IsVisible = true;
            }
            txtDestinatario.Text = "Destinatário";
            LabelDESTINO.Text = "DESTINO";
            LabelORIGEM.Text = "OREGEM";

            switch ((pckTipoMovimentacao.SelectedItem as TipoMovimentacao).Codigo)
            {
                //Alienação
                case 1:
                    stkDestino.IsVisible = true;
                    txtObservacoes.IsVisible = true;
                    lblObservacoes.IsVisible = true;
                    txtDestinatario.IsVisible = true;
                    pckDestinatario.IsVisible = true;
                    
                    break;
                //Cessão de Uso
                case 2:
                    stkDestino.IsVisible = true;
                    txtObservacoes.IsVisible = true;
                    lblObservacoes.IsVisible = true;
                    txtDestinatario.IsVisible = true;
                    pckDestinatario.IsVisible = true;
                    txtRetorno.IsVisible = true;
                    dtRetorno.IsVisible = true;
                    
                    break;
                //Devolução
                case 3:
                    stkDestino.IsVisible = true;
                    txtObservacoes.IsVisible = true;
                    lblObservacoes.IsVisible = true;
                    txtUnidades.IsVisible = true;
                    pckUnidades.IsVisible = true;
                    if ((pckUnidades.SelectedItem as TipoEstabelecimento)?.Codigo == "E") //Externa
                    {
                        txtEntradaSaida.IsVisible = true;
                        pckEntradaSaida.IsVisible = true;
                        txtDestinatario.IsVisible = true;
                        pckDestinatario.IsVisible = true;
                        if ((pckEntradaSaida.SelectedItem as TipoEstabelecimento)?.Codigo == "E") //Entrada
                        {
                            txtDestinatario.Text = "Cedente";
                            LabelDESTINO.Text = "ORIGEM";
                            LabelORIGEM.Text = "DESTINO";
                        }
                    }
                    else //INTERNA
                    {
                        txtDestTiposEstabelecimentos.IsVisible = true;
                        pckDestTiposEstabelecimentos.IsVisible = true;
                        var showEstabelecimentos0 = (pckDestTiposEstabelecimentos.SelectedItem as TipoEstabelecimento) != null;
                        txtDestEstabelecimento.IsVisible = showEstabelecimentos0;
                        EntryEstabelecimentoDest.IsVisible = showEstabelecimentos0;
                    }
                    break;
                //Evento
                case 4:
                    stkDestino.IsVisible = true;
                    txtObservacoes.IsVisible = true;
                    lblObservacoes.IsVisible = true;
                    txtUnidades.IsVisible = true;
                    pckUnidades.IsVisible = true;
                    if ((pckUnidades.SelectedItem as TipoEstabelecimento)?.Codigo == "E") //Externa
                    {
                        txtEntradaSaida.IsVisible = true;
                        pckEntradaSaida.IsVisible = true;
                        txtDestinatario.IsVisible = true;
                        pckDestinatario.IsVisible = true;
                        if ((pckEntradaSaida.SelectedItem as TipoEstabelecimento)?.Codigo == "E") //Entrada
                        {
                            txtDestinatario.Text = "Cedente";
                            LabelDESTINO.Text = "ORIGEM";
                            LabelORIGEM.Text = "DESTINO";
                        }
                    }
                    else //INTERNA
                    {
                        txtDestTiposEstabelecimentos.IsVisible = true;
                        pckDestTiposEstabelecimentos.IsVisible = true;
                        var showEstabelecimentos1 = (pckDestTiposEstabelecimentos.SelectedItem as TipoEstabelecimento) != null;
                        txtDestEstabelecimento.IsVisible = showEstabelecimentos1;
                        EntryEstabelecimentoDest.IsVisible = showEstabelecimentos1;
                    }
                    break;
                //Reforma
                case 5:
                    stkDestino.IsVisible = true;
                    txtObservacoes.IsVisible = true;
                    lblObservacoes.IsVisible = true;
                    txtDestinatario.IsVisible = true;
                    pckDestinatario.IsVisible = true;
                    break;
                //Manutenção
                case 6:
                    stkDestino.IsVisible = true;
                    txtObservacoes.IsVisible = true;
                    lblObservacoes.IsVisible = true;
                    txtUnidades.IsVisible = true;
                    pckUnidades.IsVisible = true;
                    if ((pckUnidades.SelectedItem as TipoEstabelecimento)?.Codigo == "E") //Externa
                    {
                        txtDestinatario.IsVisible = true;
                        pckDestinatario.IsVisible = true;
                    }
                    else //INTERNA
                    {
                        txtDestTiposEstabelecimentos.IsVisible = true;
                        pckDestTiposEstabelecimentos.IsVisible = true;
                        var showEstabelecimentos2 = (pckDestTiposEstabelecimentos.SelectedItem as TipoEstabelecimento) != null;
                        txtDestEstabelecimento.IsVisible = showEstabelecimentos2;
                        EntryEstabelecimentoDest.IsVisible = showEstabelecimentos2;
                    }
                    break;
                //Transferência Interna
                case 7:
                    stkDestino.IsVisible = true;
                    txtObservacoes.IsVisible = true;
                    lblObservacoes.IsVisible = true;
                    txtDestTiposEstabelecimentos.IsVisible = true;
                    pckDestTiposEstabelecimentos.IsVisible = true;
                    var showEstabelecimentos3 = (pckDestTiposEstabelecimentos.SelectedItem as TipoEstabelecimento) != null;
                    txtDestEstabelecimento.IsVisible = showEstabelecimentos3;
                    EntryEstabelecimentoDest.IsVisible = showEstabelecimentos3;
                    break;
                //Empréstimo
                case 8:
                    stkDestino.IsVisible = true;
                    txtObservacoes.IsVisible = true;
                    lblObservacoes.IsVisible = true;
                    txtUnidades.IsVisible = true;
                    pckUnidades.IsVisible = true;
                    if ((pckUnidades.SelectedItem as TipoEstabelecimento)?.Codigo == "E") //Externa
                    {
                        txtEntradaSaida.IsVisible = true;
                        pckEntradaSaida.IsVisible = true;
                        txtDestinatario.IsVisible = true;
                        pckDestinatario.IsVisible = true;

                    }
                    else //INTERNA
                    {
                        txtDestTiposEstabelecimentos.IsVisible = true;
                        pckDestTiposEstabelecimentos.IsVisible = true;
                        var showEstabelecimentos = (pckDestTiposEstabelecimentos.SelectedItem as TipoEstabelecimento) != null;
                        txtDestEstabelecimento.IsVisible = showEstabelecimentos;
                        EntryEstabelecimentoDest.IsVisible = showEstabelecimentos;

                    }
                    break;
                //Outros
                case 9:
                    stkDestino.IsVisible = true;
                    txtObservacoes.IsVisible = true;
                    lblObservacoes.IsVisible = true;
                    txtOutros.IsVisible = true;
                    pckOutros.IsVisible = true;

                    break;
                default:
                    break;
            }
        }
    }
}