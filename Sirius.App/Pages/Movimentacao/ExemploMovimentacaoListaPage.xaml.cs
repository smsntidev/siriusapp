﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Sirius.App
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class ExemploMovimentacaoListaPage : VContentPage
    {
        //Caso a página tenha mais de um construtor, repita o código
        //que seta os comandos em cada construtor
        public ExemploMovimentacaoListaPage()
        {
            InitializeComponent();
            //Estes comandos antes eram setados no controller
            //Agora setamos aqui no construtor
            ShowMenuCommand = new Command(() => ShowMenu());
            GoBackCommand = new Command(() => GoBack());
            AddCommand = new Command(() => Add());
            //Tudo que puder ser inicializado no construtor deve ser inicializado no construtor
            //no entanto, construtores não podem utilizar async/await. Quando tiver async/await,
            //fazer a inicialização no OnAppearing

            //vamos setar o clique de um botão
           // btnProcurarMaterial.Clicked += BtnProcurarMaterial_Clicked;
            btnProcurarMaterialPorCodigo.Clicked += BtnProcurarMaterialPorCodigo_Clicked;
            //A propriedade SelectCommand do ItemRepeater aceita um comando que será executado quando um
            //item da lista é clicado.
            itemRepeater.SelectCommand = new Command<Movimentacao>((Movimentacao m) => MovimentacaoClicada(m));
            //pckTiposBuscaMaterial.SelectedIndexChanged += PckTiposBuscaMaterial_SelectedIndexChanged;
            //btnProcurarMaterial.IsEnabled = false;
        }

        //m é o item da lista que foi clicado
        private async void MovimentacaoClicada(Movimentacao m)
        {
            await Navigation.PushAsync(new MovimentacaoPage(m));
        }

        private async void BtnProcurarMaterialPorCodigo_Clicked(object sender, EventArgs e)
        {
            //É necessário proteger o código para que a aplicação não dê crash caso haja uma exceção.
            //Qualquer código que chame algo no servidor precisa estar protegido por um try/catch
            try
            {
                var tipoMaterial = pckTipo.SelectedItem as TipoBuscaMaterial;
                var movimentacoes = await Business.Current.GetMovimentacoesByFiltro(txtCodigoMaterial.Text, tipoMaterial.Codigo);
                itemRepeater.ItemsSource = movimentacoes;
                //Eu percebi que o objeto movimentação tem um campo DataHora do tipo DateTime que tem um binding na tela com a
                //propriedade Text de um Label. A propriedade Label do Text é do tipo String, nesse caso seria bom fazer sempre
                //bindings com uma propriedade do mesmo tipo.
            }
            catch(Exception exception)
            {
                await DisplayAlert("Erro", exception.Message, "Ok");
            }
        }

        //eu posso ter awaits dentro de um evento
        private async void BtnProcurarMaterial_Clicked(object sender, EventArgs e)
        {
            //var result = await Extensions.ScanBarcode();
            //var movimentacoes = await Business.Current.GetMovimentacoesByFiltro(txtCodigoMaterial.Text, tipoMaterial.Codigo);
            //itemRepeater.ItemsSource = movimentacoes;
            //TODO ???
            //vamos fingir que a gente tenta ler o código de barra e depois mostramos uma mensagem de erro
            //await Task.Delay(5000);
            //await DisplayAlert("Erro", "Erro ao tentar ler código de barras.", "Ok");
        }

        private bool OnAppearingGuard = false;
        //Utilizar o OnAppearing para inicialização apenas quando for necessário utilizar o await
        protected override async void OnAppearing()
        {
            //O OnAppearing pode ser chamado múltiplas vezes pelo Android. Por exemplo, se o usuário navegar
            //para uma outra página e depois retornar para esta página, o OnAppearing será chamado novamente
            //pois a página apareceu de novo. Geralmente nós só queremos que um código de inicialização seja
            //executado apenas uma vez: na primeira vez que a página aparece para o usuário. Este 'if' faz com
            //que o código dentro dele seja executado apenas pela primeira vez que a página aparece.
            if (!OnAppearingGuard)
            {
                OnAppearingGuard = true;

                //pega os tipos de materiais
                var materiais = await Business.Current.GetTipoMateriais();
                pckTipo.ItemsSource = materiais;
                //pckTiposBuscaMaterial.ItemsSource = materiais;
                //seta o valor padrão do picker
                pckTipo.SelectedItem = materiais.First();
                
            }
        }

        //private void PckTiposBuscaMaterial_SelectedIndexChanged(object sender, EventArgs e)
        //{
        //    if(pckTiposBuscaMaterial.SelectedIndex != -1)
        //    {
        //        btnProcurarMaterial.IsEnabled = true;
        //    }
        //}

        private async void Add()
        {
            await Navigation.PushAsync(new MovimentacaoPage());
        }

        private async void GoBack()
        {
            await Navigation.PopAsync();
        }

        private void ShowMenu()
        {
            Element current = this;
            while (current != null && !(current is Xamarin.Forms.MasterDetailPage))
                current = current.Parent;

            if (current != null)
            {
                ((Xamarin.Forms.MasterDetailPage)current).IsPresented = true;
            }
        }
    }
}