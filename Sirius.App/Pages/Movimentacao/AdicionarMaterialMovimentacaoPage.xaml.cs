﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Sirius.App
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class AdicionarMaterialMovimentacaoPage : VContentPage
    {
        public AdicionarMaterialMovimentacaoPage(Estabelecimento estabelecimentoOrigem, Estabelecimento estabelecimentoDestino, Movimentacao movimentacao, Action<Item> onAdd)
        {
            InitializeComponent();
            Controller = new AdicionarMaterialMovimentacaoController(this,estabelecimentoOrigem,estabelecimentoDestino, movimentacao, onAdd);
        }
    }
}