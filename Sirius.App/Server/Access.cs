﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace Sirius.App.Server
{
    public class Access
    {
#if DEBUG
        public const string ServiceUrl = "http://172.22.16.30/siriuswebapi/";
        //public const string ServiceUrl = "http://siriusapi.conecti.com.br/";
        //public const string ServiceUrl = "http://10.0.2.2/";
        //public const string ServiceUrl = "http://192.168.10.147/";
#else
        public const string ServiceUrl = "http://172.22.16.30/siriuswebapi/";
        //public const string ServiceUrl = "http://siriusapi.conecti.com.br/";
#endif

        public class NoData { }

        private Access()
        {

        }
        public static Access Current { get; } = new Access();

        public bool HasCredentials()
        {
            return Application.Current.Properties.ContainsKey("Access.Username") && Application.Current.Properties.ContainsKey("Access.Password");
        }

        public async Task<bool> Login(string username, string password)
        {
            if (username == null)
                throw new ArgumentNullException("username");
            if (password == null)
                throw new ArgumentNullException("password");

            var url = $"{ServiceUrl}api/Autentica/Login";
            var client = new HttpClient();
            client.Timeout = TimeSpan.FromMinutes(2); //2 minutes

            var stringContent = new StringContent( JsonConvert.SerializeObject(new { Login = username, Senha = password }),
                                        Encoding.UTF8,
                                        "application/json");

            try
            {
                var response = await client.PostAsync(url, stringContent);
                if (response.IsSuccessStatusCode)
                {
                    var content = await response.Content.ReadAsStringAsync();
                    var okResponse = JsonConvert.DeserializeObject<OkResponse<bool>>(content);

                    if (!okResponse.OK)
                        throw AccessException.Create(System.Net.HttpStatusCode.BadRequest, content);

                    if (!okResponse.Data)
                        throw new BusinessException("Usuário/Senha inválidos.");

                    Application.Current.Properties["Access.Username"] = username;
                    Application.Current.Properties["Access.Password"] = password;
                    await Application.Current.SavePropertiesAsync();
                    return true;
                }
                else
                {
                    var content = await response.Content.ReadAsStringAsync();
                    throw AccessException.Create(response.StatusCode, content);
                }
            }
            catch (AccessException) { throw; }
            catch (BusinessException) { throw; }
            catch (Exception ex)
            {
                Debug.WriteLine($"{ex.Message} - StackTrace: {ex.StackTrace}");
                throw AccessException.Unknown();
            }
        }

        public async Task LogOff()
        {
            if (Application.Current.Properties.ContainsKey("Access.Username"))
                Application.Current.Properties.Remove("Access.Username");
            if (Application.Current.Properties.ContainsKey("Access.Password"))
                Application.Current.Properties.Remove("Access.Password");

            await Application.Current.SavePropertiesAsync();
        }

        public async Task<bool> CheckLogin()
        {
            if (!Application.Current.Properties.ContainsKey("Access.Username") || !Application.Current.Properties.ContainsKey("Access.Password"))
                return false;

            try
            {
                await Login((string)Application.Current.Properties["Access.Username"], (string)Application.Current.Properties["Access.Password"]);
                return true;
            }
            catch
            {
                await LogOff();
                return false;
            }
        }

        private bool QuickCheckLogin()
        {
            if (!Application.Current.Properties.ContainsKey("Access.Username") || !Application.Current.Properties.ContainsKey("Access.Password"))
                return false;
            else
                return true;
        }

        public Task<Output> Post<Output, Input>(string path, Input input)
        {
            return Call<Output, Input>(HttpMethod.Post, path, input);
        }
        public Task<Output> Get<Output>(string path)
        {
            return Call<Output, NoData>(HttpMethod.Get, path, null);
        }
        public Task<Output> Delete<Output, Input>(string path)
        {
            return Call<Output, NoData>(HttpMethod.Delete, path, null);
        }
        public Task<Output> Put<Output, Input>(string path, Input input)
        {
            return Call<Output, Input>(HttpMethod.Put, path, input);
        }
        public Task<Output> CachedPost<Output, Input>(string path, Input input, bool alwaysAskServer = true)
        {
            return CachedAccess.Current.CachedGet<Output>(path, input != null ? JsonConvert.SerializeObject(input) : "null").Get(
                () => Post<Output, Input>(path, input), onlyIfError: !alwaysAskServer);
        }
        public Task<Output> CachedGet<Output>(string path, bool alwaysAskServer = true)
        {
            return CachedAccess.Current.CachedGet<Output>(path, "null").Get(
                () => Get<Output>(path), onlyIfError: !alwaysAskServer);
        }
        
        private async Task<Output> Call<Output, Input>(HttpMethod method, string path, Input input, bool loginIfUnauthorized)
        {
            if (!QuickCheckLogin())
                throw AccessException.Unknown();

            var url = $"{ServiceUrl}api/{path}";
            var client = new HttpClient();
            client.Timeout = TimeSpan.FromMinutes(2); //2 minutes
            var username = (string)Application.Current.Properties["Access.Username"];
            var password = (string)Application.Current.Properties["Access.Password"];
            client.DefaultRequestHeaders.Authorization = new System.Net.Http.Headers.AuthenticationHeaderValue("Basic", 
                Convert.ToBase64String(Encoding.UTF8.GetBytes($"{username}:{password}")));

            try
            {
                var json = (input != null) ? JsonConvert.SerializeObject(input) : "{}";
                var content = new StringContent(json, Encoding.UTF8, "application/json");

                HttpResponseMessage response = null;
                if (method == HttpMethod.Get)
                    response = await client.GetAsync(url);
                else if (method == HttpMethod.Delete)
                    response = await client.DeleteAsync(url);
                else if (method == HttpMethod.Post)
                    response = await client.PostAsync(url, content);
                else
                    response = await client.PutAsync(url, content);

                if (response.IsSuccessStatusCode)
                {
                    if (typeof(Output) == typeof(byte[]))
                    {
                        var byteArrayResponse = await response.Content.ReadAsByteArrayAsync();
                        return (Output)(object)byteArrayResponse;
                    }
                    else
                    {
                        var strResponse = await response.Content.ReadAsStringAsync();
                        try
                        {
                            var serverResponse = JsonConvert.DeserializeObject<OkResponse<Output>>(strResponse);
                            if (!serverResponse.OK)
                            {
                                Debug.WriteLine(strResponse);
                                throw AccessException.Create(serverResponse.Exception);
                            }
                            return serverResponse.Data;
                        }
                        catch (AccessException)
                        {
                            throw;
                        }
                        catch (Exception e)
                        {
                            if (typeof(Output) == typeof(string))
                                return (Output)(object)strResponse;
                            else
                                throw e;
                        }
                    }
                }
                else
                {
                    if (response.StatusCode == System.Net.HttpStatusCode.Unauthorized && loginIfUnauthorized && (await CheckLogin()))
                        return await Call<Output, Input>(method, path, input, false);
                    else
                        throw AccessException.Create(response.StatusCode, await response.Content.ReadAsStringAsync());
                }
            }
            catch (AccessException) { throw; }
            catch (Exception ex)
            {
                Debug.WriteLine(JsonConvert.SerializeObject(ex));
                throw AccessException.Unknown();
            }
        }
        private Task<Output> Call<Output, Input>(HttpMethod method, string path, Input input)
        {
            return Call<Output, Input>(method, path, input, true);
        }
        private async Task<Output> CallUnauthenticated<Output, Input>(HttpMethod method, string path, Input input)
        {
            if (!QuickCheckLogin())
                throw AccessException.Unknown();

            var url = $"{ServiceUrl}api/{path}";
            var client = new HttpClient();
            client.Timeout = TimeSpan.FromMinutes(2); //2 minutes

            try
            {
                var json = (input != null) ? JsonConvert.SerializeObject(input) : "{}";
                var content = new StringContent(json, Encoding.UTF8, "application/json");

                HttpResponseMessage response = null;
                switch (method)
                {
                    default:
                        break;
                }
                if (method == HttpMethod.Get)
                    response = await client.GetAsync(url);
                else if (method == HttpMethod.Delete)
                    response = await client.DeleteAsync(url);
                else if (method == HttpMethod.Post)
                    response = await client.PostAsync(url, content);
                else
                    response = await client.PutAsync(url, content);

                if (response.IsSuccessStatusCode)
                {
                    if (typeof(Output) == typeof(byte[]))
                    {
                        var byteArrayResponse = await response.Content.ReadAsByteArrayAsync();
                        return (Output)(object)byteArrayResponse;
                    }
                    else
                    {
                        var strResponse = await response.Content.ReadAsStringAsync();
                        try
                        {
                            var serverResponse = JsonConvert.DeserializeObject<OkResponse<Output>>(strResponse);
                            if (!serverResponse.OK)
                            {
                                Debug.WriteLine(strResponse);
                                throw AccessException.Create(serverResponse.Exception);
                            }
                            return serverResponse.Data;
                        }
                        catch (Exception e)
                        {
                            if (typeof(Output) == typeof(string))
                                return (Output)(object)strResponse;
                            else
                                throw e;
                        }
                    }
                }
                else
                {
                    throw AccessException.Create(response.StatusCode, await response.Content.ReadAsStringAsync());
                }
            }
            catch (AccessException) { throw; }
            catch
            {
                throw AccessException.Unknown();
            }
        }

        public class OkResponse<T>
        {
            public bool OK { get; set; }
            public T Data { get; set; }
            public ExceptionModel Exception { get; set; }
        }
    }

    public class ExceptionModel
    {
        public string Type { get; set; }
        public string Message { get; set; }
        public string StackTrace { get; set; }
        public ExceptionModel InnerException { get; set; }
    }
}
