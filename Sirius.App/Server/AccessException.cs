﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace Sirius.App.Server
{
    public class AccessException : Exception
    {
        public HttpStatusCode Code { get; }
        public string ServerResponse { get; }

        private AccessException(string message, HttpStatusCode code, string serverResponse) : base(message)
        {
            this.Code = code;
            this.ServerResponse = serverResponse;
        }

        public static AccessException Create(ExceptionModel exception)
        {
            return new AccessException(exception.Message, HttpStatusCode.InternalServerError, JsonConvert.SerializeObject(exception));
        }

        public static AccessException Create(HttpStatusCode statusCode, string serverResponse)
        {
            return new AccessException(GetMessage(statusCode), statusCode, serverResponse);
        }

        public static AccessException Unknown()
        {
            return new AccessException("Unknown Error", HttpStatusCode.InternalServerError, null);
        }

        private static string GetMessage(HttpStatusCode statusCode)
        {
            switch (statusCode)
            {
                case HttpStatusCode.BadRequest:
                    return "Requisição inválida";
                case HttpStatusCode.Unauthorized:
                    return "Sem Autorização para realizar a operação pedida";
                default:
                    return "Erro Interno do Servidor";
            }
        }
    }
}
