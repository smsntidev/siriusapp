﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sirius.App.Server
{
    public class NaturezaModel
    {
        public int Codigo { get; set; }

        public string Nome { get; set; }

        public override string ToString()
        {
            return Nome;
        }
    }
    #region Iventario

    public class InventarioHeaderModel
    {
        public string TipoEstabelecimento { get; set; }

        public int? IdEstabelecimento { get; set; }

        public int? IdSetor { get; set; }

        public int? IdResponsavelAtesto { get; set; }

        public string Observacao { get; set; }

        public string CodigoBarras { get; set; }

        public bool InventarioFechado { get; set; }

        public int CodigoUsuario { get; set; }

        public List<ItemModel> Itens { get; set; }

        public List<ItemModel> ItensExcluidos { get; set; }
    }

    public class ItemModel
    {
        public int Codigo { get; set; }

        public bool SemIdentificacao { get; set; }

        public int Tombo { get; set; }

        public int TipoIBT { get; set; }

        public long IBT { get; set; }

        public bool IPBM { get; set; }

        public long IDPBM { get; set; }

        #region Material
        public int CodigoMaterial { get; set; }

        public MaterialModel Material { get; set; }
        #endregion

        #region Marca
        public int CodigoMarca { get; set; }

        public string Marca { get; set; }
        #endregion

        public string Modelo { get; set; }

        public string Serial { get; set; }

        #region SituacaoFisica
        public int CodigoSituacaoFisica { get; set; }

        public string SituacaoFisica { get; set; }
        #endregion 

        public int Quantidade { get; set; }

        public int IdInventario { get; set; }

        #region ReferenteAEntrada
        public int IdEntrada { get; set; }

        public int IdTipoEntrada { get; set; }
        #endregion

        #region Setor
        public int CodigoSetor { get; set; }

        public string NomeSetor { get; set; }

        #endregion

        #region Setor Destino (lucas oliveira)
        public int CodigoSetorDestino { get; set; }

        public string NomeSetorDestino { get; set; }

        #endregion

        #region

        public int CodigoEstabelecimento { get; set; }

        public string NomeEstabelecimento { get; set; }

        #endregion

        public string DescricaoMaterialInformatica { get; set; }

        public string NumeroControleEmpresa { get; set; }

        public string TomboToString { get; set; }

        public string Tombo2 { get; set; }
    }

    public class ItemMaterialModel
    {
        public string Codigo { get; set; }

        public string NomeMaterial { get; set; }
    }

    public class MaterialModel
    {
        public int Codigo { get; set; }

        public long Identificador { get; set; }

        public int CodigoClasse { get; set; }

        public string NomeClasse { get; set; }

        public string NomeMaterial { get; set; }

        public bool Excluido { get; set; }
    }

    public class InventarioModel
    {
        public int Id { get; set; }

        public DateTime DataAbertura { get; set; }

        public int IdSetor { get; set; }

        public SetorModel SetorModel { get; set; }

        public EstabelecimentoModel EstabelecimentoModel { get; set; }

        public int IdResponsavelAtesto { get; set; }

        public ResponsavelAtestoModel ResponsavelAtesto { get; set; }

        public DateTime? DataFechamento { get; set; }

        public bool Fechado { get; set; }

        public string Observacao { get; set; }

        public string MostraFechado { get; set; }

        //ideal é passar uma classe de Item compacta
        public List<ItemModel> Itens { get; set; }
    }

    public class MaterialFiltroModel
    {
        /// <summary>
        /// Identificador ou Nome do material
        /// </summary>
        public int Identificador { get; set; }

        public int TipoIdentificacao { get; set; }

        public string Tombo { get; set; }

        public long IBT { get; set; }

        public long IPBM { get; set; }

        public string Nome { get; set; }
    }

    #endregion

    #region Responsavel

    public class ResponsavelAtestoModel
    {
        public int Codigo { get; set; }

        public string Nome { get; set; }

        public int CodigoEstabelecimento { get; set; }

        public bool Excluido { get; set; }
    }

    public class ResponsavelAtestoHeaderModel
    {
        public int Codigo { get; set; }

        public string Nome { get; set; }

        public int CodigoEstabelecimento { get; set; }

        public bool Excluido { get; set; }
    }

    #endregion

    #region [ Setor ] 

    public class SetorModel
    {
        public int Codigo { get; set; }

        public int CodigoEstabelecimento { get; set; }

        public string Nome { get; set; }

        public bool Excluido { get; set; }
    }

    #endregion

    #region [ Estabelecimento ] 

    public class EstabelecimentoModel
    {
        public int Codigo { get; set; }

        public string NomeEstabelecimento { get; set; }

        public string Tipo { get; set; }

        public bool Excluido { get; set; }
    }

    #endregion

    #region [ Situacao Fisica ] 

    public class SituacaoFisicaModel
    {
        public int Codigo { get; set; }

        public string Nome { get; set; }

        public bool Excluido { get; set; }
    }

    #endregion

    public class UsuarioModel
    {
        public int Codigo { get; set; }

        public string Login { get; set; }

        public string Nome { get; set; }

        public int CodigoPerfil { get; set; }

        public bool Ativo { get; set; }
    }

    public class TipoMovimentacaoModel
    {
        public int Codigo { get; set; }

        public string Nome { get; set; }

        public bool Excluido { get; set; }
    }

    #region [ Ordem de Serviço ]
    public class AreaResponsavelModel
    {
        public int Codigo { get; set; }

        public string Nome { get; set; }

        public bool Excluido { get; set; }
    }
    public class ProblemaModel
    {
        public int Codigo { get; set; }

        public string Nome { get; set; }

        public bool Excluido { get; set; }
    }
    public class ChamadoModel
    {
        public int Codigo { get; set; }

        public int Numero { get; set; }

        public string Contato { get; set; }

        public Int64 Telefone { get; set; }

        public string Descricao { get; set; }

        public int CodigoItem { get; set; }

        public int CodigoStatusOcorrencia { get; set; }
        public string StatusOcorrencia { get; set; }

        public DateTime DataOcorrencia { get; set; }

        public bool SubChamado { get; set; }
        public int CodigoOcorrenciaSuperior { get; set; }

        public bool Justificativa { get; set; }
        public string JustificativaCancelamento { get; set; }

        public bool Solucao { get; set; }
        public string SolucaoChamado { get; set; }

        public int CodigoProblema { get; set; }
        public string Problema { get; set; }

        public int CodigoAreaResponsavel { get; set; }
        public string AreaResponsavel { get; set; }

        #region Arquivo ocorrência
        public int CodigoArquivoOcorrencia { get; set; }

        public string nomeArquivoOcorrencia { get; set; }

        public byte[] ArquivoOcorrencia { get; set; }
        #endregion

        public int CodigoUsuarioEncerramento { get; set; }
        public string UsuarioEncerramento { get; set; }

        public int CodigoSetor { get; set; }
        public string Setor { get; set; }

        public int CodigoEstabelecimento { get; set; }
        public string Estabelecimento { get; set; }

        public DateTime DataFinalizacao { get; set; }

        public bool Excluido { get; set; }
    }
    public class BuscarChamadoModel
    {
        public int CodigoItem { get; set; }

        public int CodigoStatusOcorrencia { get; set; }

        public int CodigoProblema { get; set; }

        public int CodigoAreaResponsavel { get; set; }

        public int CodigoSetor { get; set; }

        public int CodigoEstabelecimento { get; set; }

        public string Contato { get; set; }
    }
    #endregion

    public class MovimentacaoFiltroModel
    {
        public int TipoMovimentacao { get; set; }

        public int Numero { get; set; }

        public DateTime Data { get; set; }

        public int TipoIdentificacao { get; set; }

        public string Tombo { get; set; }

        public long IBT { get; set; }

        public long IPBM { get; set; }
    }

    public class MovimentacaoModel
    {
        public MovimentacaoModel()
        {
            Itens = new List<ItemModel>();
        }

        public int Codigo { get; set; }

        public int CodigoTipoMovimentacao { get; set; }

        public DateTime DataHora { get; set; }

        public DateTime DataRetorno { get; set; }

        public string Observacao { get; set; }

        public EstabelecimentoModel EstabelecimentoCedenteModel { get; set; }

        public EstabelecimentoModel EstabelecimentoRequisitanteModel { get; set; }

        public SetorModel SetorModel { get; set; }

        public UsuarioModel UsuarioCedenteModel { get; set; }

        public UsuarioModel UsuarioRequisitanteModel { get; set; }

        public int CodigoDestinatario { get; set; }

        public DestinatarioModel Destinatario { get; set; }

        public char TipoOutros { get; set; }

        public char TipoSolicitacao { get; set; }

        public List<ItemModel> Itens { get; set; }
    }

    public class DestinatarioModel
    {
        public int Codigo { get; set; }

        public string RazaoSocial { get; set; }

        public bool Excluido { get; set; }
    }

    public class RegistroMovimentacaoModel
    {
        public int Codigo { get; set; }

        public int CodigoTipoMovimentacao { get; set; }

        public string CodigoTipoDestinatario { get; set; }

        public DateTime DataHora { get; set; }

        public DateTime DataRetorno { get; set; }

        public string Observacao { get; set; }

        public EstabelecimentoModel EstabelecimentoCedenteModel { get; set; }

        public EstabelecimentoModel EstabelecimentoRequisitanteModel { get; set; }

        public int CodigoUnidadeSaude { get; set; }

        public UsuarioModel UsuarioCedenteModel { get; set; }

        public UsuarioModel UsuarioRequisitanteModel { get; set; }

        public int CodigoDestinatario { get; set; }

        public List<ItemModel> Itens { get; set; }

        public List<ItemModel> ItensExcluidos { get; set; }

        public List<ItemModel> ItensSemIdentificacao { get; set; }

        public char TipoOutros { get; set; }

        public char TipoSolicitacao { get; set; }
    }

    public class RegistroInventarioModel
    {
        public string TipoEstabelecimento { get; set; }

        public int? IdEstabelecimento { get; set; }

        public int? IdSetor { get; set; }

        public int? IdResponsavelAtesto { get; set; }

        public string Observacao { get; set; }

        public bool InventarioFechado { get; set; }

        public int CodigoUsuario { get; set; }

        public List<ItemModel> Itens { get; set; }

        public List<int> ItensExcluidos { get; set; }
    }
    public class AlterarSenhaModel
    {
        public string Login { get; set; }

        public string Nome { get; set; }

        public string SenhaAtual { get; set; }

        public string NovaSenha { get; set; }

        public string ConfirmarSenha { get; set; }
    }
}