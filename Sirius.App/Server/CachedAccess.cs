﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace Sirius.App.Server
{
    public partial class CachedAccess
    {
        private static CachedAccess _CachedAccess = new CachedAccess();
        public static CachedAccess Current => _CachedAccess;
        private CachedAccess() { }

        private static readonly TimeSpan CacheDuration = TimeSpan.FromHours(3);
        private static readonly TimeSpan MaximumServerDelay = TimeSpan.FromSeconds(10);

        #region Infrastructure
        public interface ICachedContinuation<T>
        {
            Task<T> Get(Func<Task<T>> onCacheMiss, bool onlyIfError = false, TimeSpan? cacheDuration = null);
        }
        public class CachedObject<T>
        {
            public DateTime Expires { get; set; }
            public bool Expired => DateTime.UtcNow >= Expires;
            public T Value { get; set; }
        }
        private class CachedContinuation<T> : ICachedContinuation<T>
        {
            private string name;
            private object[] parameters;

            public CachedContinuation(string name, object[] parameters)
            {
                this.name = name;
                this.parameters = parameters;
            }

            public async Task<T> Get(Func<Task<T>> onCacheMiss, bool onlyIfError = false, TimeSpan? cacheDuration = null)
            {
#if DISABLE_CACHE
                return await onCacheMiss();
#else
                if (cacheDuration == null)
                    cacheDuration = CachedAccess.CacheDuration;

                var key = GetKey();
                var obj = CachedAccess.Get<CachedObject<T>>(key);
                if (obj == null) //não existe no cache
                {
                    var newValue = await onCacheMiss();
                    CachedAccess.Set<CachedObject<T>>(key, new CachedObject<T>()
                    {
                        Expires = DateTime.UtcNow.Add(cacheDuration.Value),
                        Value = newValue
                    });
                    await CachedAccess.Save();
                    return newValue;
                }
                else if (obj.Expired && !onlyIfError) //existe mas expirou
                {
                    try
                    {
                        var task = onCacheMiss();

                        using (var timeoutCancellationTokenSource = new CancellationTokenSource())
                        {

                            var completedTask = await Task.WhenAny(task, Task.Delay(MaximumServerDelay, timeoutCancellationTokenSource.Token));
                            if (completedTask == task)
                            {
                                timeoutCancellationTokenSource.Cancel();
                                var newValue = await task;  // Very important in order to propagate exceptions
                                CachedAccess.Set<CachedObject<T>>(key, new CachedObject<T>()
                                {
                                    Expires = DateTime.UtcNow.Add(cacheDuration.Value),
                                    Value = newValue
                                });
                                await CachedAccess.Save();
                                return newValue;
                            }
                            else
                            {
                                CacheIfServerResponds(task, key, cacheDuration.Value);
                                return obj.Value;
                            }
                        }
                    }
                    catch
                    {
                        return obj.Value; //returns the expired
                    }
                }
                else
                {
                    if (!onlyIfError)
                        return obj.Value;
                    else
                    {
                        try
                        {
                            var newValue = await onCacheMiss();
                            CachedAccess.Set<CachedObject<T>>(key, new CachedObject<T>()
                            {
                                Expires = DateTime.UtcNow.Add(cacheDuration.Value),
                                Value = newValue
                            });
                            await CachedAccess.Save();
                            return newValue;
                        }
                        catch
                        {
                            return obj.Value; //o servidor não conseguiu retornar valor...
                        }
                    }
                }
#endif
            }

            private async void CacheIfServerResponds(Task<T> task, string key, TimeSpan cacheDuration)
            {
                try
                {
                    var newValue = await task;  // Very important in order to propagate exceptions
                    CachedAccess.Set<CachedObject<T>>(key, new CachedObject<T>()
                    {
                        Expires = DateTime.UtcNow.Add(cacheDuration),
                        Value = newValue
                    });
                    await CachedAccess.Save();
                }
                catch { }
            }

            private string GetKey()
            {
                string pPart = "P";
                if (parameters != null)
                {
                    List<object> ps = new List<object>();
                    foreach (var item in parameters)
                    {
                        if (item == null)
                            ps.Add("@@N");
                        else
                            ps.Add("@@V" + Convert.ToString(item, CultureInfo.InvariantCulture));
                    }
                    pPart = "P" + string.Join("", ps.ToArray());
                }
                return $"CachedBusiness-N{name}-{pPart}";
            }
        }
        public ICachedContinuation<T> CachedGet<T>(string name, params object[] parameters)
        {
            if (name == null)
                throw new ArgumentNullException(nameof(name));
            return new CachedContinuation<T>(name, parameters);
        }

        private static T Get<T>(string nome) where T : class
        {
            try
            {
                if (!Application.Current.Properties.ContainsKey(nome))
                    return null;

                var resultado = Application.Current.Properties[nome] as string;
                if (string.IsNullOrWhiteSpace(resultado))
                    return null;

                return JsonConvert.DeserializeObject<T>(resultado);
            }
            catch
            {
                return null;
            }
        }
        private static void Set<T>(string nome, T valor) where T : class
        {
            try
            {
                Application.Current.Properties[nome] = JsonConvert.SerializeObject(valor);
            }
            catch { }
        }
        private static void Remove(string nome)
        {
            try
            {
                if (Application.Current.Properties.ContainsKey(nome))
                    Application.Current.Properties.Remove(nome);
            }
            catch { }
        }
        private static async Task Save()
        {
            try
            {
                await Application.Current.SavePropertiesAsync();
            }
            catch
            {

            }
        }
        #endregion


    }
}
