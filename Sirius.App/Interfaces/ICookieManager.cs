﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace Sirius.App
{
    public interface ICookieManager
    {
        void Set(Uri cookiesUrl, IEnumerable<Cookie> cookies);
    }
}
