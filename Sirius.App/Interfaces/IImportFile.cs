﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sirius.App
{
    public interface IImportFile
    {
        Task<ImportedFile> OpenFile();
    }

    public class ImportedFile
    {
        public Func<Stream> Stream { get; set; }
        public string Mime { get; set; }
    }
}
