﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;

[assembly: Xamarin.Forms.Dependency(typeof(Sirius.App.Android.Implementations.AndroidImportFile))]

namespace Sirius.App.Android.Implementations
{
    public class AndroidImportFile : IImportFile
    {
        public static MainActivity Context => MainActivity.Current;

        public Task<ImportedFile> OpenFile()
        {
            TaskCompletionSource<ImportedFile> taskCompletionSource = new TaskCompletionSource<ImportedFile>();
            AlertDialog.Builder builderSingle = new AlertDialog.Builder(Context);
            
            builderSingle.SetTitle("Adicionar JPG, PDF ou DOCX?");
            builderSingle.SetNegativeButton("Cancelar", (sender, args) =>
            {
                ((IDialogInterface)sender).Dismiss();
                taskCompletionSource.SetException(new System.OperationCanceledException("Cancelado pelo usuário."));
            });

            ArrayAdapter<String> arrayAdapter = new ArrayAdapter<String>(Context, Resource.Layout.select_dialog_singlechoice_material);
            arrayAdapter.Add("JPG");
            arrayAdapter.Add("PDF");
            arrayAdapter.Add("DOCX");

            builderSingle.SetAdapter(arrayAdapter, (sender, args) =>
            {
                ((IDialogInterface)sender).Dismiss();
                var mime = args.Which == 0 ? "image/*" :
                    (args.Which == 1 ? "application/pdf" : "application/vnd.openxmlformats-officedocument.wordprocessingml.document");

                Context.PickFile(mime).ContinueWith(r =>
                {
                    Context.RunOnUiThread(() =>
                    {
                        try
                        {
                            taskCompletionSource.SetResult(r.Result);
                        }
                        catch (Exception e)
                        {
                            taskCompletionSource.SetException(e);
                        }
                    });
                });
            });
            builderSingle.Show();
            return taskCompletionSource.Task;
        }
    }
}