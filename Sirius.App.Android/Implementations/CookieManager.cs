﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Webkit;
using Android.Widget;

[assembly: Xamarin.Forms.Dependency(typeof(Sirius.App.Android.Implementations.AndroidCookieManager))]

namespace Sirius.App.Android.Implementations
{
    public class AndroidCookieManager : ICookieManager
    {
        public void Set(Uri cookiesUrl, IEnumerable<Cookie> cookieList)
        {
            var cookieManager = CookieManager.Instance;
            cookieManager.SetAcceptCookie(true);
            cookieManager.RemoveAllCookie();
            var cookies = cookieList.ToList();
            for (var i = 0; i < cookies.Count; i++)
            {
                string cookieValue = cookies[i].Value;
                string cookieDomain = cookies[i].Domain;
                string cookieName = cookies[i].Name;
                cookieManager.SetCookie(cookieDomain, cookieName + "=" + cookieValue);
            }
        }
    }
}