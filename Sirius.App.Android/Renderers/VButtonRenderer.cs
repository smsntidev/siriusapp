﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Sirius.App;
using Xamarin.Forms;
using Xamarin.Forms.Platform.Android;

[assembly: ExportRenderer(typeof(VButton), typeof(Sirius.App.Android.VButtonRenderer))]
namespace Sirius.App.Android
{
    public class VButtonRenderer : ButtonRenderer
    {
        public VButtonRenderer(Context ctx) : base(ctx)
        {

        }

        protected override void OnElementChanged(ElementChangedEventArgs<Xamarin.Forms.Button> e)
        {
            base.OnElementChanged(e);
            
            if (Control != null)
            {
                Control.Gravity = GravityFlags.Center;
                Control.SetPadding(Control.PaddingLeft, 0, Control.PaddingRight, 0);

                switch ((this.Element as VButton).TextAlignment)
                {
                    case Xamarin.Forms.TextAlignment.Start:
                        Control.Gravity = GravityFlags.Left | GravityFlags.CenterVertical;
                        break;
                    case Xamarin.Forms.TextAlignment.Center:
                        Control.Gravity = GravityFlags.Center;
                        break;
                    case Xamarin.Forms.TextAlignment.End:
                        Control.Gravity = GravityFlags.Right | GravityFlags.CenterVertical;
                        break;
                    default:
                        break;
                }
            }
        }
    }
}