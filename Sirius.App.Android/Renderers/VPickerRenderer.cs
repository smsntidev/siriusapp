﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Sirius.App;
using Xamarin.Forms;
using Xamarin.Forms.Platform.Android;

[assembly: ExportRenderer(typeof(VPicker), typeof(Sirius.App.Android.VPickerRenderer))]
namespace Sirius.App.Android
{
    public class VPickerRenderer : PickerRenderer
    {
        public VPickerRenderer(Context ctx) : base(ctx)
        {

        }

        protected override void OnElementChanged(ElementChangedEventArgs<Xamarin.Forms.Picker> e)
        {
            base.OnElementChanged(e);
            var view = Element as VPicker;

            if (Control != null)
            {
                Control.SetBackgroundResource(Resource.Drawable.VEntryBackground);
                Control.SetPadding(30, 35, 30, 35);
                Control.TextSize = 16;
                Control.InputType = global::Android.Text.InputTypes.TextFlagNoSuggestions;

                var thickness = (view as VPicker)?.Padding ?? new Thickness(30, 35, 30, 35);
                Control.SetPadding((int)thickness.Left, (int)thickness.Top, (int)thickness.Right, (int)thickness.Bottom);
            }
        }
    }
}