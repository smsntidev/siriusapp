﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Sirius.App;
using Xamarin.Forms;
using Xamarin.Forms.Platform.Android;

[assembly: ExportRenderer(typeof(VLoginEntry), typeof(Sirius.App.Android.VLoginEntryRenderer))]
namespace Sirius.App.Android
{
    public class VLoginEntryRenderer : EntryRenderer
    {
        public VLoginEntryRenderer(Context context) : base(context)
        {

        }

        protected override void OnElementChanged(ElementChangedEventArgs<Entry> e)
        {
            base.OnElementChanged(e);

            if (Control != null)
            {
                var thickness = (e.NewElement as VLoginEntry)?.Padding ?? new Thickness(30, 35, 30, 35);

                Control.SetBackgroundResource(Resource.Drawable.VLoginEntryBackground);
                Control.SetPadding((int)thickness.Left, (int)thickness.Top, (int)thickness.Right, (int)thickness.Bottom);
                if (!e.NewElement.IsPassword)
                    Control.InputType = global::Android.Text.InputTypes.TextFlagNoSuggestions;
            }
        }
    }
}