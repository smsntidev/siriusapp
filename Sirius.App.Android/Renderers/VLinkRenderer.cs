﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Sirius.App;
using Xamarin.Forms;
using Xamarin.Forms.Platform.Android;

[assembly: ExportRenderer(typeof(VLink), typeof(Sirius.App.Android.VLinkRenderer))]
namespace Sirius.App.Android
{
    public class VLinkRenderer : ButtonRenderer
    {
        public VLinkRenderer(Context ctx) : base(ctx)
        {

        }

        protected override void OnElementChanged(ElementChangedEventArgs<Xamarin.Forms.Button> e)
        {
            base.OnElementChanged(e);

            if (Control != null)
            {
                if (Build.VERSION.SdkInt >= (BuildVersionCodes)23)
                    Control.SetTextColor(Resources.GetColorStateList(Resource.Color.VLinkColor, null));
                else
                {
#pragma warning disable 0618
                    Control.SetTextColor(Resources.GetColorStateList(Resource.Color.VLinkColor));
#pragma warning restore 0618
                }
                Control.SetBackgroundResource(Resource.Drawable.VLinkBackground);
            }
        }
    }
}