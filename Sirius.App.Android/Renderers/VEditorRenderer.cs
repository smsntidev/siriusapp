﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Sirius.App;
using Xamarin.Forms;
using Xamarin.Forms.Platform.Android;

[assembly: ExportRenderer(typeof(VEditor), typeof(Sirius.App.Android.VEditorRenderer))]
namespace Sirius.App.Android
{
    public class VEditorRenderer : EditorRenderer
    {
        public VEditorRenderer(Context context) : base(context)
        {

        }

        protected override void OnElementChanged(ElementChangedEventArgs<Editor> e)
        {
            base.OnElementChanged(e);

            if (Control != null)
            {
                var thickness = (e.NewElement as VEditor)?.Padding ?? new Thickness(30, 35, 30, 35);

                Control.SetBackgroundResource(Resource.Drawable.VEntryBackground);
                Control.SetPadding((int)thickness.Left, (int)thickness.Top, (int)thickness.Right, (int)thickness.Bottom);
            }
        }
    }
}