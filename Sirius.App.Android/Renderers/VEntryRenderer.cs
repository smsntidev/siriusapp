﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Xamarin.Forms.Platform.Android;
using Xamarin.Forms;
using Sirius.App;
using Android.Graphics.Drawables;

[assembly: ExportRenderer(typeof(VEntry), typeof(Sirius.App.Android.VEntryRenderer))]
namespace Sirius.App.Android
{
    public class VEntryRenderer : EntryRenderer
    {
        public VEntryRenderer(Context context) : base(context)
        {

        }

        protected override void OnElementChanged(ElementChangedEventArgs<Entry> e)
        {
            base.OnElementChanged(e);

            if (Control != null)
            {
                var thickness = (e.NewElement as VEntry)?.Padding ?? new Thickness(30, 35, 30, 35);

                Control.SetBackgroundResource(Resource.Drawable.VEntryBackground);
                Control.SetPadding((int)thickness.Left, (int)thickness.Top, (int)thickness.Right, (int)thickness.Bottom);
                if (!e.NewElement.IsPassword && e.NewElement.Keyboard == Keyboard.Default)
                    Control.InputType = global::Android.Text.InputTypes.TextFlagNoSuggestions;
            }
        }
    }
}