﻿using System;

using Android.App;
using Android.Content.PM;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Android.OS;
using System.Threading.Tasks;
using System.Collections.Generic;
using Android.Content;

namespace Sirius.App.Android
{
    [Activity(Label = "Sirius", Icon = "@drawable/icon", Theme = "@style/MainTheme", ConfigurationChanges = ConfigChanges.ScreenSize, ScreenOrientation = ScreenOrientation.Portrait)]
    public class MainActivity : global::Xamarin.Forms.Platform.Android.FormsAppCompatActivity
    {
        public static MainActivity Current = null;

        protected override void OnCreate(Bundle bundle)
        {
            ZXing.Net.Mobile.Forms.Android.Platform.Init();
            MainActivity.Current = this;
            SetUpAutoMapper();

            TabLayoutResource = Resource.Layout.Tabbar;
            ToolbarResource = Resource.Layout.Toolbar;

            base.OnCreate(bundle);

            global::Xamarin.Forms.Forms.Init(this, bundle);
            LoadApplication(new App());
        }

        private void SetUpAutoMapper()
        {
            AutoMapper.Mapper.Initialize(cfg =>
            {
                cfg.CreateMissingTypeMaps = true;
            });
        }

        private Dictionary<int, (TaskCompletionSource<ImportedFile>, string)> pictureRequests = new Dictionary<int, (TaskCompletionSource<ImportedFile>, string)>();
        private int requestCodeSeed = 0;
        public Task<ImportedFile> PickFile(string mime)
        {
            var requestCode = requestCodeSeed++;

            TaskCompletionSource<ImportedFile> taskCompletionSource = new TaskCompletionSource<ImportedFile>();
            this.pictureRequests.Add(requestCode, (taskCompletionSource, mime == "image/*" ? "image/jpeg" : mime));
            Intent pickFile = new Intent(Intent.ActionGetContent, global::Android.Provider.MediaStore.Images.Media.ExternalContentUri);
            pickFile.SetType(mime);
            pickFile.PutExtra(Intent.ExtraLocalOnly, true);
            this.StartActivityForResult(pickFile, requestCode);
            return taskCompletionSource.Task;
        }
        protected override void OnActivityResult(int requestCode, Result resultCode, Intent data)
        {
            base.OnActivityResult(requestCode, resultCode, data);

            if (pictureRequests.ContainsKey(requestCode))
            {
                var request = pictureRequests[requestCode];
                pictureRequests.Remove(requestCode);

                try
                {
                    if (resultCode == Result.Ok)
                    {
                        global::Android.Net.Uri uri = data.Data;
                        Func<System.IO.Stream> stream = () => ContentResolver.OpenInputStream(uri);
                        request.Item1.SetResult(new ImportedFile()
                        {
                            Mime = data.Type ?? request.Item2,
                            Stream = stream
                        });
                    }
                    else
                    {
                        RunOnUiThread(() =>
                        {
                            request.Item1.SetException(new System.OperationCanceledException("Cancelado pelo usuário."));
                        });
                    }
                }
                catch (Exception e)
                {
                    RunOnUiThread(() =>
                    {
                        request.Item1.SetException(e);
                    });
                }
            }
        }

        public override void OnRequestPermissionsResult(int requestCode, string[] permissions, [GeneratedEnum] Permission[] grantResults)
        {
            global::ZXing.Net.Mobile.Android.PermissionsHandler.OnRequestPermissionsResult(requestCode, permissions, grantResults);
        }
    }
}

